$(document).ready(function() {
    $( "#datepicker" ).datepicker({ 
    	dateFormat: "yy-mm-dd",
    	minDate: 0,
    	showWeek: true,
        firstDay: 1,
        changeMonth: true,
        changeYear: true});
  });