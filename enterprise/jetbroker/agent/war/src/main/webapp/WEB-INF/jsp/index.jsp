<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<html>
<head>

<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet"  />

</head>
<body>

	<jsp:include page="header.jsp" />
	
	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/flightrequest.png"><spring:message code="index.flightRequests"/></h2>
		
	<p>
		<a class="button" href="<%=request.getContextPath()%>/flightrequest/0/edit.html"><spring:message code="index.createFlightRequest"/></a>
	</p>
	
	<h3><spring:message code="index.waitingFlightRequests"/></h3>
	<c:choose>
		<c:when test="${not empty unprocessedRequests}">
			<table class="dataTable">
				<tr>
					<th><spring:message code="global.id"/></th>
					<th><spring:message code="flightRequest.status"/></th>
					<th><spring:message code="flightRequest.departureAirport"/></th>
					<th><spring:message code="flightRequest.arrivalAirport"/></th>
					<th><spring:message code="flightRequest.date"/></th>
					<th><spring:message code="flightRequest.customer"/></th>
					<th><spring:message code="flightRequest.passengers"/></th>
					<th><spring:message code="global.edit"/></th>
					<th><spring:message code="flightRequest.response"/></th>
				</tr>
				<c:forEach items="${unprocessedRequests}" var="request">
				<tr>
					<td>${request.id}</td>
					<td>${request.niceStatus}</td>
					<td>${request.departureAirportCode}</td>
					<td>${request.arrivalAirportCode}</td>
					<td>${request.niceDate}</td>
					<td>${request.customer.niceName}</td>
					<td>${request.passengers}</td>
					<td><a href="<%=request.getContextPath()%>/flightrequest/${request.id}/edit.html"><img src="images/edit.png" ></a></td>
					<td><a href="<%=request.getContextPath()%>/index/mock/${request.id}.html">Mock</a></td>
				</tr>
		</c:forEach>
	
	</table>
		</c:when>
		<c:otherwise>
			<h4><spring:message code="index.noUnprocessed"/></h4>
		</c:otherwise>
	</c:choose>
	
	<h3><spring:message code="index.offeredFlightRequests"/></h3>
	<c:choose>
		<c:when test="${not empty offeredRequests}">
			<table class="dataTable">
				<tr>
					<th><spring:message code="global.id"/></th>
					<th><spring:message code="flightRequest.status"/></th>
					<th><spring:message code="flightRequest.departureAirport"/></th>
					<th><spring:message code="flightRequest.arrivalAirport"/></th>
					<th><spring:message code="flightRequest.date"/></th>
					<th><spring:message code="flightRequest.customer"/></th>
					<th><spring:message code="flightRequest.passengers"/></th>
					<th><spring:message code="flightRequest.planeType"/></th>
					<th><spring:message code="flightRequest.offeredPrice"/></th>
					<th><spring:message code="flightRequest.response"/></th>
				</tr>
				<c:forEach items="${offeredRequests}" var="requestWrapper">
				<tr>
					<td>${requestWrapper.request.id}</td>
					<td>${requestWrapper.request.niceStatus}</td>
					<td>${requestWrapper.request.departureAirportCode}</td>
					<td>${requestWrapper.request.arrivalAirportCode}</td>
					<td>${requestWrapper.request.niceDate}</td>
					<td>${requestWrapper.request.customer.niceName}</td>
					<td>${requestWrapper.request.passengers}</td>
					<td>${requestWrapper.planeType.name} (${requestWrapper.planeType.maxNoOfPassengers} <spring:message code="flightRequest.seats"/>)</td>
					<td><fmt:formatNumber  groupingUsed="true" value="${requestWrapper.request.price}" /> SEK</td>
					<td><a href="<%=request.getContextPath()%>/index/mock/${requestWrapper.request.id}.html">Mock</a></td>
				</tr>
		</c:forEach>
	
	</table>
		</c:when>
		<c:otherwise>
			<h4><spring:message code="index.noOffers"/></h4>
		</c:otherwise>
	</c:choose>
	
	<h3><spring:message code="index.rejectedFlightRequests"/></h3>
	<c:choose>
		<c:when test="${not empty rejectedRequests}">
			<table class="dataTable">
				<tr>
					<th><spring:message code="global.id"/></th>
					<th><spring:message code="flightRequest.status"/></th>
					<th><spring:message code="flightRequest.departureAirport"/></th>
					<th><spring:message code="flightRequest.arrivalAirport"/></th>
					<th><spring:message code="flightRequest.date"/></th>
					<th><spring:message code="flightRequest.customer"/></th>
					<th><spring:message code="flightRequest.passengers"/></th>
				</tr>
				<c:forEach items="${rejectedRequests}" var="request">
				<tr>
					<td>${request.id}</td>
					<td>${request.niceStatus}</td>
					<td>${request.departureAirportCode}</td>
					<td>${request.arrivalAirportCode}</td>
					<td>${request.niceDate}</td>
					<td>${request.customer.niceName}</td>
					<td>${request.passengers}</td>
				</tr>
		</c:forEach>
	
	</table>
		</c:when>
		<c:otherwise>
			<h4><spring:message code="index.noRejects"/></h4>
		</c:otherwise>
	</c:choose>

	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/customer.png"><spring:message code="index.customers"/></h2>
	
	<p>
		<a class="button" href="<%=request.getContextPath()%>/customer/0/edit.html"><spring:message code="index.createCustomer"/></a>
	</p>
	
	<table class="dataTable">
		<tr>
			<th><spring:message code="global.id"/></th>
			<th><spring:message code="customer.firstName"/></th>
			<th><spring:message code="customer.lastName"/></th>
			<th><spring:message code="customer.company"/></th>
			<th><spring:message code="global.edit"/></th>
		</tr>
		<c:forEach items="${customers}" var="customer">
		<tr>
			<td>${customer.id}</td>
			<td>${customer.firstName}</td>
			<td>${customer.lastName}</td>
			<td>${customer.company}</td>
			<td><a href="<%=request.getContextPath()%>/customer/${customer.id}/edit.html"><img src="images/edit.png" ></a></td>
		
		</tr>
		</c:forEach>
	
	</table>
	
</body>

</html>