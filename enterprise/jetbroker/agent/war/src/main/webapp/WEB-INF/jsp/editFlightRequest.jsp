<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet"  />
<link href="<%=request.getContextPath()%>/js/jquery-ui.css" type="text/css" rel="stylesheet"  />
</head>
<body>

	<jsp:include page="header.jsp" />
	
	<h2 class="underline">
	<img src="<%=request.getContextPath()%>/images/flightrequest.png">
	<c:choose>
		<c:when test="${editFlightRequestBean.id > 0}">
			<spring:message code="flightRequest.update"/>
		</c:when>
		<c:otherwise>
			<spring:message code="flightRequest.new"/>
		</c:otherwise>
	</c:choose>
	</h2>
	
	<form:form commandName="editFlightRequestBean">
		<form:hidden path="id" />
		<form:errors path="" cssClass="errors" />
		<table class="formTable">
			<c:if test="${editFlightRequestBean.id > 0}">
			<tr>
				<th><spring:message code="global.id"/></th>
				<td>${editFlightRequestBean.id}</td>
				<td></td>
			</tr>
			</c:if>
			<c:set var="selectCustomerText">
						<spring:message code="flightRequest.selectCustomerText"/>
			</c:set>
			<tr>
				<th><spring:message code="flightRequest.customer"/></th>
				<td>
					<form:select path="customerId">
						<c:if test="${editFlightRequestBean.id == 0}">
							<form:option value="" label="${selectCustomerText}"/>
						</c:if>
					<form:options items="${customers}" itemValue="id" itemLabel="niceName"/>
					</form:select>	
				</td>
				<td><form:errors path="customerId" cssClass="errors" /></td>
			</tr>
			<c:set var="selectAirportText">
						<spring:message code="flightRequest.selectAirportText"/>
			</c:set>
			<tr>
				<th><spring:message code="flightRequest.departureAirport"/></th>
				<td>
					<form:select path="departureAirportCode">
						<c:if test="${editFlightRequestBean.id == 0}">
							<form:option value="" label="${selectAirportText}"/>
						</c:if>
						<form:options items="${airports}" itemValue="code" itemLabel="fullName"/>
					</form:select>	
				</td>
				<td><form:errors path="departureAirportCode" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightRequest.arrivalAirport"/></th>
				<td>
					<form:select path="arrivalAirportCode">
						<c:if test="${editFlightRequestBean.id == 0}">
							<form:option value="" label="${selectAirportText}"/>
						</c:if>
						<form:options items="${airports}" itemValue="code" itemLabel="fullName"/>
					</form:select>	
				</td>
				<td><form:errors path="arrivalAirportCode" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightRequest.passengers"/></th>
				<td><form:input type ="number" path="passengers"/></td>
				<td><form:errors path="passengers" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightRequest.date"/></th>
				<td><form:input id="datepicker" path="date"/></td>
				<td><form:errors path="date" cssClass="errors" /></td>
			</tr>
			<tr>
				<th><spring:message code="flightRequest.time"/></th>
				<td>
					<form:select path="hour">
						<c:if test="${editFlightRequestBean.id == 0}">
							<form:option value="" label=""></form:option>
						</c:if>
						<c:forEach var="i" begin="0" end="23">
							<form:option value="${i}" label="${i}"></form:option>
						</c:forEach>
					</form:select>:
					<form:select path="minute">
						<c:if test="${editFlightRequestBean.id == 0}">
							<form:option value="" label=""></form:option>
						</c:if>
						<c:forEach var="i" begin="0" end="59">
							<form:option value="${i}" label="${i}"></form:option>
						</c:forEach>
					</form:select>
				</td>
				<td><form:errors path="hour" cssClass="errors" /><br><form:errors path="minute" cssClass="errors" /></td>
			</tr>
			<tr>
				<th>
				</th>
				<td>
					<c:set var="submitText">
						<spring:message code="global.submit"/>
					</c:set>
					<input type="submit" value="${submitText}"/>
					<a href="<%=request.getContextPath()%>/index.html">Cancel</a>
				</td>
				<td>
				</td>
			</tr>			
		</table>
	</form:form>

	<script src="<%=request.getContextPath()%>/js/jquery-1.9.1.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery-ui-1.10.3.custom.js"></script>
	<script src="<%=request.getContextPath()%>/js/datepicker.js"></script>
</body>
</html>