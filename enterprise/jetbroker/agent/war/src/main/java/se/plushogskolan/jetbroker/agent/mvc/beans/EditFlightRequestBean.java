package se.plushogskolan.jetbroker.agent.mvc.beans;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirport;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;

/**
 * Bean containing information about a flight-request
 * 
 * @author marcus
 *
 */
@ArrivalAndDepartureAirport
public class EditFlightRequestBean implements ArrivalAndDepartureAirportHolder {

	private long id;

	@NotNull(message = "{validation.flightRequest.customer.missing}")
	private Long customerId;

	@NotBlank(message = "{validation.flightRequest.departure.missing}")
	private String departureAirportCode;

	@NotBlank(message = "{validation.flightRequest.arrival.missing}")
	private String arrivalAirportCode;

	@NotNull(message = "{validation.flightRequest.passengers.missing}")
	@Range(min = 1, max = 500, message = "{validation.flightRequest.passengers.invalid}")
	private Integer passengers;

	@NotNull(message = "{validation.flightRequest.date.missing}")
	@DateTimeFormat(pattern = "YYYY-MM-dd")
	private DateTime date;

	@NotBlank(message = "{validation.flightRequest.hour.missing}")
	private String hour;

	@NotBlank(message = "{validation.flightRequest.minute.missing}")
	private String minute;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	@Override
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirport) {
		this.departureAirportCode = departureAirport;
	}

	@Override
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirport) {
		this.arrivalAirportCode = arrivalAirport;
	}

	public Integer getPassengers() {
		return passengers;
	}

	public void setPassengers(Integer passengers) {
		this.passengers = passengers;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}

}
