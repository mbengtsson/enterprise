package se.plushogskolan.jetbroker.agent.mvc.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.mdb.simulator.MessageSimulator;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;
import se.plushogskolan.jetbroker.agent.service.PlaneTypeService;

/**
 * Spring MVC conrtroller for the index page of the agent system
 * 
 * @author marcus
 *
 */
@Controller
public class IndexController {

	@Inject
	private CustomerService customerService;

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private PlaneTypeService planeTypeService;

	@Inject
	private MessageSimulator simulator;

	@RequestMapping("/index.html")
	public ModelAndView index() {

		List<FlightRequest> unprocessedRequests = new ArrayList<FlightRequest>();
		List<FlightRequestWraper> offeredRequests = new ArrayList<FlightRequestWraper>();
		List<FlightRequest> rejectedRequests = new ArrayList<FlightRequest>();

		for (FlightRequest request : flightRequestService.getAllFlightRequests()) {
			if (request.getStatus() == Status.CREATED || request.getStatus() == Status.REQUEST_CONFIRMED) {
				unprocessedRequests.add(request);
			} else if (request.getStatus() == Status.OFFER_RECEIVED) {
				offeredRequests.add(new FlightRequestWraper(request, planeTypeService.getPlaneTypeByCode(request
				        .getPlane())));
			} else if (request.getStatus() == Status.REJECTED) {
				rejectedRequests.add(request);
			}
		}

		List<Customer> customers = customerService.getAllCustomers();
		Collections.sort(customers);

		ModelAndView mav = new ModelAndView("index");

		mav.addObject("unprocessedRequests", unprocessedRequests);
		mav.addObject("offeredRequests", offeredRequests);
		mav.addObject("rejectedRequests", rejectedRequests);
		mav.addObject("customers", customers);

		return mav;
	}

	@RequestMapping("/index/mock/{id}.html")
	public ModelAndView mock(@PathVariable long id) {

		if (flightRequestService.getFlightRequest(id).getStatus() == Status.CREATED) {
			simulator.sendConfirmationMessage(id);
		} else if (flightRequestService.getFlightRequest(id).getStatus() == Status.REQUEST_CONFIRMED) {
			simulator.sendOfferMessage(id);
		} else if (flightRequestService.getFlightRequest(id).getStatus() == Status.OFFER_RECEIVED) {
			simulator.sendRejectMessage(id);
		}

		return new ModelAndView("redirect:/index.html");
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

	/**
	 * Wrapper class used to wrap a flight-request with it's associated
	 * plane-type
	 * 
	 * @author marcus
	 *
	 */
	public class FlightRequestWraper {

		private FlightRequest request;
		private PlaneType planeType;

		public FlightRequestWraper(FlightRequest request, PlaneType planeType) {

			this.request = request;
			this.planeType = planeType;
		}

		public FlightRequest getRequest() {
			return request;
		}

		public void setRequest(FlightRequest request) {
			this.request = request;
		}

		public PlaneType getPlaneType() {
			return planeType;
		}

		public void setPlaneType(PlaneType planeType) {
			this.planeType = planeType;
		}

	}

}
