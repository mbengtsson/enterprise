package se.plushogskolan.jetbroker.agent.mvc.beans;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jetbroker.agent.domain.Customer;

/**
 * Bean containing information about a customer
 * 
 * @author marcus
 *
 */
public class EditCustomerBean {

	private long id;

	@NotBlank(message = "{validation.customer.firstName.missing}")
	private String firstName;

	@NotBlank(message = "{validation.customer.lastName.missing}")
	private String lastName;

	@NotBlank(message = "{validation.customer.email.missing}")
	@Email(message = "{validation.customer.email.invalid}")
	private String email;
	private String company;

	public void copyCustomerValuesToBean(Customer customer) {
		setId(customer.getId());
		setFirstName(customer.getFirstName());
		setLastName(customer.getLastName());
		setEmail(customer.getEmail());
		setCompany(customer.getCompany());
	}

	public void copyBeanValuesToCustomer(Customer customer) {
		customer.setId(getId());
		customer.setFirstName(getFirstName());
		customer.setLastName(getLastName());
		customer.setEmail(getEmail());
		customer.setCompany(getCompany());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

}
