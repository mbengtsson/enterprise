package se.plushogskolan.jetbroker.agent.mvc.controllers;

import java.util.Locale;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.mvc.beans.EditCustomerBean;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

/**
 * Spring MVC conrtroller for creating / editing customers
 * 
 * @author marcus
 *
 */
@Controller
@RequestMapping("/customer/{id}/edit.html")
public class EditCustomerController {

	private static Logger log = Logger.getLogger(EditCustomerController.class.getName());

	@Inject
	private CustomerService customerService;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		EditCustomerBean bean = new EditCustomerBean();

		if (id > 0) {
			bean.copyCustomerValuesToBean(getCustomerService().getCustomer(id));
		}

		ModelAndView mav = new ModelAndView("editCustomer");
		mav.addObject("editCustomerBean", bean);
		return mav;

	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditCustomerBean bean, BindingResult errors, Locale locale) {

		if (errors.hasErrors()) {
			ModelAndView mav = new ModelAndView("editCustomer");
			mav.addObject("editCustomerBean", bean);
			return mav;
		}

		if (bean.getFirstName().equals("i18n")) {
			log.info(messageSource.getMessage("i18n.test.message", null, locale));
			return new ModelAndView("redirect:/index.html");
		}

		if (bean.getId() > 0) {
			Customer customer = getCustomerService().getCustomer(bean.getId());
			bean.copyBeanValuesToCustomer(customer);
			getCustomerService().updateCustomer(customer);
		} else {
			Customer customer = new Customer();
			bean.copyBeanValuesToCustomer(customer);
			getCustomerService().createCustomer(customer);
		}

		return new ModelAndView("redirect:/index.html");

	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

}
