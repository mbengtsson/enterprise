package se.plushogskolan.jetbroker.agent.mvc.controllers;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.mvc.beans.EditFlightRequestBean;
import se.plushogskolan.jetbroker.agent.service.AirportService;
import se.plushogskolan.jetbroker.agent.service.CustomerService;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Spring MVC conrtroller for creating / editing flight-requests
 * 
 * @author marcus
 *
 */
@Controller
@RequestMapping("/flightrequest/{id}/edit.html")
public class EditFlightRequestController {

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private CustomerService customerService;

	@Inject
	private AirportService airportService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		List<Customer> customers = customerService.getAllCustomers();
		Collections.sort(customers);

		List<Airport> airports = airportService.getAllAirports();
		Collections.sort(airports);

		EditFlightRequestBean bean = new EditFlightRequestBean();

		if (id > 0) {
			copyFlightRequestValuesToBean(bean, getFlightRequestService().getFlightRequest(id));
		}

		ModelAndView mav = new ModelAndView("editFlightRequest");
		mav.addObject("editFlightRequestBean", bean);
		mav.addObject("customers", customers);
		mav.addObject("airports", airports);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditFlightRequestBean bean, BindingResult errors) {

		if (errors.hasErrors()) {

			List<Customer> customers = customerService.getAllCustomers();
			Collections.sort(customers);

			List<Airport> airports = airportService.getAllAirports();
			Collections.sort(airports);

			ModelAndView mav = new ModelAndView("editFlightRequest");
			mav.addObject("editFlightRequestBean", bean);
			mav.addObject("customers", customers);
			mav.addObject("airports", airports);
			return mav;
		}

		if (bean.getId() > 0) {
			FlightRequest request = getFlightRequestService().getFlightRequest(bean.getId());
			copyBeanValuesToFlightRequest(bean, request);
			getFlightRequestService().updateFlightRequest(request);
		} else {
			FlightRequest request = new FlightRequest();
			copyBeanValuesToFlightRequest(bean, request);
			getFlightRequestService().createFlightRequest(request);
		}

		return new ModelAndView("redirect:/index.html");
	}

	private void copyBeanValuesToFlightRequest(EditFlightRequestBean bean, FlightRequest request) {
		request.setId(bean.getId());
		request.setCustomer(customerService.getCustomer(bean.getCustomerId()));
		request.setPassengers(bean.getPassengers());
		request.setDepartureAirportCode(bean.getDepartureAirportCode());
		request.setArrivalAirportCode(bean.getArrivalAirportCode());
		request.setDate(parseDateTime(bean.getDate(), bean.getHour(), bean.getMinute()));

	}

	public void copyFlightRequestValuesToBean(EditFlightRequestBean bean, FlightRequest request) {
		bean.setId(request.getId());
		bean.setCustomerId(request.getCustomer().getId());
		bean.setDepartureAirportCode(request.getDepartureAirportCode());
		bean.setArrivalAirportCode(request.getArrivalAirportCode());
		bean.setPassengers(request.getPassengers());
		bean.setDate(request.getDate());
		bean.setHour(String.format("%d", request.getDate().getHourOfDay()));
		bean.setMinute(String.format("%d", request.getDate().getMinuteOfHour()));
	}

	private DateTime parseDateTime(DateTime date, String h, String m) {

		int hour = Integer.parseInt(h);
		int min = Integer.parseInt(m);

		MutableDateTime mutableDate = date.toMutableDateTime();
		mutableDate.setHourOfDay(hour);
		mutableDate.setMinuteOfHour(min);

		return mutableDate.toDateTime();
	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService flightRequestService) {
		this.flightRequestService = flightRequestService;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public AirportService getAirportService() {
		return airportService;
	}

	public void setAirportService(AirportService airportService) {
		this.airportService = airportService;
	}

}
