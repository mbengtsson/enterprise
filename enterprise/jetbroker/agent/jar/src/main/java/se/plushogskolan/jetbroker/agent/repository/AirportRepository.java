package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.Airport;

/**
 * Interface for airport repository
 * 
 * @author marcus
 *
 */
public interface AirportRepository {

	Airport getAirportByCode(String code);

	List<Airport> getAllAirports();
}
