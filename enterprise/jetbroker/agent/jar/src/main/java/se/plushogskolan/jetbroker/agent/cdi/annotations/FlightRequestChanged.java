package se.plushogskolan.jetbroker.agent.cdi.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * CDI Annotation used to qualify changes to flight-requests based on status
 * 
 * @author marcus
 *
 */
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE, METHOD, FIELD, PARAMETER })
public @interface FlightRequestChanged {

	public StatusChange value();

	enum StatusChange {
		CONFIRMED, OFFER_RECEIVED, REJECTED
	}
}
