package se.plushogskolan.jetbroker.agent.cdi.events;

/**
 * Event indicating a change to a flight-request, contains the flight-requests
 * id
 * 
 * @author marcus
 *
 */
public class FlightRequestChangedEvent {

	private long requestId;

	public FlightRequestChangedEvent() {

	}

	public FlightRequestChangedEvent(long requestId) {
		this.requestId = requestId;
	}

	public long getRequestId() {
		return requestId;
	}

	public void setRequestId(long requestId) {
		this.requestId = requestId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (requestId ^ (requestId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FlightRequestChangedEvent other = (FlightRequestChangedEvent) obj;
		if (requestId != other.requestId) {
			return false;
		}
		return true;
	}

}
