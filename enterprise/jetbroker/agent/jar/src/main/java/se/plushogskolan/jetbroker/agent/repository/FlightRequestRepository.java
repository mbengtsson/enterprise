package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;

/**
 * Interface for flight request repository
 * 
 * @author marcus
 *
 */
public interface FlightRequestRepository extends BaseRepository<FlightRequest> {

	List<FlightRequest> getAllFlightRequests();

	List<FlightRequest> getFlightRequestsByStatus(FlightRequest.Status status);

}
