package se.plushogskolan.jetbroker.agent.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Airport;
import se.plushogskolan.jetbroker.agent.repository.AirportRepository;
import se.plushogskolan.jetbroker.agent.service.AirportService;

/**
 * Implementation of airport service
 * 
 * @author marcus
 *
 */
@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	private AirportRepository AirportRepo;

	@Override
	public Airport getAirportByCode(String code) {
		return getAirportRepo().getAirportByCode(code);
	}

	@Override
	public List<Airport> getAllAirports() {
		return AirportRepo.getAllAirports();
	}

	protected AirportRepository getAirportRepo() {
		return AirportRepo;
	}

	protected void setAirportRepo(AirportRepository airportRepo) {
		AirportRepo = airportRepo;
	}

}
