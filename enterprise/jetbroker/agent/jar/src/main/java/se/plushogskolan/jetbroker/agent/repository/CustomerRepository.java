package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jee.utils.repository.BaseRepository;
import se.plushogskolan.jetbroker.agent.domain.Customer;

/**
 * Interface for customer repository
 * 
 * @author marcus
 *
 */
public interface CustomerRepository extends BaseRepository<Customer> {

	List<Customer> getAllCustomers();

}
