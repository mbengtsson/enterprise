package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.Customer;

/**
 * Interface for customer service
 * 
 * @author marcus
 *
 */
@Local
public interface CustomerService {

	Customer getCustomer(long id);

	void updateCustomer(Customer customer);

	Customer createCustomer(Customer customer);

	void removeCustomer(Customer customer);

	List<Customer> getAllCustomers();

}
