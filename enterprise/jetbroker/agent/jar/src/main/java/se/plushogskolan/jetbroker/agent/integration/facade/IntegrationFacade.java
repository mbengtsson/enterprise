package se.plushogskolan.jetbroker.agent.integration.facade;

import se.plushogskolan.jetbroker.agent.domain.messages.NewFlightRequestMessage;

/**
 * Facade for integration between the different systems
 * 
 * @author marcus
 *
 */
public interface IntegrationFacade {

	void sendNewFlightRequestMessage(NewFlightRequestMessage message);
}
