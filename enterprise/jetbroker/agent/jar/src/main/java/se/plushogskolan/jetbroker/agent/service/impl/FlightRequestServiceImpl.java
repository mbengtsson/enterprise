package se.plushogskolan.jetbroker.agent.service.impl;

import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.CONFIRMED;
import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.OFFER_RECEIVED;
import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.REJECTED;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.annotations.Prod;
import se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.cdi.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.domain.messages.NewFlightRequestMessage;
import se.plushogskolan.jetbroker.agent.domain.messages.OfferMessage;
import se.plushogskolan.jetbroker.agent.integration.facade.IntegrationFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Implementation of flight request service
 * 
 * @author marcus
 *
 */
@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	@Inject
	private FlightRequestRepository flightRequestRepo;

	@Inject
	private IntegrationFacade integrationFacade;

	@Inject
	@FlightRequestChanged(CONFIRMED)
	@Prod
	private Event<FlightRequestChangedEvent> confirmationEvent;

	@Inject
	@FlightRequestChanged(OFFER_RECEIVED)
	@Prod
	private Event<FlightRequestChangedEvent> offerEvent;

	@Inject
	@FlightRequestChanged(REJECTED)
	@Prod
	private Event<FlightRequestChangedEvent> rejectEvent;

	@Override
	public FlightRequest createFlightRequest(FlightRequest flightRequest) {

		long id = flightRequestRepo.persist(flightRequest);

		NewFlightRequestMessage message = new NewFlightRequestMessage(id, flightRequest.getPassengers(),
		        flightRequest.getDepartureAirportCode(), flightRequest.getArrivalAirportCode(), flightRequest.getDate());
		integrationFacade.sendNewFlightRequestMessage(message);

		return getFlightRequest(id);
	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return flightRequestRepo.findById(id);
	}

	@Override
	public void updateFlightRequest(FlightRequest flightRequest) {
		flightRequestRepo.update(flightRequest);
	}

	@Override
	public void removeFlightRequest(FlightRequest flightRequest) {
		flightRequestRepo.remove(flightRequest);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return flightRequestRepo.getAllFlightRequests();
	}

	@Override
	public List<FlightRequest> getFlightRequestsByStatus(Status status) {
		return flightRequestRepo.getFlightRequestsByStatus(status);
	}

	/**
	 * Handles flight-request confirmations, the flight-requests status is set
	 * to REQUEST_CONFIRMED and is updated in the DB, then a confirmation-event
	 * is fired
	 */
	@Override
	public void handleFlightRequestConfirmation(long flightRequestId, long confirmationId) {

		FlightRequest request = getFlightRequest(flightRequestId);
		request.setStatus(Status.REQUEST_CONFIRMED);
		updateFlightRequest(request);

		FlightRequestChangedEvent event = new FlightRequestChangedEvent();
		event.setRequestId(flightRequestId);
		confirmationEvent.fire(event);

	}

	/**
	 * Handles flight-request offers, the flight-requests status is set to
	 * OFFER_RECEIVED,price and plane-type is set and is updated in the DB, then
	 * a offer-event is fired
	 */
	@Override
	public void handleFlightRequestOffer(OfferMessage message) {

		FlightRequest request = getFlightRequest(message.getAgentId());
		request.setPrice(message.getPrice());
		request.setPlane(message.getPlaneTypeCode());
		request.setStatus(Status.OFFER_RECEIVED);
		updateFlightRequest(request);

		FlightRequestChangedEvent event = new FlightRequestChangedEvent();
		event.setRequestId(message.getAgentId());
		offerEvent.fire(event);

	}

	/**
	 * Handles flight-request rejections, the flight-requests status is set to
	 * REJECTED, price and plane-type is set to null and is updated in the DB,
	 * then a reject-event is fired
	 */
	@Override
	public void handleFlightRequestReject(long flightRequestId, long confirmationId) {

		FlightRequest request = getFlightRequest(flightRequestId);
		request.setPrice(0);
		request.setPlane(null);
		request.setStatus(Status.REJECTED);
		updateFlightRequest(request);

		FlightRequestChangedEvent event = new FlightRequestChangedEvent();
		event.setRequestId(flightRequestId);
		rejectEvent.fire(event);

	}

	protected FlightRequestRepository getFlightRequestRepo() {
		return flightRequestRepo;
	}

	protected void setFlightRequestRepo(FlightRequestRepository flightRequestRepo) {
		this.flightRequestRepo = flightRequestRepo;
	}

	protected IntegrationFacade getIntegrationFacade() {
		return integrationFacade;
	}

	protected void setIntegrationFacade(IntegrationFacade integrationFacade) {
		this.integrationFacade = integrationFacade;
	}

	protected Event<FlightRequestChangedEvent> getConfirmationEvent() {
		return confirmationEvent;
	}

	protected void setConfirmationEvent(Event<FlightRequestChangedEvent> confirmationEvent) {
		this.confirmationEvent = confirmationEvent;
	}

	protected Event<FlightRequestChangedEvent> getOfferEvent() {
		return offerEvent;
	}

	protected void setOfferEvent(Event<FlightRequestChangedEvent> offerEvent) {
		this.offerEvent = offerEvent;
	}

	protected Event<FlightRequestChangedEvent> getRejectEvent() {
		return rejectEvent;
	}

	protected void setRejectEvent(Event<FlightRequestChangedEvent> rejectEvent) {
		this.rejectEvent = rejectEvent;
	}

}
