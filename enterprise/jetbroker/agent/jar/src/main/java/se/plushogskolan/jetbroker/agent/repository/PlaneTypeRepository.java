package se.plushogskolan.jetbroker.agent.repository;

import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;

/**
 * Interface for plane type repository
 * 
 * @author marcus
 *
 */
public interface PlaneTypeRepository {

	PlaneType getPlaneTypeByCode(String code);

	List<PlaneType> getAllPlaneTypes();

}
