package se.plushogskolan.jetbroker.agent.integration.mdb.simulator;

import javax.ejb.Local;

/**
 * Interface for flight-request message simulator used to simulate incoming
 * flight-request messages
 * 
 * @author marcus
 *
 */
@Local
public interface MessageSimulator {

	void sendConfirmationMessage(long requestId);

	void sendOfferMessage(long requestId);

	void sendRejectMessage(long requestId);
}
