package se.plushogskolan.jetbroker.agent.domain;

/**
 * Class representing a Plane type. Not for persistance
 * 
 * @author marcus
 *
 */
public class PlaneType implements Comparable<PlaneType> {

	private String code;
	private String name;
	private int maxNoOfPassengers;
	private int maxRangeKm;
	private int maxSpeedKmH;
	private double fuelConsumptionPerKm;

	public PlaneType(String code, String name, int maxNoOfPassengers, int maxRangeKm, int maxSpeedKmH,
	        double fuelConsumptionPerKm) {
		this.code = code;
		this.name = name;
		this.maxNoOfPassengers = maxNoOfPassengers;
		this.maxRangeKm = maxRangeKm;
		this.maxSpeedKmH = maxSpeedKmH;
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}

	public int getMaxRangeKm() {
		return maxRangeKm;
	}

	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}

	public String getNiceName() {
		return String.format("%s (%d passengers)", getName(), getMaxNoOfPassengers());
	}

	@Override
	public String toString() {
		return String
		        .format("PlaneType [code=%s, name=%s, maxNoOfPassengers=%s, maxRangeKm=%s, maxSpeedKmH=%s, fuelConsumptionPerKm=%s]",
		                code, name, maxNoOfPassengers, maxRangeKm, maxSpeedKmH, fuelConsumptionPerKm);
	}

	@Override
	public int compareTo(PlaneType o) {
		return getName().compareToIgnoreCase(o.getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		long temp;
		temp = Double.doubleToLongBits(fuelConsumptionPerKm);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + maxNoOfPassengers;
		result = prime * result + maxRangeKm;
		result = prime * result + maxSpeedKmH;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlaneType other = (PlaneType) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (Double.doubleToLongBits(fuelConsumptionPerKm) != Double.doubleToLongBits(other.fuelConsumptionPerKm))
			return false;
		if (maxNoOfPassengers != other.maxNoOfPassengers)
			return false;
		if (maxRangeKm != other.maxRangeKm)
			return false;
		if (maxSpeedKmH != other.maxSpeedKmH)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
