package se.plushogskolan.jetbroker.agent.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

/**
 * Implementation of customer service
 * 
 * @author marcus
 *
 */
@Stateless
public class CustomerServiceImpl implements CustomerService {

	@Inject
	private CustomerRepository customerRepo;

	@Override
	public Customer getCustomer(long id) {
		return getCustomerRepo().findById(id);
	}

	@Override
	public void updateCustomer(Customer customer) {
		getCustomerRepo().update(customer);

	}

	@Override
	public Customer createCustomer(Customer customer) {
		long id = getCustomerRepo().persist(customer);
		return getCustomer(id);
	}

	@Override
	public void removeCustomer(Customer customer) {
		getCustomerRepo().remove(customer);

	}

	@Override
	public List<Customer> getAllCustomers() {
		return getCustomerRepo().getAllCustomers();
	}

	protected CustomerRepository getCustomerRepo() {
		return customerRepo;
	}

	protected void setCustomerRepo(CustomerRepository customerRepo) {
		this.customerRepo = customerRepo;
	}

}
