package se.plushogskolan.jetbroker.agent.integration.mdb;

import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.REJECTED;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.annotations.Mock;
import se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.cdi.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Mocked implementation of MDB used to recive flight-request rejections
 * 
 * @author marcus
 *
 */
@ApplicationScoped
public class MockRejectMdb {

	private static Logger log = Logger.getLogger(MockRejectMdb.class.getName());

	@Inject
	private FlightRequestService service;

	private long confirmationId = 1;

	public void onMessage(@Observes @FlightRequestChanged(REJECTED) @Mock FlightRequestChangedEvent event) {

		long id = confirmationId++;

		log.info("Mock: Flight-request rejected, confirmationID: " + id);
		service.handleFlightRequestReject(event.getRequestId(), id);

	}
}
