package se.plushogskolan.jetbroker.agent.integration.mdb;

import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.OFFER_RECEIVED;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.annotations.Mock;
import se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.cdi.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.domain.messages.OfferMessage;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Mocked implementation of MDB used to recive flight-request offers
 * 
 * @author marcus
 *
 */
@ApplicationScoped
public class MockOfferMdb {

	private static Logger log = Logger.getLogger(MockOfferMdb.class.getName());

	@Inject
	private FlightRequestService service;

	private long offerId = 1;

	public void onMessage(@Observes @FlightRequestChanged(OFFER_RECEIVED) @Mock FlightRequestChangedEvent event) {

		long id = offerId++;
		log.info("Mock: Offer received, offerID: " + id);
		OfferMessage message = new OfferMessage(event.getRequestId(), id, 100000, "A450");
		service.handleFlightRequestOffer(message);

	}
}
