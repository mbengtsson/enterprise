package se.plushogskolan.jetbroker.agent.log;

import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.CONFIRMED;
import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.OFFER_RECEIVED;
import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.REJECTED;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import se.plushogskolan.jee.utils.cdi.annotations.Prod;
import se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.cdi.events.FlightRequestChangedEvent;

/**
 * Class used to log flight-request changes, observes FlightRequestChanged
 * events
 * 
 * @author marcus
 *
 */
@ApplicationScoped
public class FlightRequestLogger {

	private static Logger log = Logger.getLogger(FlightRequestLogger.class.getName());

	private int confirmations;
	private int offers;
	private int rejections;

	private void showLog() {
		log.info("---------[FlightRequestLogger]---------");
		log.info("Flight request confirmations: " + confirmations);
		log.info("Flight request offers:        " + offers);
		log.info("Flight request rejections:    " + rejections);
		log.info("---------------------------------------");
	}

	public void logConfirmation(@Observes @FlightRequestChanged(CONFIRMED) @Prod FlightRequestChangedEvent event) {

		confirmations++;
		showLog();
	}

	public void logOffer(@Observes @FlightRequestChanged(OFFER_RECEIVED) @Prod FlightRequestChangedEvent event) {

		offers++;
		showLog();

	}

	public void logRejection(@Observes @FlightRequestChanged(REJECTED) @Prod FlightRequestChangedEvent event) {

		rejections++;
		showLog();

	}

}
