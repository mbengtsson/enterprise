package se.plushogskolan.jetbroker.agent.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.plushogskolan.jee.utils.domain.IdHolder;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirport;
import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;

/**
 * Class representing a Flight-request entity
 * 
 * @author marcus
 *
 */
@Entity
@ArrivalAndDepartureAirport
@NamedQueries({ @NamedQuery(name = "getFlightRequestsByStatus", query = "select f from FlightRequest f where f.status = :status") })
public class FlightRequest implements IdHolder, Comparable<FlightRequest>, ArrivalAndDepartureAirportHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	@ManyToOne()
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@Range(min = 1, max = 500)
	private int passengers;

	@NotBlank
	private String departureAirportCode;

	@NotBlank
	private String arrivalAirportCode;

	private String plane;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime date;

	private int price;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;

	public FlightRequest() {
		setStatus(Status.CREATED);
	}

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getPassengers() {
		return passengers;
	}

	public void setPassengers(int passengers) {
		this.passengers = passengers;
	}

	@Override
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	@Override
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getPlane() {
		return plane;
	}

	public void setPlane(String plane) {
		this.plane = plane;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getNiceDate() {
		DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("YYYY-MMM-dd HH:mm");
		return dateFormatter.print(date);
	}

	public String getNiceStatus() {
		return status.getNiceStatus();
	}

	@Override
	public String toString() {
		return String
		        .format("FlightRequest [id=%s, customer=%s, passengers=%s, departureAirport=%s, arrivalAirport=%s, plane=%s, date=%s, price=%s, status=%s]",
		                id, customer, passengers, departureAirportCode, arrivalAirportCode, plane, date, price,
		                status);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arrivalAirportCode == null) ? 0 : arrivalAirportCode.hashCode());
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((departureAirportCode == null) ? 0 : departureAirportCode.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + passengers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FlightRequest other = (FlightRequest) obj;
		if (arrivalAirportCode == null) {
			if (other.arrivalAirportCode != null) {
				return false;
			}
		} else if (!arrivalAirportCode.equals(other.arrivalAirportCode)) {
			return false;
		}
		if (customer == null) {
			if (other.customer != null) {
				return false;
			}
		} else if (!customer.equals(other.customer)) {
			return false;
		}
		if (date == null) {
			if (other.date != null) {
				return false;
			}
		} else if (!date.equals(other.date)) {
			return false;
		}
		if (departureAirportCode == null) {
			if (other.departureAirportCode != null) {
				return false;
			}
		} else if (!departureAirportCode.equals(other.departureAirportCode)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (passengers != other.passengers) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(FlightRequest o) {

		if (date.isBefore(o.getDate())) {
			return -1;
		} else if (date.isAfter(o.getDate())) {
			return 1;
		} else {
			return 0;
		}
	}

	public enum Status {
		CREATED("New"),
		REQUEST_CONFIRMED("Request confirmed"),
		OFFER_RECEIVED("Offer received"),
		REJECTED("Rejected");

		private String niceStatus;

		Status(String niceStatus) {
			this.niceStatus = niceStatus;
		}

		private String getNiceStatus() {
			return niceStatus;
		}

	};

}
