package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.Airport;

/**
 * Interface for airport service
 * 
 * @author marcus
 *
 */
@Local
public interface AirportService {

	Airport getAirportByCode(String code);

	List<Airport> getAllAirports();

}
