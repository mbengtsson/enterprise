package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;

/**
 * Interface for plane type service
 * 
 * @author marcus
 *
 */
@Local
public interface PlaneTypeService {

	PlaneType getPlaneTypeByCode(String code);

	List<PlaneType> getAllPlaneTypes();

}
