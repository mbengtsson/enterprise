package se.plushogskolan.jetbroker.agent.integration.mdb.simulator;

import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.CONFIRMED;
import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.OFFER_RECEIVED;
import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.REJECTED;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.annotations.Mock;
import se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.cdi.events.FlightRequestChangedEvent;

/**
 * Implementation of flight-request message simulator used to simulate incoming
 * flight-request messages
 * 
 * @author marcus
 *
 */
@Stateless
public class MessageSimulatorImpl implements MessageSimulator {

	@Inject
	@FlightRequestChanged(CONFIRMED)
	@Mock
	private Event<FlightRequestChangedEvent> confirmationEvent;

	@Inject
	@FlightRequestChanged(OFFER_RECEIVED)
	@Mock
	private Event<FlightRequestChangedEvent> offerEvent;

	@Inject
	@FlightRequestChanged(REJECTED)
	@Mock
	private Event<FlightRequestChangedEvent> rejectEvent;

	@Override
	public void sendConfirmationMessage(long requestId) {

		FlightRequestChangedEvent event = new FlightRequestChangedEvent();
		event.setRequestId(requestId);
		confirmationEvent.fire(event);

	}

	@Override
	public void sendOfferMessage(long requestId) {

		FlightRequestChangedEvent event = new FlightRequestChangedEvent();
		event.setRequestId(requestId);
		offerEvent.fire(event);

	}

	@Override
	public void sendRejectMessage(long requestId) {

		FlightRequestChangedEvent event = new FlightRequestChangedEvent();
		event.setRequestId(requestId);
		rejectEvent.fire(event);

	}

}
