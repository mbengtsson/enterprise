package se.plushogskolan.jetbroker.agent.integration.mdb;

import static se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged.StatusChange.CONFIRMED;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.annotations.Mock;
import se.plushogskolan.jetbroker.agent.cdi.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.cdi.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.service.FlightRequestService;

/**
 * Mocked implementation of MDB used to recive flight-request confirmations
 * 
 * @author marcus
 *
 */
@ApplicationScoped
public class MockConfirmationMdb {

	private static Logger log = Logger.getLogger(MockConfirmationMdb.class.getName());

	@Inject
	private FlightRequestService service;

	private long confirmationId = 1;

	public void onMessage(@Observes @FlightRequestChanged(CONFIRMED) @Mock FlightRequestChangedEvent event) {

		long id = confirmationId++;

		log.info("Mock: Requst confirmed, confirmationID: " + id);
		service.handleFlightRequestConfirmation(event.getRequestId(), id);

	}
}
