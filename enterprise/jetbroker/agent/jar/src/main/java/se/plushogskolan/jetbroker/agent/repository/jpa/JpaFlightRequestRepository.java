package se.plushogskolan.jetbroker.agent.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

/**
 * Implementation of flight request repository using JPA for persistance
 * 
 * @author marcus
 *
 */
public class JpaFlightRequestRepository extends JpaRepository<FlightRequest> implements FlightRequestRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return em.createQuery("select f from FlightRequest f").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getFlightRequestsByStatus(Status status) {
		Query query = em.createNamedQuery("getFlightRequestsByStatus");
		query.setParameter("status", status);
		return query.getResultList();
	}

}
