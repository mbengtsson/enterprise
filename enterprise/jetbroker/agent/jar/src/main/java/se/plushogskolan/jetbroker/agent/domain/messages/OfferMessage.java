package se.plushogskolan.jetbroker.agent.domain.messages;

/**
 * Message used for communication between offer-Mdb and flight-request service
 * containing information about a new flight-request offer
 * 
 * @author marcus
 *
 */
public class OfferMessage {

	private long agentId;
	private long offerId;
	private int price;
	private String planeTypeCode;

	public OfferMessage(long agentId, long offerId, int price, String planeTypeCode) {
		super();
		this.agentId = agentId;
		this.offerId = offerId;
		this.price = price;
		this.planeTypeCode = planeTypeCode;
	}

	public long getAgentId() {
		return agentId;
	}

	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	@Override
	public String toString() {
		return String.format("OfferMessage [agentId=%s, offerId=%s, price=%s, planeTypeCode=%s]", agentId, offerId,
		        price, planeTypeCode);
	}

}
