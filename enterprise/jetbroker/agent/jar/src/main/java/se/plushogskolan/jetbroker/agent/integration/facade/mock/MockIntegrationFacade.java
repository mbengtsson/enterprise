package se.plushogskolan.jetbroker.agent.integration.facade.mock;

import java.util.logging.Logger;

import se.plushogskolan.jetbroker.agent.domain.messages.NewFlightRequestMessage;
import se.plushogskolan.jetbroker.agent.integration.facade.IntegrationFacade;

/**
 * Mocked implementation of the integration-facade
 * 
 * @author marcus
 *
 */
public class MockIntegrationFacade implements IntegrationFacade {

	private static Logger log = Logger.getLogger(MockIntegrationFacade.class.getName());

	@Override
	public void sendNewFlightRequestMessage(NewFlightRequestMessage message) {

		log.info("New flight request created: " + message);

	}

}
