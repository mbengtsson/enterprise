package se.plushogskolan.jetbroker.agent.domain.messages;

import org.joda.time.DateTime;

/**
 * Message used for communication between flight-request service and integration
 * facade containing information about a new flight-request
 * 
 * @author marcus
 *
 */
public class NewFlightRequestMessage {

	private long id;
	private int passengers;
	private String departureAirportCode;
	private String arrivalAirportCode;
	private DateTime date;

	public NewFlightRequestMessage(long id, int passengers, String departureAirportCode, String arrivalAirportCode,
	        DateTime date) {
		this.id = id;
		this.passengers = passengers;
		this.departureAirportCode = departureAirportCode;
		this.arrivalAirportCode = arrivalAirportCode;
		this.date = date;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getPassengers() {
		return passengers;
	}

	public void setPassengers(int passengers) {
		this.passengers = passengers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public DateTime getTime() {
		return date;
	}

	public void setTime(DateTime time) {
		this.date = time;
	}

	@Override
	public String toString() {
		return String
		        .format("MockFlightRequestMessage [id=%s, passengers=%s, departureAirportCode=%s, arrivalAirportCode=%s, date=%s]",
		                id, passengers, departureAirportCode, arrivalAirportCode, date);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arrivalAirportCode == null) ? 0 : arrivalAirportCode.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((departureAirportCode == null) ? 0 : departureAirportCode.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + passengers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NewFlightRequestMessage other = (NewFlightRequestMessage) obj;
		if (arrivalAirportCode == null) {
			if (other.arrivalAirportCode != null) {
				return false;
			}
		} else if (!arrivalAirportCode.equals(other.arrivalAirportCode)) {
			return false;
		}
		if (date == null) {
			if (other.date != null) {
				return false;
			}
		} else if (!date.equals(other.date)) {
			return false;
		}
		if (departureAirportCode == null) {
			if (other.departureAirportCode != null) {
				return false;
			}
		} else if (!departureAirportCode.equals(other.departureAirportCode)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (passengers != other.passengers) {
			return false;
		}
		return true;
	}

}
