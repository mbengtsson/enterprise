package se.plushogskolan.jetbroker.agent.domain;

/**
 * Class representing an Airport. Not for persistance
 * 
 * @author marcus
 *
 */
public class Airport implements Comparable<Airport> {

	private String code;
	private String name;
	private double latitude;
	private double longitude;

	public Airport(String code, String name, double latitude, double longitude) {
		this.code = code;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String getFullName() {
		return String.format("%s - %s", code, name);
	}

	@Override
	public String toString() {
		return String.format("Airport [code=%s, name=%s, latitude=%s, longitude=%s]", code, name, latitude, longitude);
	}

	@Override
	public int compareTo(Airport o) {
		return getCode().compareToIgnoreCase(o.getCode());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		long temp;
		temp = Double.doubleToLongBits(latitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(longitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airport other = (Airport) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (Double.doubleToLongBits(latitude) != Double.doubleToLongBits(other.latitude))
			return false;
		if (Double.doubleToLongBits(longitude) != Double.doubleToLongBits(other.longitude))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
