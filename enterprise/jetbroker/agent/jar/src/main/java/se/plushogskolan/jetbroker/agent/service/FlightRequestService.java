package se.plushogskolan.jetbroker.agent.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.domain.messages.OfferMessage;

/**
 * Interface for flight request service
 * 
 * @author marcus
 *
 */
@Local
public interface FlightRequestService {

	FlightRequest createFlightRequest(FlightRequest flightRequest);

	FlightRequest getFlightRequest(long id);

	void updateFlightRequest(FlightRequest flightRequest);

	void removeFlightRequest(FlightRequest flightRequest);

	List<FlightRequest> getAllFlightRequests();

	List<FlightRequest> getFlightRequestsByStatus(Status status);

	void handleFlightRequestConfirmation(long flightRequestId, long confirmationId);

	void handleFlightRequestOffer(OfferMessage message);

	void handleFlightRequestReject(long flightRequestId, long confirmationId);

}
