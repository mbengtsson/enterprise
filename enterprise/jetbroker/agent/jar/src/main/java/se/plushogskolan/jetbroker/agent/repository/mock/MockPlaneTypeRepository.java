package se.plushogskolan.jetbroker.agent.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

/**
 * Mocked implementation of plane type repository
 * 
 * @author marcus
 *
 */
public class MockPlaneTypeRepository implements PlaneTypeRepository {

	private List<PlaneType> planeTypes = new ArrayList<PlaneType>();

	public MockPlaneTypeRepository() {
		mockPlaneTypes();
	}

	private void mockPlaneTypes() {

		planeTypes.add(new PlaneType("B770", "Boeing 770", 300, 9700, 890, 22));
		planeTypes.add(new PlaneType("A450", "Airbus 450", 250, 8500, 930, 19));
	}

	@Override
	public PlaneType getPlaneTypeByCode(String code) {
		for (PlaneType planeType : planeTypes) {
			if (planeType.getCode().equals(code)) {
				return planeType;
			}
		}
		return null;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return planeTypes;
	}

}
