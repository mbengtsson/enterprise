package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaCustomerRepositoryIntegrationTest extends AbstractRepositoryTest<Customer, JpaCustomerRepository> {

	@Inject
	JpaCustomerRepository repo;

	@Override
	protected JpaCustomerRepository getRepository() {
		return repo;
	}

	@Override
	protected Customer getEntity1() {
		return new TestFixture.ValidCustomerBuilder().build();
	}

	@Override
	protected Customer getEntity2() {
		return new TestFixture.ValidCustomerBuilder().firstName("Bob").lastName("Smith").build();
	}

	@Test
	public void testGetAllCustomers() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		assertEquals("List is correct size", 2, repo.getAllCustomers().size());
	}

	@Test(expected = Exception.class)
	public void testPersistInvalidCustomer() {
		Customer customer = new TestFixture.ValidCustomerBuilder().firstName("").lastName(null).build();
		repo.persist(customer);
	}

}
