package se.plushogskolan.jetbroker.agent.repository.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.service.CustomerService;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaFlightRequestRepositoryIntegrationTest extends
        AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepository> {

	@Inject
	JpaFlightRequestRepository repo;

	@Inject
	CustomerService customerService;

	Customer customer1;
	Customer customer2;

	@Before
	public void setup() {
		customer1 = customerService.createCustomer(new TestFixture.ValidCustomerBuilder().build());
		customer2 = customerService.createCustomer(new TestFixture.ValidCustomerBuilder().firstName("James")
		        .lastName("Smith").build());
	}

	@Override
	protected JpaFlightRequestRepository getRepository() {
		return repo;
	}

	@Override
	protected FlightRequest getEntity1() {
		return new TestFixture.ValidFlightRequestBuilder(customer1).status(Status.REJECTED).build();
	}

	@Override
	protected FlightRequest getEntity2() {
		return new TestFixture.ValidFlightRequestBuilder(customer2).status(Status.REQUEST_CONFIRMED).build();
	}

	@Test
	public void testGetAllFlightRequests() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		assertEquals("List is correct size", 2, repo.getAllFlightRequests().size());
	}

	@Test
	public void testGetFlightRequestsByStatusRejected() {
		FlightRequest request1 = repo.findById(repo.persist(getEntity1()));
		FlightRequest request2 = repo.findById(repo.persist(getEntity2()));

		assertEquals("List is correct size", 1, repo.getFlightRequestsByStatus(Status.REJECTED).size());
		assertTrue(repo.getFlightRequestsByStatus(Status.REJECTED).contains(request1));
		assertFalse(repo.getFlightRequestsByStatus(Status.REJECTED).contains(request2));
	}

	@Test
	public void testGetFlightRequestsByStatusConfirmed() {
		FlightRequest request1 = repo.findById(repo.persist(getEntity1()));
		FlightRequest request2 = repo.findById(repo.persist(getEntity2()));

		assertEquals("List is correct size", 1, repo.getFlightRequestsByStatus(Status.REQUEST_CONFIRMED).size());
		assertTrue(repo.getFlightRequestsByStatus(Status.REQUEST_CONFIRMED).contains(request2));
		assertFalse(repo.getFlightRequestsByStatus(Status.REQUEST_CONFIRMED).contains(request1));
	}

	@Test
	public void testPersistAndFindFlightRequest() {
		DateTime date = DateTime.now();
		long id = repo.persist(new TestFixture.ValidFlightRequestBuilder(customer2).date(date).status(Status.CREATED)
		        .build());

		FlightRequest request = repo.findById(id);

		assertEquals("Correct customer", customer2, request.getCustomer());
		assertEquals("Correct date", date, request.getDate());
		assertEquals("Correct status", Status.CREATED, request.getStatus());
	}

	@Test(expected = Exception.class)
	public void testPersistInvalidFlightRequest() {
		FlightRequest request = new TestFixture.ValidFlightRequestBuilder(null).arrivalAirport("")
		        .departureAirport(null).build();
		repo.persist(request);
	}

}
