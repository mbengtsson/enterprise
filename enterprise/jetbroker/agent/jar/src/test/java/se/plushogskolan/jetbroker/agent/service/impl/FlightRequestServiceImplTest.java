package se.plushogskolan.jetbroker.agent.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.event.Event;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.cdi.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.agent.domain.messages.NewFlightRequestMessage;
import se.plushogskolan.jetbroker.agent.domain.messages.OfferMessage;
import se.plushogskolan.jetbroker.agent.integration.facade.IntegrationFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

	private FlightRequest flightRequest;
	private List<FlightRequest> flightRequests;
	private FlightRequestServiceImpl flightRequestService;

	@Before
	public void setup() {
		flightRequest = new TestFixture.ValidFlightRequestBuilder(null).build();
		flightRequests = new ArrayList<FlightRequest>();
		flightRequests.add(flightRequest);
		flightRequestService = new FlightRequestServiceImpl();
	}

	@Test
	public void testGetFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1)).andReturn(flightRequest);
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		assertEquals("get customer in return", flightRequest, flightRequestService.getFlightRequest(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testUpdateFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		repo.update(flightRequest);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		flightRequestService.updateFlightRequest(flightRequest);

		// Verify
		verify(repo);

	}

	@Test
	public void testCreateFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.persist(flightRequest)).andReturn(1L);
		expect(repo.findById(1)).andReturn(flightRequest);
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Setup mock integration facade
		IntegrationFacade facade = createMock(IntegrationFacade.class);
		facade.sendNewFlightRequestMessage(new NewFlightRequestMessage(1, flightRequest.getPassengers(), flightRequest
		        .getDepartureAirportCode(), flightRequest.getArrivalAirportCode(), flightRequest.getDate()));
		expectLastCall();
		replay(facade);
		flightRequestService.setIntegrationFacade(facade);

		// Perform test
		assertEquals("get customer in return", flightRequest, flightRequestService.createFlightRequest(flightRequest));

		// Verify
		verify(repo);

	}

	@Test
	public void testRemoveFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		repo.remove(flightRequest);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		flightRequestService.removeFlightRequest(flightRequest);

		// Verify
		verify(repo);

	}

	@Test
	public void testGetAllFlightRequests() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.getAllFlightRequests()).andReturn(flightRequests);
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		assertEquals("get customer list in return", flightRequests, flightRequestService.getAllFlightRequests());

		// Verify
		verify(repo);
	}

	@Test
	public void testGetFlightRequestsByStatus() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.getFlightRequestsByStatus(Status.CREATED)).andReturn(flightRequests);
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		assertEquals("get customer list in return", flightRequests,
		        flightRequestService.getFlightRequestsByStatus(Status.CREATED));
		// Verify
		verify(repo);
	}

	@Test
	public void testHandleFlightRequestConfirmation() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1)).andReturn(flightRequest);
		repo.update(flightRequest);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Setup mock event
		@SuppressWarnings("unchecked")
		Event<FlightRequestChangedEvent> event = createMock(Event.class);
		event.fire(new FlightRequestChangedEvent(1));
		expectLastCall();
		replay(event);
		flightRequestService.setConfirmationEvent(event);

		// Perform test
		flightRequestService.handleFlightRequestConfirmation(1, 1);
		assertEquals("Status is request confirmed", Status.REQUEST_CONFIRMED, flightRequest.getStatus());

		// Verify
		verify(repo);
		verify(event);

	}

	@Test
	public void testHandleFlightRequestOffer() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1)).andReturn(flightRequest);
		repo.update(flightRequest);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Setup mock event
		@SuppressWarnings("unchecked")
		Event<FlightRequestChangedEvent> event = createMock(Event.class);
		event.fire(new FlightRequestChangedEvent(1));
		expectLastCall();
		replay(event);
		flightRequestService.setOfferEvent(event);

		// Perform test
		flightRequestService.handleFlightRequestOffer(new OfferMessage(1, 1, 100000, "ABC"));
		assertEquals("Price is correct", 100000, flightRequest.getPrice());
		assertEquals("Plane is correct", "ABC", flightRequest.getPlane());
		assertEquals("Status is offer received", Status.OFFER_RECEIVED, flightRequest.getStatus());

		// Verify
		verify(repo);
		verify(event);

	}

	@Test
	public void testHandleFlightRequestReject() {

		// Set dummy data in flightRequest
		flightRequest.setPrice(10000);
		flightRequest.setPlane("ABC");

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1)).andReturn(flightRequest);
		repo.update(flightRequest);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Setup mock event
		@SuppressWarnings("unchecked")
		Event<FlightRequestChangedEvent> event = createMock(Event.class);
		event.fire(new FlightRequestChangedEvent(1));
		expectLastCall();
		replay(event);
		flightRequestService.setRejectEvent(event);

		// Perform test
		flightRequestService.handleFlightRequestReject(1, 1);
		assertEquals("Price is correct", 0, flightRequest.getPrice());
		assertNull("Plane is null", flightRequest.getPlane());
		assertEquals("Status is offer received", Status.REJECTED, flightRequest.getStatus());

		// Verify
		verify(repo);
		verify(event);

	}
}
