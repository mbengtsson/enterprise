package se.plushogskolan.jetbroker;

import java.util.logging.Logger;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;

public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.getName());

	public static Archive<?> createIntegrationTestArchive() {
		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
		        .loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "agent_test.war").addPackages(true, "se.plushogskolan")
		        .addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}

	public static class ValidCustomerBuilder {

		private long id = 0;
		private String firstName = "John";
		private String lastName = "Doe";
		private String email = "mail@example.com";
		private String company = "Acme";

		public ValidCustomerBuilder id(long id) {
			this.id = id;
			return this;
		}

		public ValidCustomerBuilder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public ValidCustomerBuilder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public ValidCustomerBuilder email(String email) {
			this.email = email;
			return this;
		}

		public ValidCustomerBuilder company(String company) {
			this.company = company;
			return this;
		}

		public Customer build() {
			Customer customer = new Customer();
			customer.setId(id);
			customer.setFirstName(firstName);
			customer.setLastName(lastName);
			customer.setEmail(email);
			customer.setCompany(company);
			return customer;
		}

	}

	public static class ValidFlightRequestBuilder {

		private long id = 0;
		private Customer customer;
		private int passengers = 5;
		private String departureAirport = "GOT";
		private String arrivalAirport = "ARN";
		private String plane = "A plane";
		private DateTime date = DateTime.now();
		private int price = 0;
		private Status status = Status.CREATED;

		public ValidFlightRequestBuilder(Customer customer) {
			this.customer = customer;
		}

		public ValidFlightRequestBuilder id(long id) {
			this.id = id;
			return this;
		}

		public ValidFlightRequestBuilder passengers(int passengers) {
			this.passengers = passengers;
			return this;
		}

		public ValidFlightRequestBuilder departureAirport(String departureAirport) {
			this.departureAirport = departureAirport;
			return this;
		}

		public ValidFlightRequestBuilder arrivalAirport(String arrivalAirport) {
			this.arrivalAirport = arrivalAirport;
			return this;
		}

		public ValidFlightRequestBuilder plane(String plane) {
			this.plane = plane;
			return this;
		}

		public ValidFlightRequestBuilder date(DateTime date) {
			this.date = date;
			return this;
		}

		public ValidFlightRequestBuilder price(int price) {
			this.price = price;
			return this;
		}

		public ValidFlightRequestBuilder status(Status status) {
			this.status = status;
			return this;
		}

		public FlightRequest build() {
			FlightRequest request = new FlightRequest();
			request.setId(id);
			request.setCustomer(customer);
			request.setPassengers(passengers);
			request.setDepartureAirportCode(departureAirport);
			request.setArrivalAirportCode(arrivalAirport);
			request.setPlane(plane);
			request.setDate(date);
			request.setPrice(price);
			request.setStatus(status);
			return request;
		}

	}

}
