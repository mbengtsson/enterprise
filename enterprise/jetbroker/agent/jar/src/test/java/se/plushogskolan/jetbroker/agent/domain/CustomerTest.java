package se.plushogskolan.jetbroker.agent.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.PropertyValidator;
import se.plushogskolan.jetbroker.TestFixture;

public class CustomerTest {

	PropertyValidator<Customer> validator;

	@Before
	public void setup() {
		validator = new PropertyValidator<Customer>();
	}

	@Test
	public void testEmptyConstructor() {
		Customer customer = new Customer();

		assertEquals(0, customer.getId());
		assertNull(customer.getFirstName());
		assertNull(customer.getLastName());
		assertNull(customer.getEmail());
		assertNull(customer.getCompany());
	}

	@Test
	public void testTwoParamConstructor() {
		Customer customer = new Customer("John", "Doe");

		assertEquals(0, customer.getId());
		assertEquals("John", customer.getFirstName());
		assertEquals("Doe", customer.getLastName());
		assertNull(customer.getEmail());
		assertNull(customer.getCompany());
	}

	@Test
	public void testFourParamConstructor() {
		Customer customer = new Customer("John", "Doe", "mail@example.com", "Acme");

		assertEquals(0, customer.getId());
		assertEquals("John", customer.getFirstName());
		assertEquals("Doe", customer.getLastName());
		assertEquals("mail@example.com", customer.getEmail());
		assertEquals("Acme", customer.getCompany());
	}

	@Test
	public void testValidationFirstNameEmpty() {
		Customer customer = new TestFixture.ValidCustomerBuilder().firstName("").build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		validator.assertPropertyIsInvalid("firstName", violations);
	}

	@Test
	public void testValidationFirstNameNull() {
		Customer customer = new TestFixture.ValidCustomerBuilder().firstName(null).build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		validator.assertPropertyIsInvalid("firstName", violations);
	}

	@Test
	public void testValidationFirstNameOk() {
		Customer customer = new TestFixture.ValidCustomerBuilder().build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		assertTrue("No validation errors for firstName", violations.isEmpty());
	}

	@Test
	public void testValidationLastNameEmpty() {
		Customer customer = new TestFixture.ValidCustomerBuilder().lastName("").build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		validator.assertPropertyIsInvalid("lastName", violations);
	}

	@Test
	public void testValidationLastNameNull() {
		Customer customer = new TestFixture.ValidCustomerBuilder().lastName(null).build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		validator.assertPropertyIsInvalid("lastName", violations);
	}

	@Test
	public void testValidationLastNameOk() {
		Customer customer = new TestFixture.ValidCustomerBuilder().build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		assertTrue("No validation errors for lastName", violations.isEmpty());
	}

	@Test
	public void testValidationEmailEmpty() {
		Customer customer = new TestFixture.ValidCustomerBuilder().email("").build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		validator.assertPropertyIsInvalid("email", violations);
	}

	@Test
	public void testValidationEmailNull() {
		Customer customer = new TestFixture.ValidCustomerBuilder().email(null).build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		validator.assertPropertyIsInvalid("email", violations);
	}

	@Test
	public void testValidationEmailInvalid() {
		Customer customer = new TestFixture.ValidCustomerBuilder().email("invalid_mail").build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		validator.assertPropertyIsInvalid("email", violations);
	}

	@Test
	public void testValidationEmailOk() {
		Customer customer = new TestFixture.ValidCustomerBuilder().build();
		Set<ConstraintViolation<Customer>> violations = validator.getValidator().validate(
		        customer);
		assertTrue("No validation errors for email", violations.isEmpty());
	}

}
