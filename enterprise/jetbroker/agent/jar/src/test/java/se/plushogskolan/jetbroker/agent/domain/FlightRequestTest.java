package se.plushogskolan.jetbroker.agent.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.PropertyValidator;
import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.FlightRequest.Status;

public class FlightRequestTest {

	PropertyValidator<FlightRequest> validator;
	Customer customer;

	@Before
	public void setup() {
		validator = new PropertyValidator<FlightRequest>();
		customer = new TestFixture.ValidCustomerBuilder().build();
	}

	@Test
	public void testConstructor() {
		FlightRequest request = new FlightRequest();

		assertEquals("Status is set to CREATED", Status.CREATED, request.getStatus());
	}

	@Test
	public void testValidationCustomerNull() {
		FlightRequest request = new TestFixture.ValidFlightRequestBuilder(null).build();
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(
		        request);
		validator.assertPropertyIsInvalid("customer", violations);
	}

	@Test
	public void testValidationCustomerOk() {
		FlightRequest request = new TestFixture.ValidFlightRequestBuilder(customer).build();
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(
		        request);
		assertTrue("No validation errors for customer", violations.isEmpty());
	}

	@Test
	public void testValidationPassengersLow() {
		FlightRequest request = new TestFixture.ValidFlightRequestBuilder(customer).passengers(0).build();
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(
		        request);
		validator.assertPropertyIsInvalid("passengers", violations);
	}

	@Test
	public void testValidationPassengersHigh() {
		FlightRequest request = new TestFixture.ValidFlightRequestBuilder(customer).passengers(501).build();
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(
		        request);
		validator.assertPropertyIsInvalid("passengers", violations);
	}

	@Test
	public void testValidationPassengersOk() {
		FlightRequest request = new TestFixture.ValidFlightRequestBuilder(customer).build();
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(
		        request);
		assertTrue("No validation errors for passengers", violations.isEmpty());
	}

	@Test
	public void testValidationArrivalDepartureActive() {
		FlightRequest request = new TestFixture.ValidFlightRequestBuilder(customer).arrivalAirport("GOT")
		        .departureAirport("GOT").build();
		Set<ConstraintViolation<FlightRequest>> violations = validator.getValidator().validate(request);
		validator.assertPropertyIsInvalid("", violations);

		request.setArrivalAirportCode("ARN");
		violations = validator.getValidator().validate(request);
		assertTrue("No validation errors for arrival and departure", violations.isEmpty());

	}

}
