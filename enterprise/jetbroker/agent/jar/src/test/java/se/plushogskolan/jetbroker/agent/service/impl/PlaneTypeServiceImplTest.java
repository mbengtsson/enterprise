package se.plushogskolan.jetbroker.agent.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

public class PlaneTypeServiceImplTest {

	private PlaneType planeType;
	private List<PlaneType> planeTypes;
	private PlaneTypeServiceImpl planeTypeService;

	@Before
	public void setup() {
		planeType = new PlaneType("B770", "Boeing 770", 300, 9700, 890, 22);
		planeTypes = new ArrayList<PlaneType>();
		planeTypes.add(planeType);
		planeTypeService = new PlaneTypeServiceImpl();
	}

	@Test
	public void testGetPlaneTypeByCode() {
		// Setup mock repo
		PlaneTypeRepository repo = createMock(PlaneTypeRepository.class);
		expect(repo.getPlaneTypeByCode("B770")).andReturn(planeType);
		replay(repo);
		planeTypeService.setPlaneTypeRepo(repo);

		// Perform test
		assertEquals("Get plane-type in return", planeType, planeTypeService.getPlaneTypeByCode("B770"));

		// Verify
		verify(repo);

	}

	@Test
	public void testGetAllPlaneTypes() {
		// Setup mock repo
		PlaneTypeRepository repo = createMock(PlaneTypeRepository.class);
		expect(repo.getAllPlaneTypes()).andReturn(planeTypes);
		replay(repo);
		planeTypeService.setPlaneTypeRepo(repo);

		// Perform test
		assertEquals("Get all plane types in return", planeTypes, planeTypeService.getAllPlaneTypes());

		// Verify
		verify(repo);

	}
}
