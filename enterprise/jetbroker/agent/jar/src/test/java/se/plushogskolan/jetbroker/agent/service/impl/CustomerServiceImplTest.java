package se.plushogskolan.jetbroker.agent.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.repository.CustomerRepository;
import se.plushogskolan.jetbroker.agent.service.impl.CustomerServiceImpl;

public class CustomerServiceImplTest {

	private Customer customer;
	private List<Customer> customers;
	private CustomerServiceImpl customerService;

	@Before
	public void setup() {
		customer = new TestFixture.ValidCustomerBuilder().build();
		customers = new ArrayList<Customer>();
		customers.add(customer);
		customerService = new CustomerServiceImpl();
	}

	@Test
	public void testGetCustomer() {

		// Setup mock repo
		CustomerRepository repo = createMock(CustomerRepository.class);
		expect(repo.findById(1)).andReturn(customer);
		replay(repo);
		customerService.setCustomerRepo(repo);

		// Perform test
		assertEquals("get customer in return", customer, customerService.getCustomer(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testUpdateCustomer() {

		// Setup mock repo
		CustomerRepository repo = createMock(CustomerRepository.class);
		repo.update(customer);
		expectLastCall();
		replay(repo);
		customerService.setCustomerRepo(repo);

		// Perform test
		customerService.updateCustomer(customer);

		// Verify
		verify(repo);

	}

	@Test
	public void testCreateCustomer() {

		// Setup mock repo
		CustomerRepository repo = createMock(CustomerRepository.class);
		expect(repo.persist(customer)).andReturn(1L);
		expect(repo.findById(1)).andReturn(customer);
		replay(repo);
		customerService.setCustomerRepo(repo);

		// Perform test
		assertEquals("get customer in return", customer, customerService.createCustomer(customer));

		// Verify
		verify(repo);

	}

	@Test
	public void testRemoveCustomer() {

		// Setup mock repo
		CustomerRepository repo = createMock(CustomerRepository.class);
		repo.remove(customer);
		expectLastCall();
		replay(repo);
		customerService.setCustomerRepo(repo);

		// Perform test
		customerService.removeCustomer(customer);

		// Verify
		verify(repo);

	}

	@Test
	public void testGetAllCustomers() {

		// Setup mock repo
		CustomerRepository repo = createMock(CustomerRepository.class);
		expect(repo.getAllCustomers()).andReturn(customers);
		replay(repo);
		customerService.setCustomerRepo(repo);

		// Perform test
		assertEquals("get customer list in return", customers, customerService.getAllCustomers());
	}

}
