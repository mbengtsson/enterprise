package se.plushogskolan.jetbroker.order.mvc.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.service.AirportService;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelCostService;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

/**
 * Spring MVC conrtroller for the index page of the order system
 * 
 * @author marcus
 *
 */
@Controller
public class IndexController {

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private AirportService airportService;

	@Inject
	private PlaneService planeService;

	@Inject
	private PlaneTypeService planeTypeService;

	@Inject
	private FuelCostService fuelCostService;

	@RequestMapping("/index.html")
	public ModelAndView index() {

		List<FlightRequestWrapper> flightRequestBeans = new ArrayList<FlightRequestWrapper>();
		for (FlightRequest request : flightRequestService.getAllFlightRequests()) {
			flightRequestBeans.add(new FlightRequestWrapper(request));
		}

		List<PlaneWrapper> planeBeans = new ArrayList<PlaneWrapper>();
		for (Plane plane : planeService.getAllPlanes()) {
			planeBeans.add(new PlaneWrapper(plane));
		}

		String fuelCost = String.format("%.2f", fuelCostService.getFuelCost());

		ModelAndView mav = new ModelAndView("index");

		mav.addObject("flightRequestBeans", flightRequestBeans);
		mav.addObject("planeBeans", planeBeans);
		mav.addObject("fuelCost", fuelCost);

		return mav;
	}

	/**
	 * Wrapper class used to wrap a flight-request with it's associated
	 * departure airport, arrival airport and plane-type
	 * 
	 * @author marcus
	 *
	 */
	public class FlightRequestWrapper {

		private FlightRequest request;
		private Airport depAirport;
		private Airport arrAirport;
		private PlaneType planeType;

		public FlightRequestWrapper(FlightRequest flightRequest) {
			setRequest(flightRequest);
			setDepAirport(airportService.getAirportByCode(request.getDepartureAirportCode()));
			setArrAirport(airportService.getAirportByCode(request.getArrivalAirportCode()));
			setPlaneType(request.getOffer() != null ? planeTypeService.getPlaneTypeByCode(request.getOffer().getPlane()
			        .getPlaneTypeCode()) : null);

		}

		public FlightRequest getRequest() {
			return request;
		}

		public void setRequest(FlightRequest request) {
			this.request = request;
		}

		public Airport getDepAirport() {
			return depAirport;
		}

		public void setDepAirport(Airport depAirport) {
			this.depAirport = depAirport;
		}

		public Airport getArrAirport() {
			return arrAirport;
		}

		public void setArrAirport(Airport arrAirport) {
			this.arrAirport = arrAirport;
		}

		public PlaneType getPlaneType() {
			return planeType;
		}

		public void setPlaneType(PlaneType planeType) {
			this.planeType = planeType;
		}

	}

	/**
	 * Wrapper class used to wrap a plane with it's associated plane-type
	 * 
	 * @author marcus
	 *
	 */
	public class PlaneWrapper {

		private Plane plane;
		private PlaneType planeType;

		public PlaneWrapper(Plane plane) {

			setPlane(plane);
			setPlaneType(planeTypeService.getPlaneTypeByCode(plane.getPlaneTypeCode()));

		}

		public Plane getPlane() {
			return plane;
		}

		public void setPlane(Plane plane) {
			this.plane = plane;
		}

		public PlaneType getPlaneType() {
			return planeType;
		}

		public void setPlaneType(PlaneType planeType) {
			this.planeType = planeType;
		}

	}
}
