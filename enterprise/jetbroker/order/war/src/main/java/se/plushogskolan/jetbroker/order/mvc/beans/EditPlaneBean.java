package se.plushogskolan.jetbroker.order.mvc.beans;

import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jetbroker.order.domain.Plane;

/**
 * Bean containing information about a plane
 * 
 * @author marcus
 *
 */
public class EditPlaneBean {

	private long id;
	@NotBlank(message = "{validation.planeType.missing}")
	private String planeTypeCode;
	@NotBlank(message = "{validation.description.missing}")
	private String description;

	public void copyPlaneValuesToBean(Plane plane) {
		setId(plane.getId());
		setPlaneTypeCode(plane.getPlaneTypeCode());
		setDescription(plane.getDescription());
	}

	public void copyBeanValuesToPlane(Plane plane) {
		plane.setId(getId());
		plane.setPlaneTypeCode(planeTypeCode);
		plane.setDescription(getDescription());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
