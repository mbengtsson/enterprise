package se.plushogskolan.jetbroker.order.mvc.beans;

import javax.validation.constraints.Min;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;

/**
 * Bean containing information about a flight-request
 * 
 * @author marcus
 *
 */
public class EditFlightRequestBean {

	@Min(value = 1, message = "{validation.price.missing}")
	private int price;
	@Min(value = 1, message = "{validation.plane.missing}")
	private long planeId;

	public void copyFlightRequestValuesToBean(FlightRequest request) {
		if (request.getOffer() != null) {
			setPrice(request.getOffer().getPrice());
			setPlaneId(request.getOffer().getPlane().getId());
		}
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public long getPlaneId() {
		return planeId;
	}

	public void setPlaneId(long planeId) {
		this.planeId = planeId;
	}

}
