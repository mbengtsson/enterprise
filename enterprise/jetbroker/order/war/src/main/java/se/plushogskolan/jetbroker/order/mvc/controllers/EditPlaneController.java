package se.plushogskolan.jetbroker.order.mvc.controllers;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.mvc.beans.EditPlaneBean;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

/**
 * Spring MVC conrtroller for creating / editing planes
 * 
 * @author marcus
 *
 */
@Controller
@RequestMapping("/plane/{id}/edit.html")
public class EditPlaneController {

	@Inject
	private PlaneService planeService;

	@Inject
	private PlaneTypeService planeTypeService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		List<PlaneType> planeTypes = planeTypeService.getAllPlaneTypes();
		Collections.sort(planeTypes);

		EditPlaneBean bean = new EditPlaneBean();

		if (id > 0) {
			bean.copyPlaneValuesToBean(planeService.getPlane(id));
		}

		ModelAndView mav = new ModelAndView("editPlane");
		mav.addObject("editPlaneBean", bean);
		mav.addObject("planeTypes", planeTypes);

		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(@Valid EditPlaneBean bean, BindingResult errors) {

		if (errors.hasErrors()) {

			List<PlaneType> planeTypes = planeTypeService.getAllPlaneTypes();
			Collections.sort(planeTypes);

			ModelAndView mav = new ModelAndView("editPlane");
			mav.addObject("EditPlaneBean", bean);
			mav.addObject("planeTypes", planeTypes);
			return mav;
		}

		if (bean.getId() > 0) {
			Plane plane = getPlaneService().getPlane(bean.getId());
			bean.copyBeanValuesToPlane(plane);
			getPlaneService().updatePlane(plane);
		} else {
			Plane plane = new Plane();
			bean.copyBeanValuesToPlane(plane);
			getPlaneService().createPlane(plane);
		}

		return new ModelAndView("redirect:/index.html");
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	public PlaneTypeService getPlaneTypeService() {
		return planeTypeService;
	}

	public void setPlaneTypeService(PlaneTypeService planeTypeService) {
		this.planeTypeService = planeTypeService;
	}

}
