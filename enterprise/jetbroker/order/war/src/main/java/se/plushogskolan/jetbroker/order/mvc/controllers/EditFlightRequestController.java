package se.plushogskolan.jetbroker.order.mvc.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.mvc.beans.EditFlightRequestBean;
import se.plushogskolan.jetbroker.order.service.AirportService;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.FuelCostService;
import se.plushogskolan.jetbroker.order.service.PlaneService;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

/**
 * Spring MVC conrtroller for editing flight-requests
 * 
 * @author marcus
 *
 */
@Controller
public class EditFlightRequestController {

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private AirportService airportService;

	@Inject
	private PlaneService planeService;

	@Inject
	private PlaneTypeService planeTypeService;

	@Inject
	private FuelCostService fuelCostService;

	@RequestMapping(value = "/flightrequest/{id}/edit.html", method = RequestMethod.GET)
	public ModelAndView index(@PathVariable long id) {

		FlightRequest request = flightRequestService.getFlightRequest(id);
		FligthRequestWrapper flightRequestBean = new FligthRequestWrapper(request);

		List<PlaneWrapper> planeBeans = new ArrayList<EditFlightRequestController.PlaneWrapper>();
		for (Plane plane : planeService.getAllPlanes()) {
			planeBeans.add(new PlaneWrapper(plane, request));
		}

		EditFlightRequestBean bean = new EditFlightRequestBean();
		bean.copyFlightRequestValuesToBean(request);

		ModelAndView mav = new ModelAndView("editFlightRequest");
		mav.addObject("flightRequestBean", flightRequestBean);
		mav.addObject("planeBeans", planeBeans);
		mav.addObject("editFlightRequestBean", bean);

		return mav;
	}

	@RequestMapping(value = "/flightrequest/{id}/edit.html", method = RequestMethod.POST)
	public ModelAndView handleSubmit(@PathVariable long id, @Valid EditFlightRequestBean bean, BindingResult errors) {

		FlightRequest request = flightRequestService.getFlightRequest(id);

		if (errors.hasErrors()) {

			FligthRequestWrapper flightRequestBean = new FligthRequestWrapper(request);

			List<PlaneWrapper> planeBeans = new ArrayList<EditFlightRequestController.PlaneWrapper>();
			for (Plane plane : planeService.getAllPlanes()) {
				planeBeans.add(new PlaneWrapper(plane, request));
			}

			ModelAndView mav = new ModelAndView("editFlightRequest");
			mav.addObject("flightRequestBean", flightRequestBean);
			mav.addObject("planeBeans", planeBeans);
			mav.addObject("editFlightRequestBean", bean);

			return mav;
		}

		Offer offer;

		if (request.getOffer() != null) {
			offer = request.getOffer();
		} else {
			offer = new Offer();
		}

		offer.setPrice(bean.getPrice());
		offer.setPlane(planeService.getPlane(bean.getPlaneId()));

		flightRequestService.handleFlightRequestOffer(request, offer);

		return new ModelAndView("redirect:/index.html");
	}

	@RequestMapping("/flightrequest/{id}/reject.html")
	public ModelAndView reject(@PathVariable long id) {

		flightRequestService.handleFlightRequestRejects(flightRequestService.getFlightRequest(id));

		return new ModelAndView("redirect:/index.html");
	}

	/**
	 * Wrapper class used to wrap a flight-request with it's associated
	 * departure airport, arrival airport and fuel cost
	 * 
	 * @author marcus
	 *
	 */
	public class FligthRequestWrapper {

		private FlightRequest request;
		private Airport depAirport;
		private Airport arrAirport;
		private double fuelCost;

		public FligthRequestWrapper(FlightRequest flightRequest) {
			setRequest(flightRequest);
			setDepAirport(airportService.getAirportByCode(request.getDepartureAirportCode()));
			setArrAirport(airportService.getAirportByCode(request.getArrivalAirportCode()));
			setFuelCost(fuelCostService.getFuelCost());

		}

		public FlightRequest getRequest() {
			return request;
		}

		public void setRequest(FlightRequest request) {
			this.request = request;
		}

		public Airport getDepAirport() {
			return depAirport;
		}

		public void setDepAirport(Airport depAirport) {
			this.depAirport = depAirport;
		}

		public Airport getArrAirport() {
			return arrAirport;
		}

		public void setArrAirport(Airport arrAirport) {
			this.arrAirport = arrAirport;
		}

		public double getFuelCost() {
			return fuelCost;
		}

		public void setFuelCost(double fuelCost) {
			this.fuelCost = fuelCost;
		}

	}

	/**
	 * Wrapper class used to wrap a plane with it's associated plane-type.
	 * Fuel-cost and travel-time are calculated and stored
	 * 
	 * @author marcus
	 *
	 */
	public class PlaneWrapper {

		private Plane plane;
		private PlaneType planeType;
		private int fuelCost;
		private int travelTime;

		public PlaneWrapper(Plane plane, FlightRequest request) {

			setPlane(plane);
			setPlaneType(planeTypeService.getPlaneTypeByCode(plane.getPlaneTypeCode()));
			setFuelCost((int) Math.round(fuelCostService.getFuelCost() * request.getDistance()
			        * planeType.getFuelConsumptionPerKm()));
			setTravelTime(Math.round((float) request.getDistance() / (float) planeType.getMaxSpeedKmH() * 60));

		}

		public Plane getPlane() {
			return plane;
		}

		public void setPlane(Plane plane) {
			this.plane = plane;
		}

		public PlaneType getPlaneType() {
			return planeType;
		}

		public void setPlaneType(PlaneType planeType) {
			this.planeType = planeType;
		}

		public int getFuelCost() {
			return fuelCost;
		}

		public void setFuelCost(int fuelCost) {
			this.fuelCost = fuelCost;
		}

		public int getTravelTime() {
			return travelTime;
		}

		public void setTravelTime(int travelTime) {
			this.travelTime = travelTime;
		}

	}

}
