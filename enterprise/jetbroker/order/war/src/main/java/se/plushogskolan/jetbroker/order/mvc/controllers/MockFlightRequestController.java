package se.plushogskolan.jetbroker.order.mvc.controllers;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import se.plushogskolan.jetbroker.order.cdi.events.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.integration.mdb.simulator.MessageSimulator;

/**
 * Spring MVC controller used to mock incoming flight-requests
 * 
 * @author marcus
 *
 */
@Controller
public class MockFlightRequestController {

	private static Logger log = Logger.getLogger(MockFlightRequestController.class.getName());

	@Inject
	private MessageSimulator simulator;

	@RequestMapping(value = "/mockFlightRequest.html", method = RequestMethod.GET)
	public ModelAndView index(@RequestParam(value = "id", defaultValue = "0") long id,
	        @RequestParam(value = "pass", defaultValue = "1") int passengers,
	        @RequestParam(value = "depApt", defaultValue = "GOT") String depApt,
	        @RequestParam(value = "arrApt", defaultValue = "ARN") String arrApt) {

		NewFlightRequestEvent event = new NewFlightRequestEvent(id, passengers, depApt, arrApt, DateTime.now());

		simulator.sendNewFlightRequestMessage(event);

		log.info("Mocked flight request event sent to MDB: " + event.toString());

		return new ModelAndView("redirect:/index.html");
	}

}
