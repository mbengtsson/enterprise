<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>

<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet"  />

</head>
<body>

	<jsp:include page="header.jsp" />
	
	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/plane.png">
		<c:choose>
			<c:when test="${editCustomerBean.id > 0}">
				Edit plane 
			</c:when>
			<c:otherwise>
				Add new plane
			</c:otherwise>
		</c:choose></h2>
		
	<form:form commandName="editPlaneBean">
		<form:hidden path="id" />
		<table class="formTable">
			<c:if test="${editPlaneBean.id > 0}">
			<tr>
				<th>ID</th>
				<td>${editPlaneBean.id}</td>
				<td></td>
			</tr>
			</c:if>
			
			<tr>
				<th>PlaneType</th>
				<td>
					<form:select path="planeTypeCode">
						<c:if test="${editPlaneBean.id == 0}">
							<form:option value="" label="-- Select plane-type --"/>
						</c:if>
					<form:options items="${planeTypes}" itemValue="code" itemLabel="niceName"/>
					</form:select>	
				</td>
				<td><form:errors path="planeTypeCode" cssClass="errors" /></td>
			</tr>
			<tr>
				<th>Description</th>
				<td><form:textarea path="description" rows="5" cols="30"/></td>
				<td><form:errors path="description" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th>
				</th>
				<td>
					<c:set var="submitText">
						Submit
					</c:set>
					<input type="submit" value="${submitText}"/>
					<a href="<%=request.getContextPath()%>/index.html">Cancel</a>
				</td>
				<td>
				</td>
			</tr>
		</table>
	</form:form>
	
	
</body>
</html>