<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<head>
<link href="<%=request.getContextPath()%>/style/common.css"
	type="text/css" rel="stylesheet" />

</head>
<body>
	<jsp:include page="header.jsp" />

		<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/flightrequest.png">Flight requests</h2>
		
		<table class="dataTable">
			<tr>
				<th>ID</th>
				<th>Status</th>
				<th>Departure airport</th>
				<th>Arrival airport</th>
				<th>Date</th>
				<th>Passengers</th>
				<th>Offered price</th>
				<th>Offered plane</th>
				<th>Edit</th>
			</tr>
			<c:forEach items="${flightRequestBeans}" var="requestBeans">
				<tr>
					<td>${requestBeans.request.id}</td>
					<td>${requestBeans.request.niceStatus}</td>
					<td>${requestBeans.depAirport.name} (${requestBeans.depAirport.code})</td>
					<td>${requestBeans.arrAirport.name} (${requestBeans.arrAirport.code})</td>
					<td>${requestBeans.request.niceDate}</td>
					<td>${requestBeans.request.passengers}</td>
					<td>
						<c:if test="${not empty requestBeans.request.offer}">
							<fmt:formatNumber  groupingUsed="true" value="${requestBeans.request.offer.price}" /> SEK
						</c:if>
					</td>
					<td>
						<c:if test="${not empty requestBeans.planeType}">
							${requestBeans.planeType.name} (${requestBeans.planeType.maxNoOfPassengers} seats)
						</c:if>
					</td>
					<td><a
						href="<%=request.getContextPath()%>/flightrequest/${requestBeans.request.id}/edit.html"><img src="images/edit.png" ></a></td>
				</tr>
			</c:forEach>
			
		</table>
				
		
		<br>
		<br>
		
		<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/plane.png">Our planes</h2>
		
		<p>
			<a class="button" href="<%=request.getContextPath()%>/plane/0/edit.html">Create new plane</a>
		</p>
		
		<table class="dataTable">
		<tr>
			<th>ID</th>
			<th>Code</th>
			<th>Name</th>
			<th>Max passengers</th>
			<th>Range (km)</th>
			<th>Max speed (km/h)</th>
			<th>Description</th>
			<th>Edit</th>
		</tr>
		<c:forEach items="${planeBeans}" var="planeBean">
		<tr>
			<td>${planeBean.plane.id}</td>
			<td>${planeBean.plane.planeTypeCode}</td>
			<td>${planeBean.planeType.name}</td>
			<td>${planeBean.planeType.maxNoOfPassengers}</td>
			<td>${planeBean.planeType.maxRangeKm}</td>
			<td>${planeBean.planeType.maxSpeedKmH}</td>
			<td>${planeBean.plane.description}</td>
			<td><a href="<%=request.getContextPath()%>/plane/${planeBean.plane.id}/edit.html"><img src="images/edit.png" ></a></td>
		
		</tr>
		</c:forEach>
	
	</table>
	
	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/gas.png">Fuel price</h2>
	<p>Current fuel cost is ${fuelCost} SEK</p>

</body>

</html>