<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>

<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet"  />

</head>
<body>

	<jsp:include page="header.jsp" />
	
	<h2 class="underline">
		<img src="<%=request.getContextPath()%>/images/flightrequest.png">Edit flight request</h2>
	
	<table class="formTable">
		<tr>
			<th>ID</th>
			<td>${flightRequestBean.request.id}</td>
		</tr>
		<tr>
			<th>Departure airport</th>
			<td>${flightRequestBean.depAirport.code}, ${flightRequestBean.depAirport.name}</td>
		</tr>
		<tr>
			<th>Arrival airport</th>
			<td>${flightRequestBean.arrAirport.code}, ${flightRequestBean.arrAirport.name}</td>
		</tr>
		<tr>
			<th>Date</th>
			<td>${flightRequestBean.request.niceDate}</td>
		</tr>
		<tr>
			<th>Passangers</th>
			<td>${flightRequestBean.request.passengers}</td>
		</tr>
		<tr>
			<th>Distance</th>
			<td>${flightRequestBean.request.distance}</td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td></td>
		</tr>
		<tr>
			<th>Fuel cost / liter</th>
			<td>${flightRequestBean.fuelCost}</td>
		</tr>
	</table>	

	<h2>Our offer to customer</h2>
	
	<form:form commandName="editFlightRequestBean">
		<table class="dataTable">
			<tr>
				<th></th>
				<th>Plane</th>
				<th>No of passengers</th>
				<th>Max speed (km/h)</th>
				<th>Fuel consumption (l/km)</th>
				<th>Total fuelcost</th>
				<th>Travel time (min)</th>
				<th>Description</th>
			</tr>
			<c:forEach items="${planeBeans}" var="bean">
				<tr>
					<td>
						<form:radiobutton path="planeId" value="${bean.plane.id}"/>
					</td>
					<td>${bean.planeType.name}</td>
					<td>${bean.planeType.maxNoOfPassengers}</td>
					<td>${bean.planeType.maxSpeedKmH }</td>
					<td>${bean.planeType.fuelConsumptionPerKm }</td>
					<td><fmt:formatNumber  groupingUsed="true" value="${bean.fuelCost}" /> SEK</td>
					<td>${bean.travelTime}</td>
					<td>${bean.plane.description}</td>
				</tr>

			</c:forEach>
		</table>
		<form:errors path="planeId" cssClass="errors" />

		<p>Offered price (SEK): <form:input type ="number" path="price" /> <form:errors path="price" cssClass="errors" /></p>
		
		<p>
			<input type="submit" value="Submit offer" />
			<input type="submit" formaction="<%=request.getContextPath()%>/flightrequest/${flightRequestBean.request.id}/reject.html" value="Reject offer" />
			<a href="<%=request.getContextPath()%>/index.html">Cancel</a>
		</p>

	</form:form>		
	
</body>
</html>