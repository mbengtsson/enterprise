package se.plushogskolan.jetbroker.order.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import se.plushogskolan.jetbroker.order.domain.FlightRequest.Status;

public class FlightRequestTest {

	@Test
	public void testEmptyConstructor() {
		FlightRequest request = new FlightRequest();

		assertEquals("Id is 0", 0, request.getId());
		assertEquals("AgentId is 0", 0, request.getAgentId());
		assertNull("DepartureAirportCode is null", request.getDepartureAirportCode());
		assertNull("ArrivalAirportCode is null", request.getArrivalAirportCode());
		assertNull("Date is null", request.getDate());
		assertEquals("Passengers is 0", 0, request.getPassengers());
		assertEquals("Distance is 0", 0, request.getDistance());
		assertEquals("Status is NEW", Status.NEW, request.getStatus());
		assertNull("Offer is null", request.getOffer());
	}
}
