package se.plushogskolan.jetbroker.order.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class OfferTest {

	@Test
	public void testEmptyConstructor() {
		Offer offer = new Offer();

		assertEquals("Id is 0", 0, offer.getId());
		assertEquals("Price is 0", 0, offer.getPrice());
		assertNull("Plane is null", offer.getPlane());
		assertNull("FlightRequest is null", offer.getFlightRequest());
	}
}
