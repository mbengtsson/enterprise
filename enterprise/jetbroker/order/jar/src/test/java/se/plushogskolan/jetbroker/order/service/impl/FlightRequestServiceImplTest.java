package se.plushogskolan.jetbroker.order.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.event.Event;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.cdi.events.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.facade.IntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

	private Airport depAirport;
	private Airport arrAirport;

	private FlightRequest request;
	private List<FlightRequest> requests;

	private Offer offer;

	private FlightRequestServiceImpl flightRequestService;

	@Before
	public void setup() {
		depAirport = new Airport("GOT", "Göteborg - Landvetter", 57.667778, 12.294444);
		arrAirport = new Airport("ARN", "Stockholm - Arlanda", 59.651944, 17.918611);

		request = new TestFixture.ValidFlightRequestBuilder().departureAirport("GOT").arrivalAirport("ARN").build();
		requests = new ArrayList<FlightRequest>();
		requests.add(request);

		offer = new TestFixture.ValidOfferBuilder().build();

		flightRequestService = new FlightRequestServiceImpl();
	}

	@Test
	public void testCreateFlightRequest() {
		// Setup mock flight request repo
		FlightRequestRepository frRepo = createMock(FlightRequestRepository.class);
		expect(frRepo.persist(request)).andReturn(1L);
		expect(frRepo.findById(1L)).andReturn(request);
		replay(frRepo);
		flightRequestService.setFlightRequestRepo(frRepo);

		// Setup mock airport repo
		AirportRepository aRepo = createMock(AirportRepository.class);
		expect(aRepo.getAirportByCode("GOT")).andReturn(depAirport);
		expect(aRepo.getAirportByCode("ARN")).andReturn(arrAirport);
		replay(aRepo);
		flightRequestService.setAirportRepo(aRepo);

		request = flightRequestService.createFlightRequest(request);
		assertEquals("Service calculated and added correct distance", 393, request.getDistance());

		verify(frRepo);
		verify(aRepo);

	}

	@Test
	public void testGetFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1)).andReturn(request);
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		assertEquals("get customer in return", request, flightRequestService.getFlightRequest(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testRemoveFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		repo.remove(request);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		flightRequestService.removeFlightRequest(request);

		// Verify
		verify(repo);

	}

	@Test
	public void testGetAllFlightRequests() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.getAllFlightRequests()).andReturn(requests);
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		assertEquals("get customer list in return", requests, flightRequestService.getAllFlightRequests());
	}

	@Test
	public void testUpdateFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		repo.update(request);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		flightRequestService.updateFlightRequest(request);

		// Verify
		verify(repo);

	}

	@Test
	public void testHandleIncomingFlightRequest() {

		// Setup mock facade
		IntegrationFacade facade = createMock(IntegrationFacade.class);
		facade.sendConfirmationMessage(request);
		expectLastCall();
		replay(facade);
		flightRequestService.setIntegrationFacade(facade);

		// Setup mock flight request repo
		FlightRequestRepository frRepo = createMock(FlightRequestRepository.class);
		expect(frRepo.persist(request)).andReturn(1L);
		expect(frRepo.findById(1L)).andReturn(request);
		replay(frRepo);
		flightRequestService.setFlightRequestRepo(frRepo);

		// Setup mock airport repo
		AirportRepository aRepo = createMock(AirportRepository.class);
		expect(aRepo.getAirportByCode("GOT")).andReturn(depAirport);
		expect(aRepo.getAirportByCode("ARN")).andReturn(arrAirport);
		replay(aRepo);
		flightRequestService.setAirportRepo(aRepo);

		// Setup mock event
		@SuppressWarnings("unchecked")
		Event<NewFlightRequestEvent> event = createMock(Event.class);
		event.fire(new NewFlightRequestEvent());
		expectLastCall();
		replay(event);
		flightRequestService.setNewFlightRequestEvent(event);

		// Perform test
		flightRequestService.handleIncomingFlightRequest(request);

		// Verify
		verify(facade);
		verify(aRepo);
		verify(frRepo);
		verify(event);
	}

	@Test
	public void testHandleFlightRequestOffer() {

		// Setup mock facade
		IntegrationFacade facade = createMock(IntegrationFacade.class);
		facade.sendFlightRequestOfferMessage(request);
		expectLastCall();
		replay(facade);
		flightRequestService.setIntegrationFacade(facade);

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		repo.update(request);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		flightRequestService.handleFlightRequestOffer(request, offer);
		assertEquals("Ststus is OFFER_SENT", Status.OFFER_SENT, request.getStatus());

		// Verify
		verify(facade);
		verify(repo);
	}

	@Test
	public void testHandleFlightRequestRejects() {

		// Setup mock facade
		IntegrationFacade facade = createMock(IntegrationFacade.class);
		facade.sendFlightRequestRejectedMessage(request);
		expectLastCall();
		replay(facade);
		flightRequestService.setIntegrationFacade(facade);

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		repo.update(request);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepo(repo);

		// Perform test
		flightRequestService.handleFlightRequestRejects(request);
		assertEquals("Ststus is REJECTED", Status.REJECTED, request.getStatus());

		// Verify
		verify(facade);
		verify(repo);
	}
}
