package se.plushogskolan.jetbroker.order.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class PlaneTest {

	@Test
	public void testEmptyConstructor() {
		Plane plane = new Plane();

		assertEquals("Id is 0", 0, plane.getId());
		assertNull("PlaneCode is null", plane.getPlaneTypeCode());
		assertNull("Description is null", plane.getDescription());
	}

	@Test
	public void testTwoParamConstructor() {
		Plane plane = new Plane("B770", "A plane");

		assertEquals("Id is 0", 0, plane.getId());
		assertEquals("PlaneCode is correct", "B770", plane.getPlaneTypeCode());
		assertEquals("Description is correct", "A plane", plane.getDescription());
	}

}
