package se.plushogskolan.jetbroker.order.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.service.impl.FuelCostServiceImpl;

public class FuelCostServiceImplTest {

	private FuelCostServiceImpl fuelCostService;

	@Before
	public void setup() {
		fuelCostService = new FuelCostServiceImpl();
		fuelCostService.init();
	}

	@Test
	public void testGetFuelCost() {

		assertEquals("get fuelCost in return", 5.2, fuelCostService.getFuelCost(), 2);

	}

	@Test
	public void testUpdateFuelCost() {

		fuelCostService.updateFuelCost(1.1);
		assertEquals("get fuelCost in return", 1.1, fuelCostService.getFuelCost(), 2);
	}

}
