package se.plushogskolan.jetbroker.order.repository.jpa;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Plane;

@RunWith(Arquillian.class)
@Transactional(TransactionMode.ROLLBACK)
public class JpaPlaneRepositoryIntegrationTest extends AbstractRepositoryTest<Plane, JpaPlaneRepository> {

	@Inject
	JpaPlaneRepository repo;

	@Override
	protected JpaPlaneRepository getRepository() {
		return repo;
	}

	@Override
	protected Plane getEntity1() {
		return new TestFixture.ValidPlaneBuilder().build();
	}

	@Override
	protected Plane getEntity2() {
		return new TestFixture.ValidPlaneBuilder().planeCode("CODE").description("A plane").build();
	}

	@Test
	public void testGetAllPlanes() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		assertEquals("List is correct size", 2, repo.getAllPlanes().size());
	}
}
