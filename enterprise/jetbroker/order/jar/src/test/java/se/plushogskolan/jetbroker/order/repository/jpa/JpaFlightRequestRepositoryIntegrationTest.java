package se.plushogskolan.jetbroker.order.repository.jpa;

import static org.junit.Assert.assertEquals;

import javax.inject.Inject;

import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;

public class JpaFlightRequestRepositoryIntegrationTest extends
        AbstractRepositoryTest<FlightRequest, JpaFlightRequestRepository> {

	@Inject
	JpaFlightRequestRepository repo;

	@Override
	protected JpaFlightRequestRepository getRepository() {
		return repo;
	}

	@Override
	protected FlightRequest getEntity1() {
		return new TestFixture.ValidFlightRequestBuilder().agentId(1).build();
	}

	@Override
	protected FlightRequest getEntity2() {
		return new TestFixture.ValidFlightRequestBuilder().agentId(2).build();
	}

	@Test
	public void testGetAllFlightRequests() {
		repo.persist(getEntity1());
		repo.persist(getEntity2());
		assertEquals("List is correct size", 2, repo.getAllFlightRequests().size());
	}

}
