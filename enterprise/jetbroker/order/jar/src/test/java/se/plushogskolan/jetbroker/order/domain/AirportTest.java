package se.plushogskolan.jetbroker.order.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AirportTest {

	@Test
	public void testConstructor() {
		Airport airport = new Airport("GOT", "Göteborg - Landvetter", 57.667778, 12.294444);

		assertEquals("Code is correct", "GOT", airport.getCode());
		assertEquals("Name is correct", "Göteborg - Landvetter", airport.getName());
		assertEquals("Latitude is correct", 57.667778, airport.getLatitude(), 2);
		assertEquals("Longitude is correct", 12.294444, airport.getLongitude(), 2);
	}

}
