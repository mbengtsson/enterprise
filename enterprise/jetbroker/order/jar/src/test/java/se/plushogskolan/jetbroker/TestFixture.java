package se.plushogskolan.jetbroker;

import java.util.logging.Logger;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.joda.time.DateTime;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.domain.Plane;

public class TestFixture {

	private static Logger log = Logger.getLogger(TestFixture.class.getName());

	public static Archive<?> createIntegrationTestArchive() {

		MavenDependencyResolver mvnResolver = DependencyResolvers.use(MavenDependencyResolver.class)
		        .loadMetadataFromPom("pom.xml");

		WebArchive war = ShrinkWrap.create(WebArchive.class, "order_test.war").addPackages(true, "se.plushogskolan")
		        .addAsWebInfResource("beans.xml").addAsResource("META-INF/persistence.xml");

		war.addAsLibraries(mvnResolver.artifact("org.easymock:easymock:3.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("joda-time:joda-time:2.2").resolveAsFiles());
		war.addAsLibraries(mvnResolver.artifact("org.jadira.usertype:usertype.core:3.1.0.CR8").resolveAsFiles());

		log.info("JAR: " + war.toString(true));
		return war;
	}

	public static class ValidPlaneBuilder {

		private long id = 0;
		private String planeCode = "TEST";
		private String description = "A test plane";

		public ValidPlaneBuilder id(long id) {
			this.id = id;
			return this;
		}

		public ValidPlaneBuilder planeCode(String planeCode) {
			this.planeCode = planeCode;
			return this;
		}

		public ValidPlaneBuilder description(String description) {
			this.description = description;
			return this;
		}

		public Plane build() {
			Plane plane = new Plane();
			plane.setId(id);
			plane.setPlaneTypeCode(planeCode);
			plane.setDescription(description);
			return plane;
		}
	}

	public static class ValidFlightRequestBuilder {

		private long id = 0;
		private long agentId = 1;
		private String departureAirport = "GOT";
		private String arrivalAirport = "ARN";
		private DateTime date = DateTime.now();
		private int passengers = 25;
		private int distance = 393;
		private Offer offer = null;
		private Status status = Status.NEW;

		public ValidFlightRequestBuilder id(long id) {
			this.id = id;
			return this;
		}

		public ValidFlightRequestBuilder agentId(long agentId) {
			this.agentId = agentId;
			return this;
		}

		public ValidFlightRequestBuilder departureAirport(String departureAirport) {
			this.departureAirport = departureAirport;
			return this;
		}

		public ValidFlightRequestBuilder arrivalAirport(String arrivalAirport) {
			this.arrivalAirport = arrivalAirport;
			return this;
		}

		public ValidFlightRequestBuilder date(DateTime date) {
			this.date = date;
			return this;
		}

		public ValidFlightRequestBuilder passengers(int passengers) {
			this.passengers = passengers;
			return this;
		}

		public ValidFlightRequestBuilder distance(int distance) {
			this.distance = distance;
			return this;
		}

		public ValidFlightRequestBuilder offer(Offer offer) {
			this.offer = offer;
			return this;
		}

		public ValidFlightRequestBuilder status(Status status) {
			this.status = status;
			return this;
		}

		public FlightRequest build() {
			FlightRequest request = new FlightRequest();

			request.setId(id);
			request.setAgentId(agentId);
			request.setDepartureAirportCode(departureAirport);
			request.setArrivalAirportCode(arrivalAirport);
			request.setDate(date);
			request.setPassengers(passengers);
			request.setDistance(distance);
			request.setOffer(offer);
			request.setStatus(status);

			return request;
		}
	}

	public static class ValidOfferBuilder {

		private long id = 0;
		private Plane plane = null;
		private int price = 0;
		private FlightRequest flightRequest = null;

		public ValidOfferBuilder id(long id) {
			this.id = id;
			return this;
		}

		public ValidOfferBuilder plane(Plane plane) {
			this.plane = plane;
			return this;
		}

		public ValidOfferBuilder price(int price) {
			this.price = price;
			return this;
		}

		public ValidOfferBuilder flightRequest(FlightRequest flightRequest) {
			this.flightRequest = flightRequest;
			return this;
		}

		public Offer build() {
			Offer offer = new Offer();

			offer.setId(id);
			offer.setPlane(plane);
			offer.setPrice(price);
			offer.setFlightRequest(flightRequest);

			return offer;
		}

	}
}
