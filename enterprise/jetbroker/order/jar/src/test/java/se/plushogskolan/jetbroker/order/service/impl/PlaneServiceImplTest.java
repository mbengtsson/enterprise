package se.plushogskolan.jetbroker.order.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;
import se.plushogskolan.jetbroker.order.service.impl.PlaneServiceImpl;

public class PlaneServiceImplTest {

	private Plane plane;
	private List<Plane> planes;
	private PlaneServiceImpl planeService;

	@Before
	public void setup() {
		plane = new TestFixture.ValidPlaneBuilder().build();
		planes = new ArrayList<Plane>();
		planes.add(plane);
		planeService = new PlaneServiceImpl();
	}

	@Test
	public void testGetPlane() {

		// Setup mock repo
		PlaneRepository repo = createMock(PlaneRepository.class);
		expect(repo.findById(1)).andReturn(plane);
		replay(repo);
		planeService.setPlaneRepo(repo);

		// Perform test
		assertEquals("get plane in return", plane, planeService.getPlane(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testUpdatePlane() {

		// Setup mock repo
		PlaneRepository repo = createMock(PlaneRepository.class);
		repo.update(plane);
		expectLastCall();
		replay(repo);
		planeService.setPlaneRepo(repo);

		// Perform test
		planeService.updatePlane(plane);

		// Verify
		verify(repo);

	}

	@Test
	public void testCreatePlane() {

		// Setup mock repo
		PlaneRepository repo = createMock(PlaneRepository.class);
		expect(repo.persist(plane)).andReturn(1L);
		expect(repo.findById(1)).andReturn(plane);
		replay(repo);
		planeService.setPlaneRepo(repo);

		// Perform test
		assertEquals("get plane in return", plane, planeService.createPlane(plane));

		// Verify
		verify(repo);

	}

	@Test
	public void testRemovePlane() {

		// Setup mock repo
		PlaneRepository repo = createMock(PlaneRepository.class);
		repo.remove(plane);
		expectLastCall();
		replay(repo);
		planeService.setPlaneRepo(repo);

		// Perform test
		planeService.removePlane(plane);

		// Verify
		verify(repo);

	}

	@Test
	public void testGetAllPlanes() {

		// Setup mock repo
		PlaneRepository repo = createMock(PlaneRepository.class);
		expect(repo.getAllPlanes()).andReturn(planes);
		replay(repo);
		planeService.setPlaneRepo(repo);

		// Perform test
		assertEquals("get plane list in return", planes, planeService.getAllPlanes());
	}
}
