package se.plushogskolan.jetbroker.order.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlaneTypeTest {

	@Test
	public void testConstructor() {
		PlaneType planeType = new PlaneType("B770", "Boeing 770", 300, 9700, 890, 22);

		assertEquals("Code is correct", "B770", planeType.getCode());
		assertEquals("Name is correct", "Boeing 770", planeType.getName());
		assertEquals("Passengers is correct", 300, planeType.getMaxNoOfPassengers());
		assertEquals("Range is correct", 9700, planeType.getMaxRangeKm());
		assertEquals("Speed is correct", 890, planeType.getMaxSpeedKmH());
		assertEquals("Fuel consumption is correct", 22, planeType.getFuelConsumptionPerKm(), 2);
	}

}
