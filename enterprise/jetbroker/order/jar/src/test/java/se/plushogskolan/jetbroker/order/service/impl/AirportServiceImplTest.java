package se.plushogskolan.jetbroker.order.service.impl;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.service.impl.AirportServiceImpl;

public class AirportServiceImplTest {

	private Airport airport;
	private List<Airport> airports;
	private AirportServiceImpl airportService;

	@Before
	public void setup() {
		airport = new Airport("GOT", "Göteborg - Landvetter", 57.667778, 12.294444);
		airports = new ArrayList<Airport>();
		airports.add(airport);
		airportService = new AirportServiceImpl();
	}

	@Test
	public void testGetAirportByCode() {
		// Setup mock repo
		AirportRepository repo = createMock(AirportRepository.class);
		expect(repo.getAirportByCode("GOT")).andReturn(airport);
		replay(repo);
		airportService.setAirportRepo(repo);

		// Perform test
		assertEquals("Get airport in return", airport, airportService.getAirportByCode("GOT"));

		// Verify
		verify(repo);

	}

	@Test
	public void testGetAllAirports() {
		// Setup mock repo
		AirportRepository repo = createMock(AirportRepository.class);
		expect(repo.getAllAirports()).andReturn(airports);
		replay(repo);
		airportService.setAirportRepo(repo);

		// Perform test
		assertEquals("Get all airports in return", airports, airportService.getAllAirports());

		// Verify
		verify(repo);

	}

}
