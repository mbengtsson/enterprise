package se.plushogskolan.jetbroker.order.service;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Local;
import javax.ejb.Lock;
import javax.ejb.LockType;

/**
 * Interface for fuel cost service
 * 
 * @author marcus
 *
 */
@Local
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public interface FuelCostService {

	@Lock(LockType.READ)
	double getFuelCost();

	@Lock(LockType.WRITE)
	void updateFuelCost(double fuelCost);
}
