package se.plushogskolan.jetbroker.order.integration.mdb.simulator;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.cdi.events.NewFlightRequestEvent;

/**
 * Interface for flight-request message simulator used to simulate incoming
 * flight-request messages
 * 
 * @author marcus
 *
 */
@Local
public interface MessageSimulator {

	public void sendNewFlightRequestMessage(NewFlightRequestEvent event);
}
