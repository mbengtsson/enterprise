package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Class representing a flight request entity
 * 
 * @author marcus
 *
 */
@Entity
public class FlightRequest implements IdHolder, Comparable<FlightRequest> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotNull
	private long agentId;

	@NotBlank
	private String departureAirportCode;

	@NotBlank
	private String arrivalAirportCode;

	@NotNull
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime date;

	@NotNull
	private int passengers;

	@NotNull
	private int distance;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "offerId")
	private Offer offer;

	public FlightRequest() {
		setStatus(Status.NEW);
	}

	@JoinColumn(name = "planeId")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAgentId() {
		return agentId;
	}

	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public int getPassengers() {
		return passengers;
	}

	public void setPassengers(int passengers) {
		this.passengers = passengers;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public String getNiceDate() {
		DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("YYYY-MMM-dd HH:mm");
		return dateFormatter.print(date);
	}

	public String getNiceStatus() {
		return status.getNiceStatus();
	}

	@Override
	public String toString() {
		return String
		        .format("FlightRequest [id=%s, agentId=%s, departureAirportCode=%s, arrivalAirportCode=%s, date=%s, passengers=%s, distance=%s, status=%s, offer=%s]",
		                id, agentId, departureAirportCode, arrivalAirportCode, date, passengers, distance, status,
		                offer != null ? offer.getId() : null);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (agentId ^ (agentId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FlightRequest other = (FlightRequest) obj;
		if (agentId != other.agentId) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(FlightRequest o) {

		if (date.isBefore(o.getDate())) {
			return -1;
		} else if (date.isAfter(o.getDate())) {
			return 1;
		} else {
			return 0;
		}
	}

	public enum Status {
		NEW("New"),
		OFFER_SENT("Offer sent"),
		REJECTED("Rejected");

		private String niceStatus;

		Status(String niceStatus) {
			this.niceStatus = niceStatus;
		}

		private String getNiceStatus() {
			return niceStatus;
		}

	}

}
