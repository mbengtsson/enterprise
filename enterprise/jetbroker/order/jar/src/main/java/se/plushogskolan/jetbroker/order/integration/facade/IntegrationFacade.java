package se.plushogskolan.jetbroker.order.integration.facade;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;

/**
 * Facade for integration between the different systems
 * 
 * @author marcus
 *
 */
public interface IntegrationFacade {

	void sendConfirmationMessage(FlightRequest request);

	void sendFlightRequestOfferMessage(FlightRequest request);

	void sendFlightRequestRejectedMessage(FlightRequest request);
}
