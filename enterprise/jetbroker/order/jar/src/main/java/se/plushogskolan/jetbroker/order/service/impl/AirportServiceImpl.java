package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.service.AirportService;

/**
 * Implementation of airport service
 * 
 * @author marcus
 *
 */
@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	private AirportRepository AirportRepo;

	@Override
	public Airport getAirportByCode(String code) {
		return getAirportRepo().getAirportByCode(code);
	}

	@Override
	public List<Airport> getAllAirports() {
		return AirportRepo.getAllAirports();
	}

	protected AirportRepository getAirportRepo() {
		return AirportRepo;
	}

	protected void setAirportRepo(AirportRepository airportRepo) {
		AirportRepo = airportRepo;
	}

}
