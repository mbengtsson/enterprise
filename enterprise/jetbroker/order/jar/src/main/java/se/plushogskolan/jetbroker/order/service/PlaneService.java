package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.Plane;

/**
 * Interface for plane service
 * 
 * @author marcus
 *
 */
@Local
public interface PlaneService {

	Plane getPlane(long id);

	void updatePlane(Plane plane);

	Plane createPlane(Plane plane);

	void removePlane(Plane plane);

	List<Plane> getAllPlanes();

}
