package se.plushogskolan.jetbroker.order.integration.mdb.simulator;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.annotations.Mock;
import se.plushogskolan.jetbroker.order.cdi.annotations.NewFlightRequest;
import se.plushogskolan.jetbroker.order.cdi.events.NewFlightRequestEvent;

/**
 * Implementation of flight-request message simulator used to simulate incoming
 * flight-request messages
 * 
 * @author marcus
 *
 */
@Stateless
public class MessageSimulatorImpl implements MessageSimulator {

	@Inject
	@NewFlightRequest
	@Mock
	private Event<NewFlightRequestEvent> newFlightRequestEvent;

	@Override
	public void sendNewFlightRequestMessage(NewFlightRequestEvent event) {

		newFlightRequestEvent.fire(event);

	}

}
