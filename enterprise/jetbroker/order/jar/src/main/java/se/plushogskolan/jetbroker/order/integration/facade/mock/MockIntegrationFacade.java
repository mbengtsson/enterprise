package se.plushogskolan.jetbroker.order.integration.facade.mock;

import java.util.logging.Logger;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.integration.facade.IntegrationFacade;

/**
 * Mocked implementation of the integration facade
 * 
 * @author marcus
 *
 */
public class MockIntegrationFacade implements IntegrationFacade {

	private static Logger log = Logger.getLogger(MockIntegrationFacade.class.getName());

	@Override
	public void sendConfirmationMessage(FlightRequest request) {
		log.info("FlightRequest recieved, agent-id: " + request.getAgentId() + ", order-id: " + request.getId());

	}

	@Override
	public void sendFlightRequestOfferMessage(FlightRequest request) {
		log.info("FlightRequest offered, order-id: " + request.getId() + ", offer-id: " + request.getOffer().getId());

	}

	@Override
	public void sendFlightRequestRejectedMessage(FlightRequest request) {
		log.info("FlightRequest rejected, agent-id: " + request.getAgentId() + ", order-id: " + request.getId());

	}

}
