package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;
import se.plushogskolan.jetbroker.order.service.PlaneService;

/**
 * Implementation of plane service
 * 
 * @author marcus
 *
 */
@Stateless
public class PlaneServiceImpl implements PlaneService {

	@Inject
	private PlaneRepository planeRepo;

	@Override
	public Plane getPlane(long id) {
		return getPlaneRepo().findById(id);
	}

	@Override
	public void updatePlane(Plane plane) {
		getPlaneRepo().update(plane);
	}

	@Override
	public Plane createPlane(Plane plane) {
		long id = getPlaneRepo().persist(plane);
		return getPlane(id);
	}

	@Override
	public void removePlane(Plane plane) {
		getPlaneRepo().remove(plane);
	}

	@Override
	public List<Plane> getAllPlanes() {
		return getPlaneRepo().getAllPlanes();
	}

	protected PlaneRepository getPlaneRepo() {
		return planeRepo;
	}

	protected void setPlaneRepo(PlaneRepository planeRepository) {
		this.planeRepo = planeRepository;
	}

}
