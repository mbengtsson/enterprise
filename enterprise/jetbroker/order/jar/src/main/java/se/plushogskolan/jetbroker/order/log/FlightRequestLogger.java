package se.plushogskolan.jetbroker.order.log;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import se.plushogskolan.jee.utils.cdi.annotations.Prod;
import se.plushogskolan.jetbroker.order.cdi.annotations.NewFlightRequest;
import se.plushogskolan.jetbroker.order.cdi.events.NewFlightRequestEvent;

/**
 * Class used to log new flight-requests, observes NewFlightRequest events
 * 
 * @author marcus
 *
 */
@ApplicationScoped
public class FlightRequestLogger {

	private static Logger log = Logger.getLogger(FlightRequestLogger.class.getName());

	private int newRequests;

	private void showLog() {
		log.info("------[FlightRequestLogger]------");
		log.info("New flight requests: " + newRequests);
		log.info("---------------------------------");
	}

	public void logNewFlightRequest(@Observes @NewFlightRequest @Prod NewFlightRequestEvent event) {

		newRequests++;
		showLog();
	}

}
