package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.repository.PlaneTypeRepository;
import se.plushogskolan.jetbroker.order.service.PlaneTypeService;

/**
 * Implementation of plane type service
 * 
 * @author marcus
 *
 */
@Stateless
public class PlaneTypeServiceImpl implements PlaneTypeService {

	@Inject
	private PlaneTypeRepository planeTypeRepo;

	@Override
	public PlaneType getPlaneTypeByCode(String code) {
		return getPlaneTypeRepo().getPlaneTypeByCode(code);
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return getPlaneTypeRepo().getAllPlaneTypes();
	}

	protected PlaneTypeRepository getPlaneTypeRepo() {
		return planeTypeRepo;
	}

	protected void setPlaneTypeRepo(PlaneTypeRepository planeTypeRepo) {
		this.planeTypeRepo = planeTypeRepo;
	}

}
