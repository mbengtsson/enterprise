package se.plushogskolan.jetbroker.order.repository.mock;

import java.util.ArrayList;
import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;

/**
 * Mocked implementation of airport repository
 * 
 * @author marcus
 *
 */
public class MockAirportRepository implements AirportRepository {

	private List<Airport> airports = new ArrayList<Airport>();

	public MockAirportRepository() {
		mockAirports();
	}

	private void mockAirports() {
		airports.add(new Airport("GOT", "Göteborg - Landvetter", 57.667778, 12.294444));
		airports.add(new Airport("GSE", "Göteborg - City", 57.774722, 11.870278));
		airports.add(new Airport("ARN", "Stockholm - Arlanda", 59.651944, 17.918611));
	}

	@Override
	public Airport getAirportByCode(String code) {
		for (Airport airport : airports) {
			if (airport.getCode().equals(code)) {
				return airport;
			}
		}
		return null;
	}

	@Override
	public List<Airport> getAllAirports() {

		return airports;
	}

}
