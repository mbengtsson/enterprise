package se.plushogskolan.jetbroker.order.service;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;

/**
 * Interface for flight request service
 * 
 * @author marcus
 *
 */
@Local
public interface FlightRequestService {

	FlightRequest createFlightRequest(FlightRequest flightRequest);

	FlightRequest getFlightRequest(long id);

	void updateFlightRequest(FlightRequest flightRequest);

	void removeFlightRequest(FlightRequest flightRequest);

	List<FlightRequest> getAllFlightRequests();

	public void handleIncomingFlightRequest(FlightRequest flightRequest);

	void handleFlightRequestOffer(FlightRequest flightRequest, Offer offer);

	void handleFlightRequestRejects(FlightRequest flightRequest);
}
