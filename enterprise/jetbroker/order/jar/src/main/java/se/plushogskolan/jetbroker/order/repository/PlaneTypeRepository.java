package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.PlaneType;

/**
 * Interface for plane type repository
 * 
 * @author marcus
 *
 */
public interface PlaneTypeRepository {

	PlaneType getPlaneTypeByCode(String code);

	List<PlaneType> getAllPlaneTypes();

}
