package se.plushogskolan.jetbroker.order.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

/**
 * Implementation of flight request repository using JPA for persistance
 * 
 * @author marcus
 *
 */
public class JpaFlightRequestRepository extends JpaRepository<FlightRequest> implements FlightRequestRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return em.createQuery("select f from FlightRequest f").getResultList();
	}

}
