package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Class representing a offer entity
 * 
 * @author marcus
 *
 */
@Entity
public class Offer implements IdHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private int price;
	@ManyToOne
	@JoinColumn(name = "planeId")
	private Plane plane;
	@OneToOne(mappedBy = "offer")
	private FlightRequest flightRequest;

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Plane getPlane() {
		return plane;
	}

	public void setPlane(Plane plane) {
		this.plane = plane;
	}

	public FlightRequest getFlightRequest() {
		return flightRequest;
	}

	public void setFlightRequest(FlightRequest flightRequest) {
		this.flightRequest = flightRequest;
	}

	@Override
	public String toString() {
		return String.format("Offer [id=%s, price=%s, plane=%s, flightRequest=%s]", id, price,
		        plane != null ? plane.getId() : null, flightRequest != null ? flightRequest.getId() : null);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flightRequest == null) ? 0 : flightRequest.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Offer other = (Offer) obj;
		if (flightRequest == null) {
			if (other.flightRequest != null) {
				return false;
			}
		} else if (!flightRequest.equals(other.flightRequest)) {
			return false;
		}
		return true;
	}

}
