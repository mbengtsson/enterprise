package se.plushogskolan.jetbroker.order.service.impl;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import se.plushogskolan.jetbroker.order.service.FuelCostService;

/**
 * Implementation of fuel cost service
 * 
 * @author marcus
 *
 */
@Singleton
@Startup
public class FuelCostServiceImpl implements FuelCostService {

	private double fuelCost;

	@PostConstruct
	protected void init() {
		updateFuelCost(5.2);
	}

	@Override
	public double getFuelCost() {
		return fuelCost;
	}

	@Override
	public void updateFuelCost(double fuelCost) {
		this.fuelCost = fuelCost;
	}

}
