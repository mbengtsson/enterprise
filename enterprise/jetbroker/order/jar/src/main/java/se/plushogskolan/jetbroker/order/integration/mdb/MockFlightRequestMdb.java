package se.plushogskolan.jetbroker.order.integration.mdb;

import java.util.logging.Logger;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.annotations.Mock;
import se.plushogskolan.jetbroker.order.cdi.annotations.NewFlightRequest;
import se.plushogskolan.jetbroker.order.cdi.events.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

/**
 * Mocked implementation of MDB used to recive new flight-requests
 * 
 * @author marcus
 *
 */
public class MockFlightRequestMdb {

	private static Logger log = Logger.getLogger(MockFlightRequestMdb.class.getName());

	@Inject
	private FlightRequestService service;

	public void onMessage(@Observes @NewFlightRequest @Mock NewFlightRequestEvent event) {

		log.info("Mock: incoming new flight request, agentId: " + event.getId());

		FlightRequest request = new FlightRequest();

		request.setAgentId(event.getId());

		request.setPassengers(event.getPassengers());
		request.setDepartureAirportCode(event.getDepartureAirportCode());
		request.setArrivalAirportCode(event.getArrivalAirportCode());
		request.setDate(event.getDate());

		service.handleIncomingFlightRequest(request);

	}
}
