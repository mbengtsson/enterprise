package se.plushogskolan.jetbroker.order.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

import se.plushogskolan.jee.utils.domain.IdHolder;

/**
 * Class representing a Plane entity
 * 
 * @author marcus
 *
 */
@Entity
public class Plane implements IdHolder, Comparable<Plane> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank
	private String planeTypeCode;
	@NotBlank
	private String description;

	public Plane() {

	}

	public Plane(String planeCode, String description) {
		setId(id);
		setPlaneTypeCode(planeCode);
		setDescription(description);
	}

	@Override
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlaneTypeCode() {
		return planeTypeCode;
	}

	public void setPlaneTypeCode(String planeTypeCode) {
		this.planeTypeCode = planeTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return String.format("Plane [id=%s, planeCode=%s, description=%s]", id, planeTypeCode, description);
	}

	@Override
	public int compareTo(Plane o) {
		return getPlaneTypeCode().compareTo(o.getPlaneTypeCode());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((planeTypeCode == null) ? 0 : planeTypeCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plane other = (Plane) obj;
		if (id != other.id)
			return false;
		if (planeTypeCode == null) {
			if (other.planeTypeCode != null)
				return false;
		} else if (!planeTypeCode.equals(other.planeTypeCode))
			return false;
		return true;
	}

}
