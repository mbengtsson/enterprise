package se.plushogskolan.jetbroker.order.repository;

import java.util.List;

import se.plushogskolan.jetbroker.order.domain.Airport;

/**
 * Interface for airport repository
 * 
 * @author marcus
 *
 */
public interface AirportRepository {

	Airport getAirportByCode(String code);

	List<Airport> getAllAirports();
}
