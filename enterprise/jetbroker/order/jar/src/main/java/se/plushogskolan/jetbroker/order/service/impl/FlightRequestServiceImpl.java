package se.plushogskolan.jetbroker.order.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import se.plushogskolan.jee.utils.cdi.annotations.Prod;
import se.plushogskolan.jetbroker.order.cdi.annotations.NewFlightRequest;
import se.plushogskolan.jetbroker.order.cdi.events.NewFlightRequestEvent;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequest.Status;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.facade.IntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.AirportRepository;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;

/**
 * Implementation of flight request service
 * 
 * @author marcus
 *
 */
@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	@Inject
	private FlightRequestRepository flightRequestRepo;

	@Inject
	private AirportRepository airportRepo;

	@Inject
	private IntegrationFacade integrationFacade;

	@Inject
	@NewFlightRequest
	@Prod
	private Event<NewFlightRequestEvent> newFlightRequestEvent;

	/**
	 * Calculates the distance of a new flight-request and persists it
	 */
	@Override
	public FlightRequest createFlightRequest(FlightRequest flightRequest) {

		int distance = calculateDistance(airportRepo.getAirportByCode(flightRequest.getDepartureAirportCode()),
		        airportRepo.getAirportByCode(flightRequest.getArrivalAirportCode()));

		flightRequest.setDistance(distance);

		long id = flightRequestRepo.persist(flightRequest);
		return getFlightRequest(id);
	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return flightRequestRepo.findById(id);
	}

	@Override
	public void updateFlightRequest(FlightRequest flightRequest) {
		flightRequestRepo.update(flightRequest);
	}

	@Override
	public void removeFlightRequest(FlightRequest flightRequest) {
		flightRequestRepo.remove(flightRequest);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return flightRequestRepo.getAllFlightRequests();
	}

	/**
	 * Handles incoming flight requests, they are persisted and a
	 * NewFlightRequestEvent is fired
	 */
	@Override
	public void handleIncomingFlightRequest(FlightRequest flightRequest) {
		integrationFacade.sendConfirmationMessage(flightRequest);
		createFlightRequest(flightRequest);

		newFlightRequestEvent.fire(new NewFlightRequestEvent());

	}

	/**
	 * Handles offerd flight requests, their status is changed to OFFER_SENT and
	 * they are updated in the DB.
	 */
	@Override
	public void handleFlightRequestOffer(FlightRequest flightRequest, Offer offer) {
		flightRequest.setOffer(offer);
		flightRequest.setStatus(Status.OFFER_SENT);
		updateFlightRequest(flightRequest);
		integrationFacade.sendFlightRequestOfferMessage(flightRequest);
	}

	/**
	 * Handles rejected flight requests, their status is changed to REJECTED and
	 * they are updated in the DB.
	 */
	@Override
	public void handleFlightRequestRejects(FlightRequest flightRequest) {
		flightRequest.setOffer(null);
		flightRequest.setStatus(Status.REJECTED);
		updateFlightRequest(flightRequest);
		integrationFacade.sendFlightRequestRejectedMessage(flightRequest);
	}

	protected FlightRequestRepository getFlightRequestRepo() {
		return flightRequestRepo;
	}

	protected void setFlightRequestRepo(FlightRequestRepository flightRequestRepo) {
		this.flightRequestRepo = flightRequestRepo;
	}

	protected AirportRepository getAirportRepo() {
		return airportRepo;
	}

	protected void setAirportRepo(AirportRepository airportRepo) {
		this.airportRepo = airportRepo;
	}

	protected IntegrationFacade getIntegrationFacade() {
		return integrationFacade;
	}

	protected void setIntegrationFacade(IntegrationFacade integrationFacade) {
		this.integrationFacade = integrationFacade;
	}

	protected Event<NewFlightRequestEvent> getNewFlightRequestEvent() {
		return newFlightRequestEvent;
	}

	protected void setNewFlightRequestEvent(Event<NewFlightRequestEvent> newFlightRequestEvent) {
		this.newFlightRequestEvent = newFlightRequestEvent;
	}

	/**
	 * This method calculates the distance between two airports based on their
	 * coordinates using the harversine formula. For simplicity, I assume that
	 * the Earth is a perfect sphere and that the plane will fly in a stright
	 * line.
	 * 
	 * See {@link http://en.wikipedia.org/wiki/Haversine_formula} for details.
	 * 
	 * @param depAirport
	 *            airport 1
	 * @param arrAirport
	 *            airport 2
	 * @return distance between airports in kilometers
	 */
	private int calculateDistance(Airport depAirport, Airport arrAirport) {

		int earthRadius = 6371;

		double lat1 = Math.toRadians(depAirport.getLatitude());
		double lat2 = Math.toRadians(arrAirport.getLatitude());
		double lon1 = Math.toRadians(depAirport.getLongitude());
		double lon2 = Math.toRadians(arrAirport.getLongitude());

		double deltaLat = lat2 - lat1;
		double deltaLon = lon2 - lon1;

		double halfChordSquared = (Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2)) + (Math.cos(lat1) * Math.cos(lat2)
		        * Math.sin(deltaLon / 2) * Math.sin(deltaLon / 2));

		double angularDistance = 2 * Math.atan2(Math.sqrt(halfChordSquared), Math.sqrt(1 - halfChordSquared));

		return Math.round((float) (earthRadius * angularDistance));
	}

}
