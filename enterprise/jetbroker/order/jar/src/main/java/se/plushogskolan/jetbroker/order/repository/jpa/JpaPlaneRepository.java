package se.plushogskolan.jetbroker.order.repository.jpa;

import java.util.List;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.repository.PlaneRepository;

/**
 * Implementation of plane repository using JPA for persistance
 * 
 * @author marcus
 *
 */
public class JpaPlaneRepository extends JpaRepository<Plane> implements PlaneRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<Plane> getAllPlanes() {
		return em.createQuery("select p from Plane p").getResultList();
	}
}
