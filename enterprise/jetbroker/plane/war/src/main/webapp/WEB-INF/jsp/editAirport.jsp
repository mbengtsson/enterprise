<%@ page contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<title><spring:message code="editAirport.webtitle" /></title>

<link href="<%=request.getContextPath()%>/style/common.css" type="text/css" rel="stylesheet"  />

</head>

<body>

	<jsp:include page="header.jsp" />

	<h2 class="underline">
	<img src="<%=request.getContextPath()%>/images/airport.png">
	<c:choose>
		<c:when test="${editAirportBean.id > 0}">
			<spring:message code="editAirport.title.update" />
		</c:when>
		<c:otherwise>
			<spring:message code="editAirport.title.create" />
		</c:otherwise>
	</c:choose>
	</h2>

	<form:form commandName="editAirportBean">
		<form:hidden path="id"/>
		<table class="formTable">
			<c:if test="${editAirportBean.id > 0}">
			<tr>
				<th><spring:message code="global.id" /></th>
				<td>${editAirportBean.id}</td>
				<td></td>
			</tr>
			</c:if>
			
			<tr>
				<th><spring:message code="airport.code" /></th>
				<td><form:input path="code"/></td>
				<td><form:errors path="code" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th><spring:message code="global.name" /></th>
				<td><form:input path="name"/></td>
				<td><form:errors path="name" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th><spring:message code="airport.latitude" /></th>
				<td><form:input path="latitude" size="20" maxlength="20"/></td>
				<td><form:errors path="latitude" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th><spring:message code="airport.longitude" /></th>
				<td><form:input path="longitude" size="20" maxlength="20"/></td>
				<td><form:errors path="longitude" cssClass="errors" /></td>
			</tr>
			
			<tr>
				<th></th>
				<td>
					<c:set var="submitText">
						<spring:message code="global.submit" />
					</c:set>
					<input type="submit" value="${submitText}"/> <a href="<%=request.getContextPath()%>/index.html"><spring:message code="global.cancel" /></a></td>
				<td></td>
			</tr>
			
		</table>
	</form:form>

</body>
</html>