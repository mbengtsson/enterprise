package se.plushogskolan.jetbroker.plane.repository.jpa;

import java.util.List;

import javax.persistence.Query;

import org.joda.time.DateTime;

import se.plushogskolan.jee.utils.repository.jpa.JpaRepository;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;

public class JpaAirportRepository extends JpaRepository<Airport> implements AirportRepository {

	@Override
	public List<Airport> getAllAirports() {
		return em.createQuery("select a from Airport a").getResultList();
	}

	@Override
	public Airport getAirportByCode(String code) {
		Query query = em.createQuery("select a from Airport a where a.code = ?1");
		query.setParameter(1, code);
		List<Airport> resultList = query.getResultList();
		if (resultList.size() == 1) {
			return resultList.get(0);
		} else if (resultList.isEmpty()) {
			return null;
		} else {
			throw new RuntimeException("Received two airports for code " + code + ". This should not be possible.");
		}
	}

	public int deleteAirportsOlderThan(DateTime date) {

		Query query = em.createQuery("delete Airport a where a.lastUpdated < ?1 or a.lastUpdated is null");
		query.setParameter(1, date);
		return query.executeUpdate();
	}
}
