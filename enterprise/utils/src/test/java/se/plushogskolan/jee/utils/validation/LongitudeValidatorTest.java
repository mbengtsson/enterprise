package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class LongitudeValidatorTest {

	private LongitudeValidator validator;

	@Before
	public void setup() {
		validator = new LongitudeValidator();
	}

	@Test
	public void testIsValidDoubleOk() {
		assertTrue("OK", validator.isValid(45.25, null));
	}

	@Test
	public void testIsValidStringOk() {
		assertTrue("OK", validator.isValid("45.25", null));
	}

	@Test
	public void testIsValidIntOk() {
		assertTrue("OK", validator.isValid(45, null));
	}

	@Test
	public void testIsValidZero() {
		assertFalse("0", validator.isValid(0, null));
	}

	@Test
	public void testIsValidEmpty() {
		assertFalse("", validator.isValid("", null));
	}

	@Test
	public void testIsValidTooSmall() {
		assertFalse("To small", validator.isValid(-181, null));
	}

	@Test
	public void testIsValidTooBig() {
		assertFalse("To big", validator.isValid(181, null));
	}

	@Test
	public void testIsValidNull() {
		assertFalse("null", validator.isValid(null, null));
	}
}
