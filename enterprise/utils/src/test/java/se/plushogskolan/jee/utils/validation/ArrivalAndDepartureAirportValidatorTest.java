package se.plushogskolan.jee.utils.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class ArrivalAndDepartureAirportValidatorTest {

	private ArrivalAndDepartureAirportValidator validator;

	@Before
	public void setup() {
		validator = new ArrivalAndDepartureAirportValidator();
	}

	@Test
	public void testIsValidOk() {
		assertTrue("OK", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return "GOT";
			}

			@Override
			public String getArrivalAirportCode() {
				return "ARN";
			}
		}, null));
	}

	@Test
	public void testIsValidSame() {
		assertFalse("Same", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return "GOT";
			}

			@Override
			public String getArrivalAirportCode() {
				return "GOT";
			}
		}, null));
	}

	@Test
	public void testIsValidNull() {
		assertFalse("Invalid", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return null;
			}

			@Override
			public String getArrivalAirportCode() {
				return "ARN";
			}
		}, null));

		assertFalse("Invalid", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return "GOT";
			}

			@Override
			public String getArrivalAirportCode() {
				return null;
			}
		}, null));

		assertFalse("Invalid", validator.isValid(new ArrivalAndDepartureAirportHolder() {

			@Override
			public String getDepartureAirportCode() {
				return null;
			}

			@Override
			public String getArrivalAirportCode() {
				return null;
			}
		}, null));
	}

}
