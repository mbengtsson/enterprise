package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ArrivalAndDepartureAirportValidator implements
        ConstraintValidator<ArrivalAndDepartureAirport, ArrivalAndDepartureAirportHolder> {

	@Override
	public void initialize(ArrivalAndDepartureAirport constraintAnnotation) {

	}

	@Override
	public boolean isValid(ArrivalAndDepartureAirportHolder value, ConstraintValidatorContext context) {

		if (value.getArrivalAirportCode() != null && value.getDepartureAirportCode() != null) {
			if (!value.getArrivalAirportCode().equals(value.getDepartureAirportCode()))
				return true;
		}

		return false;
	}

}
