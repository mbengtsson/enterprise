package se.plushogskolan.jee.utils.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LongitudeValidator implements ConstraintValidator<Longitude, Object> {

	@Override
	public void initialize(Longitude constraintAnnotation) {

	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {

		double longitude = 0;

		if (value instanceof Double) {
			longitude = (Double) value;
		} else if (value instanceof Integer) {
			longitude = ((Integer) value).doubleValue();
		} else if (value instanceof String) {
			try {
				longitude = Double.parseDouble((String) value);
			} catch (NumberFormatException e) {
				return false;
			}
		} else {
			return false;
		}

		if (longitude != 0.0 && longitude >= -180.0 && longitude <= 180.0) {
			return true;
		}

		return false;

	}

}
