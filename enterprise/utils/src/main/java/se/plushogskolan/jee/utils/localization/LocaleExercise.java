package se.plushogskolan.jee.utils.localization;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleExercise {

	private ResourceBundle messages;

	public Object sayHello(Locale locale) {
		messages = ResourceBundle.getBundle("messages", locale);

		return messages.getString("sayHello");
	}

	public Object sayName(String name, Locale locale) {
		messages = ResourceBundle.getBundle("messages", locale);

		return MessageFormat.format(messages.getString("sayName"), name);
	}

	public Object saySomethingEnglish(Locale locale) {
		messages = ResourceBundle.getBundle("messages", locale);

		return messages.getString("saySomethingEnglish");
	}

	public Object introduceYourself(int age, String city, String name, Locale locale) {
		messages = ResourceBundle.getBundle("messages", locale);

		return MessageFormat.format(messages.getString("introduction"), name, age, city);
	}

	public Object sayApointmentDate(Date date, Locale locale) {
		messages = ResourceBundle.getBundle("messages", locale);

		MessageFormat messageFormat = new MessageFormat(messages.getString("sayApointmentDate"), locale);
		Object[] args = { date };

		return messageFormat.format(args);
	}

	public Object sayPrice(double price, Locale locale) {
		messages = ResourceBundle.getBundle("messages", locale);

		MessageFormat messageFormat = new MessageFormat(messages.getString("sayPrice"), locale);
		Object[] args = { price };

		return messageFormat.format(args);
	}

	public Object sayFractionDigits(double number) {
		messages = ResourceBundle.getBundle("messages");

		return MessageFormat.format(messages.getString("sayFractionDigits"), number);
	}

}
