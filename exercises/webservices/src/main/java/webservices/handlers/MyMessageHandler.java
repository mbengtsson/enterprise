package webservices.handlers;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.util.Set;

/**
 * A message handler that can listen to outgoing and incoming message.
 * <p/>
 * By using the callbacks in this handler, we can access various parts of the soap document.
 * <p/>
 * This class should be implemented to listen to soap messages and transform them to xml. Outgoing soap messages should
 * be stored in soapBodyRequestXml, and incoming soap messages should be stored in soapBodyResponseXml.
 */
public class MyMessageHandler implements SOAPHandler<SOAPMessageContext> {

	private String soapBodyRequestXml;
	private String soapBodyResponseXml;

	public String getSoapBodyRequestXml() {
		return soapBodyRequestXml;
	}

	public String getSoapBodyResponseXml() {
		return soapBodyResponseXml;
	}

	@Override
	public void close(MessageContext arg0) {
		System.out.println("MyMessageHandler: close");

	}

	@Override
	public boolean handleFault(SOAPMessageContext arg0) {
		System.out.println("MyMessageHandler: handleFault");
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		System.out.println("MyMessageHandler: handleMessage");

		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		try {
			if (outboundProperty.booleanValue()) {
				this.soapBodyRequestXml = convertSoapMessageToString(context.getMessage());
			} else {
				this.soapBodyResponseXml = convertSoapMessageToString(context.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		System.out.println("MyMessageHandler: getHeaders");
		return null;
	}

	/**
	 * A utility method to transform a SOAPMessage to string
	 */
	private String convertSoapMessageToString(SOAPMessage msg) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		msg.writeTo(baos);
		return baos.toString();
	}

}
