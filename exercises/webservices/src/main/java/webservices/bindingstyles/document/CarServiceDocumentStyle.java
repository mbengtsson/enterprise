package webservices.bindingstyles.document;

import webservices.bindingstyles.CarService;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * Add annotations to make this a document style web service
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT)
public interface CarServiceDocumentStyle extends CarService {

}
