package webservices.bindingstyles.document;

import webservices.bindingstyles.Car;
import webservices.bindingstyles.CarRepository;
import webservices.bindingstyles.RegNumber;

import javax.jws.WebService;

/**
 * Implement the CarService web service.
 */
@WebService(endpointInterface = "webservices.bindingstyles.document.CarServiceDocumentStyle")
public class CarServiceDocumentStyleImpl implements CarServiceDocumentStyle {

	private CarRepository repo = new CarRepository();

	public static void main(String[] args) {
		// Implement so that this web service can start itself
	}

	@Override
	public Car getCar(RegNumber regNo) {
		return repo.getCar(regNo);
	}

}
