package webservices.bindingstyles;

import javax.jws.WebMethod;

public interface CarService {

	@WebMethod
	public Car getCar(RegNumber regNo);
}
