package webservices.bindingstyles.rpc;

import webservices.bindingstyles.Car;
import webservices.bindingstyles.CarRepository;
import webservices.bindingstyles.RegNumber;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * Implement the CarService web service.
 */
@WebService(endpointInterface = "webservices.bindingstyles.rpc.CarServiceRpcStyle")
public class CarServiceRpcStyleImpl implements CarServiceRpcStyle {

	private CarRepository repo = new CarRepository();

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8180/carServiceRpc", new CarServiceRpcStyleImpl());
	}

	@Override
	public Car getCar(RegNumber regNo) {
		return repo.getCar(regNo);
	}

}
