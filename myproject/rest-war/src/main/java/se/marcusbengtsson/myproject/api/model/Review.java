package se.marcusbengtsson.myproject.api.model;

/**
 * Object used to store an api review response
 *
 * @author Marcus Bengtsson
 */
public class Review {

	private int steamId;

	private String title;

	private String about;

	private String summary;

	private String review;

	private String reviewer;

	private int score;

	private String siteUrl;

	public int getSteamId() {
		return steamId;
	}

	public void setSteamId(int steamId) {
		this.steamId = steamId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}
}
