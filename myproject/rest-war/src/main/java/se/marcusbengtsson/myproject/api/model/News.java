package se.marcusbengtsson.myproject.api.model;

import java.util.List;

/**
 * Object used to store an api news response
 *
 * @author Marcus Bengtsson
 */
public class News {

	private int steamId;

	private String title;

	private List<NewsItem> news;

	public int getSteamId() {
		return steamId;
	}

	public void setSteamId(int steamId) {
		this.steamId = steamId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<NewsItem> getNews() {
		return news;
	}

	public void setNews(List<NewsItem> news) {
		this.news = news;
	}
}
