package se.marcusbengtsson.myproject.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import se.marcusbengtsson.myproject.api.model.News;
import se.marcusbengtsson.myproject.api.model.NewsItem;
import se.marcusbengtsson.myproject.api.model.Review;
import se.marcusbengtsson.myproject.api.model.ReviewAndNews;
import se.marcusbengtsson.myproject.domain.GbGameDetail;
import se.marcusbengtsson.myproject.domain.GbReview;
import se.marcusbengtsson.myproject.domain.GbSearchResult;
import se.marcusbengtsson.myproject.domain.SteamNews;
import se.marcusbengtsson.myproject.services.GiantBombService;
import se.marcusbengtsson.myproject.services.SteamService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Controller used for the REST API
 *
 * @author Marcus Bengtsson
 */
@Controller
public class ApiController {

	Logger log = Logger.getLogger(ApiController.class.getName());

	@Inject
	SteamService steamService;

	@Inject
	GiantBombService giantBombService;

	@RequestMapping(value = "/getReviewAndNewsById/{id}", method = RequestMethod.GET,
			produces = "application/json")
	@ResponseBody
	public ResponseEntity<ReviewAndNews> getReviewAndNewsById(@PathVariable int id) {

		log.fine("REST API: getReviewAndNewsById called with: " + id);

		String title = steamService.getAppNameById(id);

		if (title == null) {
			return new ResponseEntity<ReviewAndNews>(new ReviewAndNews(), HttpStatus.NOT_FOUND);
		}

		List<NewsItem> news = new ArrayList<NewsItem>();
		for (SteamNews steamNews : steamService.getSteamNewsList(id)) {
			news.add(new NewsItem(steamNews.getTitle(), steamNews.getUrl(), steamNews.getSource()));
		}

		GbGameDetail details;
		List<GbSearchResult> searchResults = giantBombService.getGbSearchResultList(title);
		if (!searchResults.isEmpty()) {
			details = giantBombService.getGameDetail(searchResults.get(0).getGameDetailUrl());
		} else {
			details = new GbGameDetail(null, null, null);
		}

		GbReview gbReview;
		List<String> reviewUrls = details.getReviewUrls();
		if (reviewUrls != null && !reviewUrls.isEmpty()) {
			gbReview = giantBombService.getGameReview(details.getReviewUrls().get(0));
		} else {
			gbReview = new GbReview();
		}

		ReviewAndNews reviewAndNews = new ReviewAndNews();

		reviewAndNews.setSteamId(id);
		reviewAndNews.setTitle(title);
		reviewAndNews.setAbout(details.getSummary());
		reviewAndNews.setSummary(gbReview.getSummary());
		reviewAndNews.setReview(gbReview.getReview());
		reviewAndNews.setScore(gbReview.getScore());
		reviewAndNews.setReviewer(gbReview.getReviewer());
		reviewAndNews.setSiteUrl(gbReview.getSiteUrl());
		reviewAndNews.setPictureUrl(details.getImageUrl());
		reviewAndNews.setNews(news);

		return new ResponseEntity<ReviewAndNews>(reviewAndNews, HttpStatus.OK);
	}

	@RequestMapping(value = "/getReviewAndNewsByTitle/{title}", method = RequestMethod.GET,
			produces = "application/json")
	@ResponseBody
	public ResponseEntity<ReviewAndNews> getReviewAndNewsByTitle(@PathVariable String title) {

		log.fine("REST API: getReviewAndNewsByTitle called with: " + title);

		return getReviewAndNewsById(steamService.getAppIdByName(title));
	}

	@RequestMapping(value = "/getReviewById/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Review> getReviewById(@PathVariable int id) {

		log.fine("REST API: getReviewById called with: " + id);

		String title = steamService.getAppNameById(id);

		if (title == null) {
			return new ResponseEntity<Review>(new Review(), HttpStatus.NOT_FOUND);
		}

		GbGameDetail details;
		List<GbSearchResult> searchResults = giantBombService.getGbSearchResultList(title);
		if (!searchResults.isEmpty()) {
			details = giantBombService.getGameDetail(searchResults.get(0).getGameDetailUrl());
		} else {
			details = new GbGameDetail(null, null, null);
		}

		GbReview gbReview;
		List<String> reviewUrls = details.getReviewUrls();
		if (reviewUrls != null && !reviewUrls.isEmpty()) {
			gbReview = giantBombService.getGameReview(details.getReviewUrls().get(0));
		} else {
			gbReview = new GbReview();
		}

		Review review = new Review();

		review.setSteamId(id);
		review.setTitle(title);
		review.setAbout(details.getSummary());
		review.setSummary(gbReview.getSummary());
		review.setReview(gbReview.getReview());
		review.setScore(gbReview.getScore());
		review.setReviewer(gbReview.getReviewer());
		review.setSiteUrl(gbReview.getSiteUrl());

		return new ResponseEntity<Review>(review, HttpStatus.OK);
	}

	@RequestMapping(value = "/getReviewByTitle/{title}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Review> getReviewByTitle(@PathVariable String title) {

		log.fine("REST API: getReviewByTitle called with: " + title);

		return getReviewById(steamService.getAppIdByName(title));
	}

	@RequestMapping(value = "/getNewsById/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<News> getNewsById(@PathVariable int id) {

		log.fine("REST API: getNewsById called with: " + id);

		String title = steamService.getAppNameById(id);

		if (title == null) {
			return new ResponseEntity<News>(new News(), HttpStatus.NOT_FOUND);
		}

		List<NewsItem> newsList = new ArrayList<NewsItem>();
		for (SteamNews steamNews : steamService.getSteamNewsList(id)) {
			newsList.add(new NewsItem(steamNews.getTitle(), steamNews.getUrl(), steamNews.getSource()));
		}

		News news = new News();

		news.setSteamId(id);
		news.setTitle(title);
		news.setNews(newsList);

		return new ResponseEntity<News>(news, HttpStatus.OK);
	}

	@RequestMapping(value = "/getNewsByTitle/{title}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<News> getNewsByTitle(@PathVariable String title) {

		log.fine("REST API: getNewsByTitle called with: " + title);

		return getNewsById(steamService.getAppIdByName(title));
	}

}
