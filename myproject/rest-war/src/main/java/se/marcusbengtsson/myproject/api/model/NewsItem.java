package se.marcusbengtsson.myproject.api.model;

/**
 * Object used to store a news-item for an api news response
 *
 * @author Marcus Bengtsson
 */
public class NewsItem {

	private String headline;

	private String url;

	private String source;

	public NewsItem(String headline, String url, String source) {
		this.headline = headline;
		this.url = url;
		this.source = source;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
