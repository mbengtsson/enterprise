package se.marcusbengtsson.myproject.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import se.marcusbengtsson.myproject.domain.SteamApp;
import se.marcusbengtsson.myproject.mvc.bean.SteamAppBean;
import se.marcusbengtsson.myproject.services.SteamService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by marcus on 2014-10-16.
 */

@Controller
public class IndexController {

	@Inject
	SteamService steamService;

	@RequestMapping(value = "/index.html", method = RequestMethod.GET)
	public ModelAndView index() {

		List<SteamApp> appList = new ArrayList<SteamApp>(steamService.getAllSteamAppsAsMap().values());
		Collections.sort(appList);

		ModelAndView mav = new ModelAndView("index");
		mav.addObject("appList", appList);
		mav.addObject("alphabet", "$ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray());
		mav.addObject("appBean", new SteamAppBean());

		return mav;
	}

	@RequestMapping(value = "/index.html", method = RequestMethod.POST)
	public ModelAndView handleSubmit(SteamAppBean bean) {

		return new ModelAndView("redirect:/review/" + bean.getAppId() + ".html");
	}
}
