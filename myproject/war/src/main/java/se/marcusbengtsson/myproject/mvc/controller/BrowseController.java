package se.marcusbengtsson.myproject.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import se.marcusbengtsson.myproject.domain.SteamApp;
import se.marcusbengtsson.myproject.services.SteamService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by marcus on 2014-10-20.
 */
@Controller
public class BrowseController {

	@Inject
	SteamService steamService;

	@RequestMapping(value = "/browse/{letter}.html", method = RequestMethod.GET)
	public ModelAndView review(@PathVariable char letter) {
		List<SteamApp> appList = new ArrayList<SteamApp>(steamService.getSteamAppsAsMapByLetter(letter).values());
		Collections.sort(appList);

		ModelAndView mav = new ModelAndView("browse");
		mav.addObject("appList", appList);
		mav.addObject("letter", letter);
		return mav;
	}

}
