package se.marcusbengtsson.myproject.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import se.marcusbengtsson.myproject.domain.GbGameDetail;
import se.marcusbengtsson.myproject.domain.GbReview;
import se.marcusbengtsson.myproject.domain.GbSearchResult;
import se.marcusbengtsson.myproject.domain.SteamNews;
import se.marcusbengtsson.myproject.services.GiantBombService;
import se.marcusbengtsson.myproject.services.SteamService;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by marcus on 2014-10-20.
 */
@Controller
public class ReviewController {

	@Inject
	SteamService steamService;
	@Inject
	GiantBombService giantBombService;

	@RequestMapping(value = "/review/{appId}.html", method = RequestMethod.GET)
	public ModelAndView review(@PathVariable int appId) {

		String name = steamService.getAppNameById(appId);
		List<SteamNews> news = steamService.getSteamNewsList(appId);

		List<GbSearchResult> searchResults = giantBombService.getGbSearchResultList(name);
		GbGameDetail details;
		if (!searchResults.isEmpty()) {
			details = giantBombService.getGameDetail(searchResults.get(0).getGameDetailUrl());
		} else {
			details = new GbGameDetail(null, null, null);
		}
		List<String> reviewUrls = details.getReviewUrls();
		GbReview review;
		if (reviewUrls != null && !reviewUrls.isEmpty()) {
			review = giantBombService.getGameReview(details.getReviewUrls().get(0));
		} else {
			review = new GbReview("Sorry, there are no review for this title");
		}
		ModelAndView mav = new ModelAndView("review");
		mav.addObject("appId", appId);
		mav.addObject("name", name);
		mav.addObject("news", news);
		mav.addObject("details", details);
		mav.addObject("review", review);

		return mav;

	}
}
