package se.marcusbengtsson.myproject.mvc.bean;

/**
 * Created by marcus on 2014-10-17.
 */
public class SteamAppBean {

	int appId;

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}
}
