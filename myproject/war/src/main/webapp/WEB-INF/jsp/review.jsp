<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>SteamReviews - ${name} review</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="<%=request.getContextPath()%>/style/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">
    <jsp:include page="header.jsp"/>
    <section class="title">
        <h1>${name}</h1>
        <a href="http://store.steampowered.com/app/${appId}">Get &lsquo;${name}&rsquo; on Steam</a>
    </section>

    <section id="summary">
        <p>${details.summary}</p>
    </section>

    <section id="image">
        <img src="${details.imageUrl}">
    </section>

    <section id="review">
        <h2>Review:</h2>
        <c:if test="${review.score != -1}">
            <h3>Rating: ${review.score}/5 Reviewer: ${review.reviewer}</h3>
        </c:if>

        <p>${review.summary}</p>

        <p>${review.review}</p>
        <c:if test="${review.score != -1}">
            <a href="${review.siteUrl}">Read review on GiantBomb</a>
        </c:if>
    </section>

    <section id="news">
        <c:if test="${not empty news}">
            <h2>News: </h2>
            <c:forEach items="${news}" var="gameNews">
                <div class="newsItem">
                    <h3>${gameNews.title}</h3>
                    <a href="${gameNews.url}">${gameNews.source}</a>
                </div>
            </c:forEach>
        </c:if>
    </section>
    <div class="push"></div>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
