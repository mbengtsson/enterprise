<header>
    <a href="<%=request.getContextPath()%>/index.html"><img id="title"
                                                            src="<%=request.getContextPath()%>/img/steamreviews.png"
                                                            alt="SteamReviews"></a>

    <div id="logos">
        <a href="http://store.steampowered.com/"><img src="<%=request.getContextPath()%>/img/steamlogo.png" alt="Steam"></a>
        <a href="http://www.giantbomb.com/"><img src="<%=request.getContextPath()%>/img/gblogo.png" alt="GiantBomb"></a>
    </div>
</header>
<nav>
    <a href="<%=request.getContextPath()%>/index.html">Home</a>&nbsp;&nbsp;-&nbsp;&nbsp;<a
        href="${header.referer}">Back</a>
</nav>
