<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>SteamReviews - Browse ${letter}</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="<%=request.getContextPath()%>/style/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">
    <jsp:include page="header.jsp"/>
    <section class="title">
        <h1>${letter}</h1>
    </section>

    <section class="columns">
        <c:forEach items="${appList}" var="app">
            <a href="<%=request.getContextPath()%>/review/${app.appId}.html">${app.name}</a><br>
        </c:forEach>
    </section>
    <div class="push"></div>
</div>
<jsp:include page="footer.jsp"/>
</body>
</html>
