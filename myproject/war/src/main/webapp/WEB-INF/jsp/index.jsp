<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>SteamReviews - Index</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="<%=request.getContextPath()%>/js/select2/select2.css" type="text/css" rel="stylesheet"/>
    <link href="<%=request.getContextPath()%>/style/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">
    <jsp:include page="header.jsp"/>
    <section id="choose">
        <h3>
            Choose a game from steam:
        </h3>

        <p>
            <form:form commandName="appBean">
                <form:select style="width:600px" id="selectApp" path="appId">
                    <form:options items="${appList}" itemValue="appId" itemLabel="name"/>
                </form:select>
                &nbsp;
                <input id="button" type="submit" value="GO!"/>
            </form:form>
        </p>
    </section>

    <section id="browse">
        <h3>
            Browse by letter:
        </h3>

        <p id="letter">
            <c:forEach items="${alphabet}" var="letter">
                <a href="<%=request.getContextPath()%>/browse/${letter}.html">${letter}</a>&nbsp;
            </c:forEach>
        </p>
    </section>
    <div class="push"></div>
</div>
<jsp:include page="footer.jsp"/>
<script src="<%=request.getContextPath()%>/js/jquery/jquery-1.11.1.js"></script>
<script src="<%=request.getContextPath()%>/js/select2/select2.js"></script>
<script src="<%=request.getContextPath()%>/js/selectapp.js"></script>
</body>
</html>
