package se.marcusbengtsson.myproject.services.impl;

import org.junit.Before;
import org.junit.Test;
import se.marcusbengtsson.myproject.domain.SteamApp;
import se.marcusbengtsson.myproject.domain.SteamNews;
import se.marcusbengtsson.myproject.restclient.dao.steam.SteamAppListDao;
import se.marcusbengtsson.myproject.restclient.dao.steam.SteamNewsDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class SteamServiceImplTest {

	SteamServiceImpl steamServcie;

	Map<String, SteamApp> appMap;
	List<SteamNews> newsList;

	@Before
	public void setup() {
		steamServcie = new SteamServiceImpl();

		appMap = new HashMap<String, SteamApp>();
		appMap.put("A title", new SteamApp(1, "A title"));
		appMap.put("B title", new SteamApp(2, "B title"));
		appMap.put("9 title", new SteamApp(3, "9 title"));

		newsList = new ArrayList<SteamNews>();
	}

	@Test
	public void testUpdateSteamAppList() throws Exception {

		//Setup mock dao
		SteamAppListDao dao = createMock(SteamAppListDao.class);
		dao.updateAppList();
		expectLastCall();
		replay(dao);
		steamServcie.setSteamAppListDao(dao);

		//Perform test
		steamServcie.updateSteamAppList();

		//Verify
		verify(dao);

	}

	@Test
	public void testGetAllSteamAppsAsMap() throws Exception {

		//Setup mock dao
		SteamAppListDao dao = createMock(SteamAppListDao.class);
		dao.updateAppList();
		expectLastCall();
		expect(dao.getAllSteamAppsMap()).andReturn(appMap);
		replay(dao);
		steamServcie.setSteamAppListDao(dao);

		//Perform test
		assertEquals(appMap, steamServcie.getAllSteamAppsAsMap());

		//Verify
		verify(dao);

	}

	@Test
	public void testGetSteamAppsAsMapByLetter() throws Exception {

		//Setup mock dao
		SteamAppListDao dao = createMock(SteamAppListDao.class);
		dao.updateAppList();
		expectLastCall().atLeastOnce();
		expect(dao.getAllSteamAppsMap()).andReturn(appMap).atLeastOnce();
		replay(dao);
		steamServcie.setSteamAppListDao(dao);

		//Perform test
		assertEquals(1, steamServcie.getSteamAppsAsMapByLetter('9').size());
		assertEquals(1, steamServcie.getSteamAppsAsMapByLetter('A').size());

		//Verify
		verify(dao);

	}

	@Test
	public void testGetAppNameById() throws Exception {

		//Setup mock dao
		SteamAppListDao dao = createMock(SteamAppListDao.class);
		dao.updateAppList();
		expectLastCall();
		expect(dao.getAllSteamAppsMap()).andReturn(appMap);
		replay(dao);
		steamServcie.setSteamAppListDao(dao);

		//Perform test
		assertEquals("A title", steamServcie.getAppNameById(1));

		//Verify
		verify(dao);

	}

	@Test
	public void testGetAppIdByName() throws Exception {

		//Setup mock dao
		SteamAppListDao dao = createMock(SteamAppListDao.class);
		dao.updateAppList();
		expectLastCall();
		expect(dao.getAllSteamAppsMap()).andReturn(appMap);
		replay(dao);
		steamServcie.setSteamAppListDao(dao);

		//Perform test
		assertEquals(3, steamServcie.getAppIdByName("9 title"));

		//Verify
		verify(dao);
	}

	@Test
	public void testGetSteamNewsList() throws Exception {

		//Setup mock dao
		SteamNewsDao dao = createMock(SteamNewsDao.class);
		expect(dao.getSteamNewsList(1)).andReturn(newsList);
		replay(dao);
		steamServcie.setSteamNewsDao(dao);

		//Perform test
		assertEquals(newsList, steamServcie.getSteamNewsList(1));

		//Verify
		verify(dao);

	}
}
