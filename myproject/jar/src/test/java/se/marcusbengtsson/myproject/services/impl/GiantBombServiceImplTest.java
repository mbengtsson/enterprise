package se.marcusbengtsson.myproject.services.impl;

import org.junit.Before;
import org.junit.Test;
import se.marcusbengtsson.myproject.domain.GbGameDetail;
import se.marcusbengtsson.myproject.domain.GbReview;
import se.marcusbengtsson.myproject.domain.GbSearchResult;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbGameDetailDao;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbReviewDao;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbSearchResultDao;

import java.util.ArrayList;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

public class GiantBombServiceImplTest {

	GiantBombServiceImpl gbService;

	String exampleUrl = "http://exapmle.com";
	String searchString = "example search";

	GbSearchResult searchResult;
	GbGameDetail gameDetail;
	GbReview review;

	List<GbSearchResult> searchResults;

	@Before
	public void setup() {
		gbService = new GiantBombServiceImpl();

		searchResult = new GbSearchResult(exampleUrl, "example");
		gameDetail = new GbGameDetail("example", exampleUrl, new ArrayList<String>());
		review = new GbReview("example", "example", "John Doe", 5, exampleUrl);

		searchResults = new ArrayList<GbSearchResult>();
		searchResults.add(searchResult);
	}

	@Test
	public void testGetGbSearchResultList() throws Exception {

		//Setup mock dao
		GbSearchResultDao dao = createMock(GbSearchResultDao.class);
		expect(dao.getGbSearchResultList(searchString)).andReturn(searchResults);
		replay(dao);
		gbService.setGbSearchResultDao(dao);

		//Perform test
		assertEquals(searchResults, gbService.getGbSearchResultList(searchString));

		//Veify
		verify(dao);

	}

	@Test
	public void testGetGameDetail() throws Exception {

		//Setup mock dao
		GbGameDetailDao dao = createMock(GbGameDetailDao.class);
		expect(dao.getGameDetail(exampleUrl)).andReturn(gameDetail);
		replay(dao);
		gbService.setGbGameDetailDao(dao);

		//Perform test
		assertEquals(gameDetail, gbService.getGameDetail(exampleUrl));

		//Verify
		verify(dao);

	}

	@Test
	public void testGetGameReview() throws Exception {

		//Setup mocj dao
		GbReviewDao dao = createMock(GbReviewDao.class);
		expect(dao.getGameReview(exampleUrl)).andReturn(review);
		replay(dao);
		gbService.setGbReviewDao(dao);

		//Perform test
		assertEquals(review, gbService.getGameReview(exampleUrl));

		//Verify
		verify(dao);

	}
}
