package se.marcusbengtsson.myproject.domain;

/**
 * Domain object containing a steam-app
 *
 * @author Marcus Bengtsson
 */
public class SteamApp implements Comparable<SteamApp> {

	private int appId;

	private String name;

	public SteamApp(int appId, String name) {
		this.appId = appId;
		this.name = name;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(SteamApp steamApp) {

		return getName().compareTo(steamApp.getName());
	}

	@Override
	public String toString() {
		return "SteamApp{" +
				"appId=" + appId +
				", name='" + name + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof SteamApp)) {
			return false;
		}

		SteamApp steamApp = (SteamApp) o;

		if (appId != steamApp.appId) {
			return false;
		}
		if (!name.equals(steamApp.name)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = appId;
		result = 31 * result + name.hashCode();
		return result;
	}
}
