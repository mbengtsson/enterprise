package se.marcusbengtsson.myproject.restclient.dao.giantbomb;

import se.marcusbengtsson.myproject.domain.GbGameDetail;

import javax.ejb.Local;

/**
 * Interface for a giant-bomb game-detail data access object
 *
 * @author Marcus Bengtsson
 */
@Local
public interface GbGameDetailDao {

	GbGameDetail getGameDetail(String gameDetailUrl);
}
