package se.marcusbengtsson.myproject.services;

import se.marcusbengtsson.myproject.domain.SteamApp;
import se.marcusbengtsson.myproject.domain.SteamNews;

import javax.ejb.Local;
import java.util.List;
import java.util.Map;

/**
 * Interface for the steam service
 *
 * @author Marcus Bengtsson
 */
@Local
public interface SteamService {

	void updateSteamAppList();

	Map<String, SteamApp> getAllSteamAppsAsMap();

	Map<String, SteamApp> getSteamAppsAsMapByLetter(char letter);

	String getAppNameById(int appId);

	int getAppIdByName(String name);

	List<SteamNews> getSteamNewsList(int appId);
}
