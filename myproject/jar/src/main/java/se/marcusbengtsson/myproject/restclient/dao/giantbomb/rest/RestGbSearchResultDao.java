package se.marcusbengtsson.myproject.restclient.dao.giantbomb.rest;

import org.springframework.web.client.RestTemplate;
import se.marcusbengtsson.myproject.domain.GbSearchResult;
import se.marcusbengtsson.myproject.restclient.Constants;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbSearchResultDao;
import se.marcusbengtsson.myproject.restclient.model.giantbomb.GbSearchResultModel;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

/**
 * Giant-bomb searchresult data access object REST implementation REST implementation
 *
 * @author Marcus Bengtsson
 */
@Stateless
public class RestGbSearchResultDao implements GbSearchResultDao {

	@Override
	public List<GbSearchResult> getGbSearchResultList(String searchString) {

		RestTemplate restTemplate = new RestTemplate();

		GbSearchResultModel searchModel = restTemplate.getForObject("http://www.giantbomb.com/api/search/?api_key=" +
				Constants.GB_API_KEY + "&format=json&query=\"" + searchString + "\"&field_list=api_detail_url", GbSearchResultModel.class);

		List<GbSearchResult> searchResultList = new ArrayList<GbSearchResult>();

		for (GbSearchResultModel.Result result : searchModel.getResults()) {

			searchResultList.add(new GbSearchResult(result.getApiDetailUrl(), result.getResourceType()));
		}

		return searchResultList;
	}
}
