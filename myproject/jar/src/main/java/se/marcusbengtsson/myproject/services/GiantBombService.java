package se.marcusbengtsson.myproject.services;

import se.marcusbengtsson.myproject.domain.GbGameDetail;
import se.marcusbengtsson.myproject.domain.GbReview;
import se.marcusbengtsson.myproject.domain.GbSearchResult;

import javax.ejb.Local;
import java.util.List;

/**
 * Interface for the giant-bomb service
 *
 * @author Marcus Bengtsson
 */
@Local
public interface GiantBombService {

	List<GbSearchResult> getGbSearchResultList(String searchString);

	GbGameDetail getGameDetail(String gameDetailUrl);

	GbReview getGameReview(String reviewUrl);

}
