package se.marcusbengtsson.myproject.domain;

/**
 * Domain object containing a giantbomb-searchresult
 *
 * @author Marcus Bengtsson
 */
public class GbSearchResult {

	private String gameDetailUrl;

	private String resourceType;

	public GbSearchResult(String gameDetailUrl, String resourceType) {
		this.gameDetailUrl = gameDetailUrl;
		this.resourceType = resourceType;
	}

	public String getGameDetailUrl() {
		return gameDetailUrl;
	}

	public void setGameDetailUrl(String gameDetailUrl) {
		this.gameDetailUrl = gameDetailUrl;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	@Override
	public String toString() {
		return "GbSearchResult{" +
				"gameDetailUrl='" + gameDetailUrl + '\'' +
				", resourceType='" + resourceType + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof GbSearchResult)) {
			return false;
		}

		GbSearchResult that = (GbSearchResult) o;

		if (gameDetailUrl != null ? !gameDetailUrl.equals(that.gameDetailUrl) : that.gameDetailUrl != null) {
			return false;
		}
		if (resourceType != null ? !resourceType.equals(that.resourceType) : that.resourceType != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = gameDetailUrl != null ? gameDetailUrl.hashCode() : 0;
		result = 31 * result + (resourceType != null ? resourceType.hashCode() : 0);
		return result;
	}
}
