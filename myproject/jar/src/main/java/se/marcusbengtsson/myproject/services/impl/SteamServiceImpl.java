package se.marcusbengtsson.myproject.services.impl;

import se.marcusbengtsson.myproject.domain.SteamApp;
import se.marcusbengtsson.myproject.domain.SteamNews;
import se.marcusbengtsson.myproject.restclient.dao.steam.SteamAppListDao;
import se.marcusbengtsson.myproject.restclient.dao.steam.SteamNewsDao;
import se.marcusbengtsson.myproject.services.SteamService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Steam service implementation
 *
 * @author Marcus Bengtsson
 */
@Stateless
public class SteamServiceImpl implements SteamService {

	@Inject
	private SteamAppListDao steamAppListDao;

	@Inject
	private SteamNewsDao steamNewsDao;

	@Override
	public void updateSteamAppList() {
		steamAppListDao.updateAppList();
	}

	@Override
	public Map<String, SteamApp> getAllSteamAppsAsMap() {

		updateSteamAppList();

		return steamAppListDao.getAllSteamAppsMap();
	}

	@Override
	public Map<String, SteamApp> getSteamAppsAsMapByLetter(char letter) {

		Map<String, SteamApp> steamAppMap = new HashMap<String, SteamApp>();

		for (Entry<String, SteamApp> entry : getAllSteamAppsAsMap().entrySet()) {

			if (Character.toString(letter).toLowerCase().equals(Character.toString(entry.getKey().charAt(0))
					.toLowerCase())) {
				steamAppMap.put(entry.getKey(), entry.getValue());
			} else if (letter == '$' && "0123456789".contains(Character.toString(entry.getKey().charAt(0)))) {
				steamAppMap.put(entry.getKey(), entry.getValue());
			}
		}

		return steamAppMap;
	}

	@Override
	public String getAppNameById(int appId) {
		for (Entry<String, SteamApp> entry : getAllSteamAppsAsMap().entrySet()) {
			if (entry.getValue().getAppId() == appId) {
				return entry.getKey();
			}
		}

		return null;
	}

	@Override
	public int getAppIdByName(String name) {

		int appId;

		try {
			appId = getAllSteamAppsAsMap().get(name).getAppId();
		} catch (NullPointerException e) {
			appId = -1;
		}

		return appId;
	}

	@Override
	public List<SteamNews> getSteamNewsList(int appId) {
		return steamNewsDao.getSteamNewsList(appId);
	}

	protected void setSteamAppListDao(SteamAppListDao steamAppListDao) {
		this.steamAppListDao = steamAppListDao;
	}

	protected void setSteamNewsDao(SteamNewsDao steamNewsDao) {
		this.steamNewsDao = steamNewsDao;
	}
}
