package se.marcusbengtsson.myproject.restclient.dao.giantbomb;

import se.marcusbengtsson.myproject.domain.GbReview;

import javax.ejb.Local;

/**
 * Interface for a giant-bomb review data access object
 *
 * @author Marcus Bengtsson
 */
@Local
public interface GbReviewDao {

	GbReview getGameReview(String reviewUrl);
}
