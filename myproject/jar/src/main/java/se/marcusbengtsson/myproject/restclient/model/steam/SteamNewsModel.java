package se.marcusbengtsson.myproject.restclient.model.steam;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Object used to store a steam news response
 *
 * @author Marcus Bengtsson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SteamNewsModel {

	@JsonProperty("appnews")
	private Appnews appnews;

	@JsonProperty("appnews")
	public Appnews getAppnews() {
		return appnews;
	}

	@JsonProperty("appnews")
	public void setAppnews(Appnews appnews) {
		this.appnews = appnews;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Appnews {

		@JsonProperty("appid")
		private int appid;
		@JsonProperty("newsitems")
		private Newsitems newsitems;

		@JsonProperty("appid")
		public int getAppid() {
			return appid;
		}

		@JsonProperty("appid")
		public void setAppid(int appid) {
			this.appid = appid;
		}

		@JsonProperty("newsitems")
		public Newsitems getNewsitems() {
			return newsitems;
		}

		@JsonProperty("newsitems")
		public void setNewsitems(Newsitems newsitems) {
			this.newsitems = newsitems;
		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Newsitems {

		@JsonProperty("newsitem")
		private List<Newsitem> newsitem = new ArrayList<Newsitem>();

		@JsonProperty("newsitem")
		public List<Newsitem> getNewsitem() {
			return newsitem;
		}

		@JsonProperty("newsitem")
		public void setNewsitem(List<Newsitem> newsitem) {
			this.newsitem = newsitem;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Newsitem {

		@JsonProperty("gid")
		private String gid;
		@JsonProperty("title")
		private String title;
		@JsonProperty("url")
		private String url;
		@JsonProperty("is_external_url")
		private boolean isExternalUrl;
		@JsonProperty("author")
		private String author;
		@JsonProperty("contents")
		private String contents;
		@JsonProperty("feedlabel")
		private String feedlabel;
		@JsonProperty("date")
		private int date;
		@JsonProperty("feedname")
		private String feedname;

		@JsonProperty("gid")
		public String getGid() {
			return gid;
		}

		@JsonProperty("gid")
		public void setGid(String gid) {
			this.gid = gid;
		}

		@JsonProperty("title")
		public String getTitle() {
			return title;
		}

		@JsonProperty("title")
		public void setTitle(String title) {
			this.title = title;
		}

		@JsonProperty("url")
		public String getUrl() {
			return url;
		}

		@JsonProperty("url")
		public void setUrl(String url) {
			this.url = url;
		}

		@JsonProperty("is_external_url")
		public boolean isIsExternalUrl() {
			return isExternalUrl;
		}

		@JsonProperty("is_external_url")
		public void setIsExternalUrl(boolean isExternalUrl) {
			this.isExternalUrl = isExternalUrl;
		}

		@JsonProperty("author")
		public String getAuthor() {
			return author;
		}

		@JsonProperty("author")
		public void setAuthor(String author) {
			this.author = author;
		}

		@JsonProperty("contents")
		public String getContents() {
			return contents;
		}

		@JsonProperty("contents")
		public void setContents(String contents) {
			this.contents = contents;
		}

		@JsonProperty("feedlabel")
		public String getFeedlabel() {
			return feedlabel;
		}

		@JsonProperty("feedlabel")
		public void setFeedlabel(String feedlabel) {
			this.feedlabel = feedlabel;
		}

		@JsonProperty("date")
		public int getDate() {
			return date;
		}

		@JsonProperty("date")
		public void setDate(int date) {
			this.date = date;
		}

		@JsonProperty("feedname")
		public String getFeedname() {
			return feedname;
		}

		@JsonProperty("feedname")
		public void setFeedname(String feedname) {
			this.feedname = feedname;
		}

		@Override
		public String toString() {
			return "Newsitem{" +
					"gid='" + gid + '\'' +
					", title='" + title + '\'' +
					", url='" + url + '\'' +
					", isExternalUrl=" + isExternalUrl +
					", author='" + author + '\'' +
					", contents='" + contents + '\'' +
					", feedlabel='" + feedlabel + '\'' +
					", date=" + date +
					", feedname='" + feedname + '\'' +
					'}';
		}
	}
}
