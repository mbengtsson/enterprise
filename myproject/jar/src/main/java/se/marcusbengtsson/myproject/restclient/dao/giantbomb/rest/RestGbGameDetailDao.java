package se.marcusbengtsson.myproject.restclient.dao.giantbomb.rest;

import org.springframework.web.client.RestTemplate;
import se.marcusbengtsson.myproject.domain.GbGameDetail;
import se.marcusbengtsson.myproject.restclient.Constants;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbGameDetailDao;
import se.marcusbengtsson.myproject.restclient.model.giantbomb.GbGameDetailModel;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

/**
 * Giant-bomb game-detail data access object REST implementation
 *
 * @author Marcus Bengtsson
 */
@Stateless
public class RestGbGameDetailDao implements GbGameDetailDao {

	@Override
	public GbGameDetail getGameDetail(String gameDetailUrl) {

		RestTemplate restTemplate = new RestTemplate();

		GbGameDetailModel detailModel = restTemplate.getForObject(gameDetailUrl + "?api_key=" + Constants.GB_API_KEY +
				"&format=json&field_list=deck,reviews,image", GbGameDetailModel.class);

		List<String> reviewUrls = new ArrayList<String>();

		for (GbGameDetailModel.Review review : detailModel.getResults().getReviews()) {
			reviewUrls.add(review.getApiDetailUrl());
		}

		GbGameDetail gameDetail = new GbGameDetail();
		gameDetail.setSummary(detailModel.getResults().getDeck());
		if (detailModel.getResults().getImage() != null) {
			gameDetail.setImageUrl(detailModel.getResults().getImage().getSmallUrl());
		}
		gameDetail.setReviewUrls(reviewUrls);

		return gameDetail;
	}
}
