package se.marcusbengtsson.myproject.domain;

/**
 * Domain object containing a steam-news
 *
 * @author Marcus Bengtsson
 */
public class SteamNews {

	private String title;

	private String url;

	private String source;

	public SteamNews(String title) {
		this(title, "", "");
	}

	public SteamNews(String title, String url, String source) {
		this.title = title;
		this.url = url;
		this.source = source;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return "SteamNews{" +
				"title='" + title + '\'' +
				", url='" + url + '\'' +
				", source='" + source + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof SteamNews)) {
			return false;
		}

		SteamNews steamNews = (SteamNews) o;

		if (source != null ? !source.equals(steamNews.source) : steamNews.source != null) {
			return false;
		}
		if (title != null ? !title.equals(steamNews.title) : steamNews.title != null) {
			return false;
		}
		if (url != null ? !url.equals(steamNews.url) : steamNews.url != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = title != null ? title.hashCode() : 0;
		result = 31 * result + (url != null ? url.hashCode() : 0);
		result = 31 * result + (source != null ? source.hashCode() : 0);
		return result;
	}
}
