package se.marcusbengtsson.myproject.utils.logging;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.util.logging.Logger;

/**
 * Created by marcus on 2014-10-19.
 */
@ApplicationScoped
public class LoggingFactory {

	@Produces
	Logger createLogger(InjectionPoint injectionPoint) {

		return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());

	}


}

