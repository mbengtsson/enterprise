package se.marcusbengtsson.myproject.domain;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.util.List;

/**
 * Domain object containing a giantbomb-gamedetail
 *
 * @author Marcus Bengtsson
 */
public class GbGameDetail {

	private String summary;

	private String imageUrl;

	private List<String> reviewUrls;

	public GbGameDetail() {
	}

	public GbGameDetail(String summary, String imageUrl, List<String> reviewUrls) {

		setSummary(summary);

		setImageUrl(imageUrl);
		setReviewUrls(reviewUrls);
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		if (summary != null) {
			this.summary = Jsoup.clean(summary, Whitelist.basicWithImages());
		}
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<String> getReviewUrls() {
		return reviewUrls;
	}

	public void setReviewUrls(List<String> reviewUrls) {
		this.reviewUrls = reviewUrls;
	}

	@Override
	public String toString() {
		return "GbGameDetail{" +
				"summary='" + summary.substring(0, 10) + "..." + '\'' +
				", imageUrl='" + imageUrl + '\'' +
				", reviewUrls=" + reviewUrls +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof GbGameDetail)) {
			return false;
		}

		GbGameDetail that = (GbGameDetail) o;

		if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null) {
			return false;
		}
		if (reviewUrls != null ? !reviewUrls.equals(that.reviewUrls) : that.reviewUrls != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = imageUrl != null ? imageUrl.hashCode() : 0;
		result = 31 * result + (reviewUrls != null ? reviewUrls.hashCode() : 0);
		return result;
	}
}
