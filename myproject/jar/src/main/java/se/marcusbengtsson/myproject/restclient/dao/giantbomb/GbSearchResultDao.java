package se.marcusbengtsson.myproject.restclient.dao.giantbomb;

import se.marcusbengtsson.myproject.domain.GbSearchResult;

import javax.ejb.Local;
import java.util.List;

/**
 * Interface for a giant-bomb searchresult data access object
 *
 * @author Marcus Bengtsson
 */
@Local
public interface GbSearchResultDao {

	List<GbSearchResult> getGbSearchResultList(String searchString);
}
