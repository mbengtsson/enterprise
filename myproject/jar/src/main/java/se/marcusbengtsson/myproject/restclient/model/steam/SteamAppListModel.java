package se.marcusbengtsson.myproject.restclient.model.steam;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Object used to store a steam app-list response
 *
 * @author Marcus Bengtsson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SteamAppListModel {

	@JsonProperty("applist")
	private AppList appList;

	@JsonProperty("applist")
	public AppList getAppList() {
		return appList;
	}

	@JsonProperty("applist")
	public void setAppList(AppList appList) {
		this.appList = appList;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class AppList {

		@JsonProperty("apps")
		private Apps apps;

		@JsonProperty("apps")
		public Apps getApps() {
			return apps;
		}

		@JsonProperty("apps")
		public void setApps(Apps apps) {
			this.apps = apps;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Apps {

		@JsonProperty("app")
		private List<App> app = new ArrayList<App>();

		@JsonProperty("app")
		public List<App> getApp() {
			return app;
		}

		@JsonProperty("app")
		public void setApp(List<App> app) {
			this.app = app;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class App {
		@JsonProperty("appid")
		private int appid;

		@JsonProperty("name")
		private String name;

		@JsonProperty("appid")
		public int getAppid() {
			return appid;
		}

		@JsonProperty("appid")
		public void setAppid(int appid) {
			this.appid = appid;
		}

		@JsonProperty("name")
		public String getName() {
			return name;
		}

		@JsonProperty("name")
		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return "App{" +
					"appid=" + appid +
					", name='" + name + '\'' +
					'}';
		}
	}
}
