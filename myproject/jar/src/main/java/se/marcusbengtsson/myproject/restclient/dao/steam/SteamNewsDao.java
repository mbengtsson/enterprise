package se.marcusbengtsson.myproject.restclient.dao.steam;

import se.marcusbengtsson.myproject.domain.SteamNews;

import javax.ejb.Local;
import java.util.List;

/**
 * Interface for a steam news data access object
 *
 * @author Marcus Bengtsson
 */
@Local
public interface SteamNewsDao {

	List<SteamNews> getSteamNewsList(int appId);
}
