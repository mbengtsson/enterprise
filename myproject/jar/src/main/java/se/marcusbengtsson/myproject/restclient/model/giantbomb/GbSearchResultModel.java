package se.marcusbengtsson.myproject.restclient.model.giantbomb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Object used to store a giant-bomb search-result response
 *
 * @author Marcus Bengtsson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GbSearchResultModel {

	@JsonProperty("error")
	private String error;

	@JsonProperty("limit")
	private int limit;

	@JsonProperty("offset")
	private int offset;

	@JsonProperty("number_of_page_results")
	private int numberOfPageResults;

	@JsonProperty("number_of_total_results")
	private int numberOfTotalResults;

	@JsonProperty("status_code")
	private int statusCode;

	@JsonProperty("results")
	private List<Result> results = new ArrayList<Result>();

	@JsonProperty("version")
	private String version;

	@JsonProperty("error")
	public String getError() {
		return error;
	}

	@JsonProperty("error")
	public void setError(String error) {
		this.error = error;
	}

	@JsonProperty("limit")
	public int getLimit() {
		return limit;
	}

	@JsonProperty("limit")
	public void setLimit(int limit) {
		this.limit = limit;
	}

	@JsonProperty("offset")
	public int getOffset() {
		return offset;
	}

	@JsonProperty("offset")
	public void setOffset(int offset) {
		this.offset = offset;
	}

	@JsonProperty("number_of_page_results")
	public int getNumberOfPageResults() {
		return numberOfPageResults;
	}

	@JsonProperty("number_of_page_results")
	public void setNumberOfPageResults(int numberOfPageResults) {
		this.numberOfPageResults = numberOfPageResults;
	}

	@JsonProperty("number_of_total_results")
	public int getNumberOfTotalResults() {
		return numberOfTotalResults;
	}

	@JsonProperty("number_of_total_results")
	public void setNumberOfTotalResults(int numberOfTotalResults) {
		this.numberOfTotalResults = numberOfTotalResults;
	}

	@JsonProperty("status_code")
	public int getStatusCode() {
		return statusCode;
	}

	@JsonProperty("status_code")
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	@JsonProperty("results")
	public List<Result> getResults() {
		return results;
	}

	@JsonProperty("results")
	public void setResults(List<Result> results) {
		this.results = results;
	}

	@JsonProperty("version")
	public String getVersion() {
		return version;
	}

	@JsonProperty("version")
	public void setVersion(String version) {
		this.version = version;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Result {

		@JsonProperty("api_detail_url")
		private String apiDetailUrl;

		@JsonProperty("resource_type")
		private String resourceType;

		@JsonProperty("api_detail_url")
		public String getApiDetailUrl() {
			return apiDetailUrl;
		}

		@JsonProperty("api_detail_url")
		public void setApiDetailUrl(String apiDetailUrl) {
			this.apiDetailUrl = apiDetailUrl;
		}

		@JsonProperty("resource_type")
		public String getResourceType() {
			return resourceType;
		}

		@JsonProperty("resource_type")
		public void setResourceType(String resourceType) {
			this.resourceType = resourceType;
		}

	}

}



