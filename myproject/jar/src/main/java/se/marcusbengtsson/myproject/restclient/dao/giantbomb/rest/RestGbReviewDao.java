package se.marcusbengtsson.myproject.restclient.dao.giantbomb.rest;

import org.springframework.web.client.RestTemplate;
import se.marcusbengtsson.myproject.domain.GbReview;
import se.marcusbengtsson.myproject.restclient.Constants;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbReviewDao;
import se.marcusbengtsson.myproject.restclient.model.giantbomb.GbReviewModel;

import javax.ejb.Stateless;

/**
 * Giant-bomb review data access object REST implementation
 *
 * @author Marcus Bengtsson
 */
@Stateless
public class RestGbReviewDao implements GbReviewDao {

	@Override
	public GbReview getGameReview(String reviewUrl) {

		RestTemplate restTemplate = new RestTemplate();

		GbReviewModel reviewModel = restTemplate.getForObject(reviewUrl + "?api_key=" + Constants.GB_API_KEY +
				"&format=json&field_list=deck,description,reviewer,score,site_detail_url", GbReviewModel.class);

		return new GbReview(reviewModel.getResults().getDeck(), reviewModel.getResults().getDescription(),
				reviewModel.getResults().getReviewer(), reviewModel.getResults().getScore(), reviewModel.getResults().getSiteDetailUrl());

	}
}
