package se.marcusbengtsson.myproject.domain;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 * Domain object containing a giantbomb-review
 *
 * @author Marcus Bengtsson
 */
public class GbReview {

	private String summary;

	private String review;

	private String reviewer;

	private int score;

	private String siteUrl;

	public GbReview() {
		this(null);
	}

	public GbReview(String summary) {
		this(summary, null, null, -1, null);
	}

	public GbReview(String summary, String review, String reviewer, int score, String siteUrl) {
		setSummary(summary);
		setReview(review);
		setReviewer(reviewer);
		setScore(score);
		setSiteUrl(siteUrl);
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		if (summary != null) {
			this.summary = Jsoup.clean(summary, Whitelist.basicWithImages());
		}
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		if (review != null) {
			this.review = Jsoup.clean(review, Whitelist.basicWithImages());
		}
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getSiteUrl() {
		return siteUrl;
	}

	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	@Override
	public String toString() {
		return "GbReview{" +
				"summary='" + summary.substring(0, 10) + "..." + '\'' +
				", review='" + review.substring(0, 10) + "..." + '\'' +
				", reviewer='" + reviewer + '\'' +
				", score=" + score +
				", siteUrl='" + siteUrl + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof GbReview)) {
			return false;
		}

		GbReview gbReview = (GbReview) o;

		if (score != gbReview.score) {
			return false;
		}
		if (reviewer != null ? !reviewer.equals(gbReview.reviewer) : gbReview.reviewer != null) {
			return false;
		}
		if (siteUrl != null ? !siteUrl.equals(gbReview.siteUrl) : gbReview.siteUrl != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = reviewer != null ? reviewer.hashCode() : 0;
		result = 31 * result + score;
		result = 31 * result + (siteUrl != null ? siteUrl.hashCode() : 0);
		return result;
	}
}
