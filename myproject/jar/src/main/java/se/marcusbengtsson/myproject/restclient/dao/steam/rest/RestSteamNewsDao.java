package se.marcusbengtsson.myproject.restclient.dao.steam.rest;

import org.springframework.web.client.RestTemplate;
import se.marcusbengtsson.myproject.domain.SteamNews;
import se.marcusbengtsson.myproject.restclient.dao.steam.SteamNewsDao;
import se.marcusbengtsson.myproject.restclient.model.steam.SteamNewsModel;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

/**
 * Steam news data access object REST implementation
 *
 * @author Marcus Bengtsson
 */
@Stateless
public class RestSteamNewsDao implements SteamNewsDao {

	@Override
	public List<SteamNews> getSteamNewsList(int appId) {

		RestTemplate restTemplate = new RestTemplate();

		SteamNewsModel steamNewsModel = restTemplate.getForObject("http://api.steampowered" +
				".com/ISteamNews/GetNewsForApp/v0001/?appid=" + appId, SteamNewsModel.class);

		List<SteamNews> steamNewsList = new ArrayList<SteamNews>();

		if (steamNewsModel.getAppnews().getNewsitems().getNewsitem().get(0) != null && !steamNewsModel.getAppnews()
				.getNewsitems().getNewsitem().isEmpty()) {
			for (SteamNewsModel.Newsitem newsItem : steamNewsModel.getAppnews().getNewsitems().getNewsitem()) {

				steamNewsList.add(new SteamNews(newsItem.getTitle(), newsItem.getUrl(), newsItem.getFeedlabel()));
			}
		}

		return steamNewsList;
	}
}
