package se.marcusbengtsson.myproject.restclient.model.giantbomb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Object used to store a giant-bomb game-detail response
 *
 * @author Marcus Bengtsson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GbGameDetailModel {

	@JsonProperty("error")
	private String error;
	@JsonProperty("limit")
	private int limit;
	@JsonProperty("offset")
	private int offset;
	@JsonProperty("number_of_page_results")
	private int numberOfPageResults;
	@JsonProperty("number_of_total_results")
	private int numberOfTotalResults;
	@JsonProperty("status_code")
	private int statusCode;
	@JsonProperty("results")
	private Results results;
	@JsonProperty("version")
	private String version;

	@JsonProperty("error")
	public String getError() {
		return error;
	}

	@JsonProperty("error")
	public void setError(String error) {
		this.error = error;
	}

	@JsonProperty("limit")
	public int getLimit() {
		return limit;
	}

	@JsonProperty("limit")
	public void setLimit(int limit) {
		this.limit = limit;
	}

	@JsonProperty("offset")
	public int getOffset() {
		return offset;
	}

	@JsonProperty("offset")
	public void setOffset(int offset) {
		this.offset = offset;
	}

	@JsonProperty("number_of_page_results")
	public int getNumberOfPageResults() {
		return numberOfPageResults;
	}

	@JsonProperty("number_of_page_results")
	public void setNumberOfPageResults(int numberOfPageResults) {
		this.numberOfPageResults = numberOfPageResults;
	}

	@JsonProperty("number_of_total_results")
	public int getNumberOfTotalResults() {
		return numberOfTotalResults;
	}

	@JsonProperty("number_of_total_results")
	public void setNumberOfTotalResults(int numberOfTotalResults) {
		this.numberOfTotalResults = numberOfTotalResults;
	}

	@JsonProperty("status_code")
	public int getStatusCode() {
		return statusCode;
	}

	@JsonProperty("status_code")
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	@JsonProperty("results")
	public Results getResults() {
		return results;
	}

	@JsonProperty("results")
	public void setResults(Results results) {
		this.results = results;
	}

	@JsonProperty("version")
	public String getVersion() {
		return version;
	}

	@JsonProperty("version")
	public void setVersion(String version) {
		this.version = version;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Results {

		@JsonProperty("deck")
		private String deck;
		@JsonProperty("image")
		private Image image;
		@JsonProperty("reviews")
		private List<Review> reviews = new ArrayList<Review>();

		@JsonProperty("deck")
		public String getDeck() {
			return deck;
		}

		@JsonProperty("deck")
		public void setDeck(String deck) {
			this.deck = deck;
		}

		@JsonProperty("image")
		public Image getImage() {
			return image;
		}

		@JsonProperty("image")
		public void setImage(Image image) {
			this.image = image;
		}

		@JsonProperty("reviews")
		public List<Review> getReviews() {
			return reviews;
		}

		@JsonProperty("reviews")
		public void setReviews(List<Review> reviews) {
			this.reviews = reviews;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Image {

		@JsonProperty("icon_url")
		private String iconUrl;
		@JsonProperty("medium_url")
		private String mediumUrl;
		@JsonProperty("screen_url")
		private String screenUrl;
		@JsonProperty("small_url")
		private String smallUrl;
		@JsonProperty("super_url")
		private String superUrl;
		@JsonProperty("thumb_url")
		private String thumbUrl;
		@JsonProperty("tiny_url")
		private String tinyUrl;

		@JsonProperty("icon_url")
		public String getIconUrl() {
			return iconUrl;
		}

		@JsonProperty("icon_url")
		public void setIconUrl(String iconUrl) {
			this.iconUrl = iconUrl;
		}

		@JsonProperty("medium_url")
		public String getMediumUrl() {
			return mediumUrl;
		}

		@JsonProperty("medium_url")
		public void setMediumUrl(String mediumUrl) {
			this.mediumUrl = mediumUrl;
		}

		@JsonProperty("screen_url")
		public String getScreenUrl() {
			return screenUrl;
		}

		@JsonProperty("screen_url")
		public void setScreenUrl(String screenUrl) {
			this.screenUrl = screenUrl;
		}

		@JsonProperty("small_url")
		public String getSmallUrl() {
			return smallUrl;
		}

		@JsonProperty("small_url")
		public void setSmallUrl(String smallUrl) {
			this.smallUrl = smallUrl;
		}

		@JsonProperty("super_url")
		public String getSuperUrl() {
			return superUrl;
		}

		@JsonProperty("super_url")
		public void setSuperUrl(String superUrl) {
			this.superUrl = superUrl;
		}

		@JsonProperty("thumb_url")
		public String getThumbUrl() {
			return thumbUrl;
		}

		@JsonProperty("thumb_url")
		public void setThumbUrl(String thumbUrl) {
			this.thumbUrl = thumbUrl;
		}

		@JsonProperty("tiny_url")
		public String getTinyUrl() {
			return tinyUrl;
		}

		@JsonProperty("tiny_url")
		public void setTinyUrl(String tinyUrl) {
			this.tinyUrl = tinyUrl;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Review {

		@JsonProperty("api_detail_url")
		private String apiDetailUrl;
		@JsonProperty("id")
		private int id;
		@JsonProperty("name")
		private String name;
		@JsonProperty("site_detail_url")
		private String siteDetailUrl;

		@JsonProperty("api_detail_url")
		public String getApiDetailUrl() {
			return apiDetailUrl;
		}

		@JsonProperty("api_detail_url")
		public void setApiDetailUrl(String apiDetailUrl) {
			this.apiDetailUrl = apiDetailUrl;
		}

		@JsonProperty("id")
		public int getId() {
			return id;
		}

		@JsonProperty("id")
		public void setId(int id) {
			this.id = id;
		}

		@JsonProperty("name")
		public String getName() {
			return name;
		}

		@JsonProperty("name")
		public void setName(String name) {
			this.name = name;
		}

		@JsonProperty("site_detail_url")
		public String getSiteDetailUrl() {
			return siteDetailUrl;
		}

		@JsonProperty("site_detail_url")
		public void setSiteDetailUrl(String siteDetailUrl) {
			this.siteDetailUrl = siteDetailUrl;
		}

	}

}
