package se.marcusbengtsson.myproject.services.impl;

import se.marcusbengtsson.myproject.domain.GbGameDetail;
import se.marcusbengtsson.myproject.domain.GbReview;
import se.marcusbengtsson.myproject.domain.GbSearchResult;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbGameDetailDao;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbReviewDao;
import se.marcusbengtsson.myproject.restclient.dao.giantbomb.GbSearchResultDao;
import se.marcusbengtsson.myproject.services.GiantBombService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Giant-bomb service implementation
 *
 * @author Marcus Bengtsson
 */
@Stateless
public class GiantBombServiceImpl implements GiantBombService {

	@Inject
	private GbSearchResultDao gbSearchResultDao;

	@Inject
	private GbGameDetailDao gbGameDetailDao;

	@Inject
	private GbReviewDao gbReviewDao;

	@Override
	public List<GbSearchResult> getGbSearchResultList(String searchString) {
		return gbSearchResultDao.getGbSearchResultList(searchString);
	}

	@Override
	public GbGameDetail getGameDetail(String gameDetailUrl) {
		return gbGameDetailDao.getGameDetail(gameDetailUrl);
	}

	@Override
	public GbReview getGameReview(String reviewUrl) {
		return gbReviewDao.getGameReview(reviewUrl);
	}

	protected void setGbSearchResultDao(GbSearchResultDao gbSearchResultDao) {
		this.gbSearchResultDao = gbSearchResultDao;
	}

	protected void setGbGameDetailDao(GbGameDetailDao gbGameDetailDao) {
		this.gbGameDetailDao = gbGameDetailDao;
	}

	protected void setGbReviewDao(GbReviewDao gbReviewDao) {
		this.gbReviewDao = gbReviewDao;
	}
}
