package se.marcusbengtsson.myproject.restclient.dao.steam;

import se.marcusbengtsson.myproject.domain.SteamApp;

import javax.ejb.*;
import java.util.Map;

/**
 * Interface for a steam app-list data access object
 *
 * @author Marcus Bengtsson
 */
@Local
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public interface SteamAppListDao {

	@Lock(LockType.WRITE)
	void updateAppList();

	@Lock(LockType.READ)
	Map<String, SteamApp> getAllSteamAppsMap();
}
