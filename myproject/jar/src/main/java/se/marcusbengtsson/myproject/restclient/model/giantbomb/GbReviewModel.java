package se.marcusbengtsson.myproject.restclient.model.giantbomb;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marcus on 2014-10-18.
 */

/**
 * Object used to store a giant-bomb review response
 *
 * @author Marcus Bengtsson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GbReviewModel {

	@JsonProperty("error")
	private String error;
	@JsonProperty("limit")
	private int limit;
	@JsonProperty("offset")
	private int offset;
	@JsonProperty("number_of_page_results")
	private int numberOfPageResults;
	@JsonProperty("number_of_total_results")
	private int numberOfTotalResults;
	@JsonProperty("status_code")
	private int statusCode;
	@JsonProperty("results")
	private Results results;
	@JsonProperty("version")
	private String version;

	@JsonProperty("error")
	public String getError() {
		return error;
	}

	@JsonProperty("error")
	public void setError(String error) {
		this.error = error;
	}

	@JsonProperty("limit")
	public int getLimit() {
		return limit;
	}

	@JsonProperty("limit")
	public void setLimit(int limit) {
		this.limit = limit;
	}

	@JsonProperty("offset")
	public int getOffset() {
		return offset;
	}

	@JsonProperty("offset")
	public void setOffset(int offset) {
		this.offset = offset;
	}

	@JsonProperty("number_of_page_results")
	public int getNumberOfPageResults() {
		return numberOfPageResults;
	}

	@JsonProperty("number_of_page_results")
	public void setNumberOfPageResults(int numberOfPageResults) {
		this.numberOfPageResults = numberOfPageResults;
	}

	@JsonProperty("number_of_total_results")
	public int getNumberOfTotalResults() {
		return numberOfTotalResults;
	}

	@JsonProperty("number_of_total_results")
	public void setNumberOfTotalResults(int numberOfTotalResults) {
		this.numberOfTotalResults = numberOfTotalResults;
	}

	@JsonProperty("status_code")
	public int getStatusCode() {
		return statusCode;
	}

	@JsonProperty("status_code")
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	@JsonProperty("results")
	public Results getResults() {
		return results;
	}

	@JsonProperty("results")
	public void setResults(Results results) {
		this.results = results;
	}

	@JsonProperty("version")
	public String getVersion() {
		return version;
	}

	@JsonProperty("version")
	public void setVersion(String version) {
		this.version = version;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Results {

		@JsonProperty("deck")
		private String deck;
		@JsonProperty("description")
		private String description;
		@JsonProperty("reviewer")
		private String reviewer;
		@JsonProperty("score")
		private int score;
		@JsonProperty("site_detail_url")
		private String siteDetailUrl;

		@JsonProperty("deck")
		public String getDeck() {
			return deck;
		}

		@JsonProperty("deck")
		public void setDeck(String deck) {
			this.deck = deck;
		}

		@JsonProperty("description")
		public String getDescription() {
			return description;
		}

		@JsonProperty("description")
		public void setDescription(String description) {
			this.description = description;
		}

		@JsonProperty("reviewer")
		public String getReviewer() {
			return reviewer;
		}

		@JsonProperty("reviewer")
		public void setReviewer(String reviewer) {
			this.reviewer = reviewer;
		}

		@JsonProperty("score")
		public int getScore() {
			return score;
		}

		@JsonProperty("score")
		public void setScore(int score) {
			this.score = score;
		}

		@JsonProperty("site_detail_url")
		public String getSiteDetailUrl() {
			return siteDetailUrl;
		}

		@JsonProperty("site_detail_url")
		public void setSiteDetailUrl(String siteDetailUrl) {
			this.siteDetailUrl = siteDetailUrl;
		}

	}
}
