package se.marcusbengtsson.myproject.restclient.dao.steam.rest;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.web.client.RestTemplate;
import se.marcusbengtsson.myproject.domain.SteamApp;
import se.marcusbengtsson.myproject.restclient.dao.steam.SteamAppListDao;
import se.marcusbengtsson.myproject.restclient.model.steam.SteamAppListModel;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Steam app-list data access object
 *
 * @author Marcus Bengtsson
 */
@Singleton
@Startup
public class RestSteamAppListDao implements SteamAppListDao {

	@Inject
	private Logger logger;

	private HashMap<String, SteamApp> steamAppMap;

	private DateTime lastUpdated = new DateTime(0);

	@PostConstruct
	protected void init() {

		updateAppList();
	}

	@Override
	public void updateAppList() {

		if (Days.daysBetween(lastUpdated.toLocalDate(), DateTime.now().toLocalDate()).getDays() >= 1) {

			logger.fine("More than 1 day since last update, updating");

			RestTemplate restTemplate = new RestTemplate();
			SteamAppListModel steamApps = restTemplate.getForObject("http://api.steampowered" +
							".com/ISteamApps/GetAppList/v0001/",
					SteamAppListModel.class);

			steamAppMap = new HashMap<String, SteamApp>();

			for (SteamAppListModel.App app : steamApps.getAppList().getApps().getApp()) {

				if (!steamAppMap.containsKey(app.getName()) && appFilter(app.getName())) {
					steamAppMap.put(app.getName().trim(), new SteamApp(app.getAppid(), app.getName().trim()));
				}
			}

			lastUpdated = DateTime.now();
		} else {
			logger.fine("Less than 1 day since last update, holding back");
		}
	}

	@Override
	public Map<String, SteamApp> getAllSteamAppsMap() {
		return steamAppMap;
	}

	private boolean appFilter(String appName) {

		List<String> filterList = new ArrayList<String>();
		filterList.add("ValveTestApp");
		filterList.add(" Demo");
		filterList.add(" Trailer");
		filterList.add(" trailer");
		filterList.add(" Pack");
		filterList.add(" Teaser");
		filterList.add(" Soundtrack");
		filterList.add(" DLC");
		filterList.add(" SDK");
		filterList.add(" Server");
		filterList.add(" Video");
		filterList.add(" video");
		filterList.add(" Artbook");
		filterList.add(" Add-On");
		filterList.add(" Guide");
		filterList.add(" Bundle");
		filterList.add(" Key");

		for (String filterItem : filterList) {
			if (appName.contains(filterItem)) {
				return false;
			}
		}

		return true;
	}
}
