package se.plushogskolan.jetbroker.plane.integration.airportws;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import se.plushogskolan.jee.utils.integration.FailedIntegrationConnectionException;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.integration.airportws.stubs.AirportSoap;

import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Logger;

/**
 * Created by Marcus Bengtsson on 2014-10-24.
 *
 * @author Marcus Bengtsson
 */
public class AirportWebserviceImpl implements AirportWebservice {

	@Inject
	Logger log;

	@Override
	public Collection<Airport> getAllAirports() throws FailedIntegrationConnectionException {

		log.fine("Fetching airports from http://www.webservicex.net");

		se.plushogskolan.jetbroker.plane.integration.airportws.stubs.Airport service = new se.plushogskolan.jetbroker
				.plane.integration.airportws.stubs.Airport();
		AirportSoap port = service.getAirportSoap();
		String airportXml = port.getAirportInformationByCountry("Sweden");

		SAXParserFactory factory = SAXParserFactory.newInstance();

		try {
			SAXParser parser = factory.newSAXParser();

			InputSource in = new InputSource(new StringReader(airportXml));
			AirportHandler handler = new AirportHandler();

			parser.parse(in, handler);

			return handler.getParsedAirports();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	static class AirportHandler extends DefaultHandler {

		private Collection<Airport> airports;
		private Airport currentAirport;
		private String currentTextValue;

		public Collection<Airport> getParsedAirports() {
			return airports;
		}

		@Override
		public void startDocument() throws SAXException {

			airports = new HashSet<Airport>();
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

			if (qName.equals("Table")) {
				currentAirport = new Airport();
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			currentTextValue = new String(ch, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {

			switch (qName) {
				case "AirportCode":
					currentAirport.setCode(currentTextValue);
					break;
				case "CityOrAirportName":
					currentAirport.setName(currentTextValue);
					break;
				case "LatitudeDegree":
					currentAirport.setLatitude(Double.parseDouble(currentTextValue));
					break;
				case "LatitudeMinute":
					currentAirport.setLatitude(currentAirport.getLatitude() + (Double.parseDouble(currentTextValue)
							/ 60));
					break;
				case "LatitudeSecond":
					currentAirport.setLatitude(currentAirport.getLatitude() + (Double.parseDouble(currentTextValue)
							/ 3600));
					break;
				case "LatitudeNpeerS":
					if (currentTextValue.equals("S")) {
						currentAirport.setLatitude(currentAirport.getLatitude() * -1);
					}
					break;
				case "LongitudeDegree":
					currentAirport.setLongitude(Double.parseDouble(currentTextValue));
					break;
				case "LongitudeMinute":
					currentAirport.setLongitude(currentAirport.getLongitude() + (Double.parseDouble(currentTextValue)
							/ 60));
					break;
				case "LongitudeSeconds":
					currentAirport.setLongitude(currentAirport.getLongitude() + (Double.parseDouble(currentTextValue)
							/ 3600));
					break;
				case "LongitudeEperW":
					if (currentTextValue.equals("W")) {
						currentAirport.setLongitude(currentAirport.getLongitude() * -1);
					}
					break;
				case "Table":
					airports.add(currentAirport);
					break;

			}
		}

	}
}
