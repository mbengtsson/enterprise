package se.plushogskolan.jetbroker.plane.service;

import org.joda.time.DateTime;
import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.integration.airportws.AirportWebservice;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.AirportRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Stateless
public class AirportServiceImpl implements AirportService {

	@Inject
	Logger log;

	@Inject
	private AirportRepository airportRepo;

	@Inject
	private AirportWebservice airportWebservice;

	@Inject
	private PlaneIntegrationFacade planeIntegrationFacade;

	@Override
	public Airport getAirport(long id) {
		return getAirportRepo().findById(id);
	}

	@Override
	public Airport getAirportByCode(String code) {
		return getAirportRepo().getAirportByCode(code);
	}

	@Override
	public void updateAirport(Airport airport) {
		planeIntegrationFacade.broadcastAirportsChanged();
		getAirportRepo().update(airport);
	}

	@Override
	public Airport createAirport(Airport airport) {
		long id = getAirportRepo().persist(airport);
		planeIntegrationFacade.broadcastAirportsChanged();
		return getAirport(id);
	}

	@Override
	public List<Airport> getAllAirports() {
		List<Airport> airports = getAirportRepo().getAllAirports();
		Collections.sort(airports);
		return airports;
	}

	@Override
	public void deleteAirport(long id) {
		Airport airport = getAirport(id);
		planeIntegrationFacade.broadcastAirportsChanged();
		getAirportRepo().remove(airport);
	}

	@Override
	public void updateAirportsFromWebService() {

		log.fine("Updating airports from webservice");

		Map<String, Airport> fetchedAiportMap = new HashMap<String, Airport>();
		for (Airport airport : airportWebservice.getAllAirports()) {
			fetchedAiportMap.put(airport.getCode(), airport);
		}

		Map<String, Airport> localAirportMap = new HashMap<String, Airport>();
		for (Airport airport : getAirportRepo().getAllAirports()) {

			if (fetchedAiportMap.containsKey(airport.getCode())) {
				localAirportMap.put(airport.getCode(), airport);
			} else {
				log.fine("Removing obsolete airport: " + airport);
				getAirportRepo().remove(airport);
			}
		}

		for (Airport airport : fetchedAiportMap.values()) {
			if (localAirportMap.containsKey(airport.getCode())) {
				Airport localAirport = localAirportMap.get(airport.getCode());
				if (!localAirport.equals(airport)) {
					airport.setId(localAirport.getId());
					airport.setLastUpdated(DateTime.now());
					log.fine("updating airport: " + airport);
					getAirportRepo().update(airport);
				}
			} else {
				log.fine("creating airport: " + airport);
				airport.setLastUpdated(DateTime.now());
				getAirportRepo().persist(airport);
			}
		}

		planeIntegrationFacade.broadcastAirportsChanged();

	}

	public AirportRepository getAirportRepo() {
		return airportRepo;
	}

	public void setAirportRepo(AirportRepository airportRepo) {
		this.airportRepo = airportRepo;
	}

}
