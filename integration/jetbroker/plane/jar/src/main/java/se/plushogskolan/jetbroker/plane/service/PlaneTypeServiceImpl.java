package se.plushogskolan.jetbroker.plane.service;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.plane.repository.PlaneTypeRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class PlaneTypeServiceImpl implements PlaneTypeService {

	@Inject
	private PlaneTypeRepository planeRepository;

	@Inject
	private PlaneIntegrationFacade planeIntegrationFacade;

	@Override
	public PlaneType getPlaneType(long id) {
		return getPlaneRepository().findById(id);
	}

	@Override
	public PlaneType createPlaneType(PlaneType planeType) {
		long id = getPlaneRepository().persist(planeType);
		planeIntegrationFacade.broadcastPlaneTypesChanged();
		return getPlaneType(id);
	}

	@Override
	public void updatePlaneType(PlaneType planeType) {
		getPlaneRepository().update(planeType);
		planeIntegrationFacade.broadcastPlaneTypesChanged();
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {
		return getPlaneRepository().getAllPlaneTypes();
	}

	@Override
	public void deletePlaneType(long id) {
		PlaneType planeType = getPlaneType(id);
		planeIntegrationFacade.broadcastPlaneTypesChanged();
		getPlaneRepository().remove(planeType);
	}

	public PlaneTypeRepository getPlaneRepository() {
		return planeRepository;
	}

	public void setPlaneRepository(PlaneTypeRepository planeRepository) {
		this.planeRepository = planeRepository;
	}

}
