package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.PlaneTypeService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by marcus on 2014-10-22.
 */
@Stateless
@WebService(name = "PlaneWebService", endpointInterface = "se.plushogskolan.jetbroker.plane.integration.jetbroker.ws.PlaneWebService")
public class PlaneWebServiceImpl implements PlaneWebService {

	@Inject
	Logger log;

	@Inject
	private AirportService airportService;

	@Inject
	private PlaneTypeService planeTypeService;

	@Override
	@WebMethod
	public List<WSAirport> getAirports() {

		log.fine("PROD: Get airports webservice called");

		List<WSAirport> wsAirports = new ArrayList<WSAirport>();

		for (Airport airport : airportService.getAllAirports()) {
			wsAirports.add(new WSAirport(airport.getCode(), airport.getName(), airport.getLatitude(),
					airport.getLongitude()));
		}

		return wsAirports;
	}

	@Override
	@WebMethod
	public List<WSPlaneType> getPlaneTypes() {

		log.fine("PROD: Get planetypes webservice called");

		List<WSPlaneType> wsPlaneTypes = new ArrayList<WSPlaneType>();

		for (PlaneType planeType : planeTypeService.getAllPlaneTypes()) {
			wsPlaneTypes.add(new WSPlaneType(planeType.getCode(), planeType.getName(), planeType.getMaxNoOfPassengers()
					, planeType.getMaxRangeKm(), planeType.getMaxSpeedKmH(), planeType.getFuelConsumptionPerKm()));
		}

		return wsPlaneTypes;
	}
}
