package se.plushogskolan.jetbroker.plane.integration.jetbroker.jms;

import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.*;
import java.util.logging.Logger;

public class PlaneIntegrationFacadeJms implements PlaneIntegrationFacade {

	@Inject
	Logger log;

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private TopicConnectionFactory connectionFactory;

	@Resource(mappedName = JmsConstants.TOPIC_PLANE_BROADCAST)
	private Topic topic;

	@Override
	public void broadcastNewFuelPrice(double fuelPrice) {

		log.fine("PROD: broadcastNewFuelPrice. New price is " + fuelPrice);

		TopicConnection connection = null;
		TopicSession session = null;
		try {

			connection = connectionFactory.createTopicConnection();
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

			connection.start();

			Message message = session.createMessage();
			message.setDoubleProperty("fuelPrice", fuelPrice);
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED);

			TopicPublisher publisher = session.createPublisher(topic);
			publisher.publish(message);

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}

	}

	@Override
	public void broadcastAirportsChanged() {

		log.fine("PROD: broadcast airport changed");

		broadcastChanges(JmsConstants.MSGTYPE_PLANEBROADCAST_AIRPORTSCHANGED);
	}

	@Override
	public void broadcastPlaneTypesChanged() {

		log.fine("PROD: broadcast planetype changed");

		broadcastChanges(JmsConstants.MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED);

	}

	private void broadcastChanges(String messageType) {

		TopicConnection connection = null;
		TopicSession session = null;
		try {

			connection = connectionFactory.createTopicConnection();
			session = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);

			connection.start();

			Message message = session.createMessage();

			message.setStringProperty("messageType", messageType);

			TopicPublisher publisher = session.createPublisher(topic);
			publisher.publish(message);

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}

	}

}
