package se.plushogskolan.jetbroker.plane.integration.jetbroker.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface PlaneWebService {

	@WebMethod
	public List<WSAirport> getAirports();

	@WebMethod
	public List<WSPlaneType> getPlaneTypes();

}
