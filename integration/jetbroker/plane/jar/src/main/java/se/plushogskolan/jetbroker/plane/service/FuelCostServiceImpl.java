package se.plushogskolan.jetbroker.plane.service;

import se.plushogskolan.jetbroker.plane.integration.jetbroker.PlaneIntegrationFacade;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.logging.Logger;

/**
 * Implementation of fuel cost service
 *
 * @author marcus
 */
@Singleton
@Startup
public class FuelCostServiceImpl implements FuelCostService {

	@Inject
	Logger log;

	@Inject
	private PlaneIntegrationFacade planeIntegrationFacade;

	private double fuelCost;

	@PostConstruct
	protected void init() {
		updateFuelCost(5.2);
	}

	@Override
	public double getFuelCost() {
		return fuelCost;
	}

	@Override
	public void updateFuelCost(double fuelCost) {

		log.fine("Updating fuelcost " + fuelCost);

		this.fuelCost = fuelCost;
		planeIntegrationFacade.broadcastNewFuelPrice(getFuelCost());
	}

}
