package se.plushogskolan.jetbroker.plane.rest.airport.model;

import se.plushogskolan.jetbroker.plane.domain.Airport;

public class CreateAirportRequest {
	
	private String code;
	private String name;
	private double latitude;
	private double longitude;

	public Airport buildAirport() {
		Airport airport = new Airport();
		airport.setCode(getCode());
		airport.setName(getName());
		airport.setLatitude(getLatitude());
		airport.setLongitude(getLongitude());
		return airport;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "CreateAirportRequest [code=" + code + ", name=" + name + ", latitude=" + latitude + ", longitude="
				+ longitude + "]";
	}

}
