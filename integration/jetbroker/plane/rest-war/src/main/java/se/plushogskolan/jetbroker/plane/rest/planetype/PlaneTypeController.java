package se.plushogskolan.jetbroker.plane.rest.planetype;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;
import se.plushogskolan.jetbroker.plane.rest.planetype.model.CreatePlaneTypeRequest;
import se.plushogskolan.jetbroker.plane.service.PlaneTypeService;

@Controller
public class PlaneTypeController {

	Logger log = Logger.getLogger(PlaneTypeController.class.getName());

	@Inject
	private PlaneTypeService planeTypeService;

	@RequestMapping(value = "/createPlaneType", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<PlaneType> createPlaneType(@RequestBody CreatePlaneTypeRequest request) throws Exception {
		log.fine("createPlaneType: " + request);

		PlaneType planeType = null;
		try {
			planeType = planeTypeService.createPlaneType(request.buildPlaneType());
			return new ResponseEntity<PlaneType>(planeType, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<PlaneType>(planeType, HttpStatus.BAD_REQUEST);

		}
	}

	@RequestMapping(value = "/getPlaneType/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<PlaneType> getPlaneType(@PathVariable long id) {

		PlaneType planeType = null;
		try {
			planeType = planeTypeService.getPlaneType(id);
			return new ResponseEntity<PlaneType>(planeType, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<PlaneType>(planeType, HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/deletePlaneType/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> deletePlaneType(@PathVariable long id) {

		try {
			planeTypeService.deletePlaneType(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}
}
