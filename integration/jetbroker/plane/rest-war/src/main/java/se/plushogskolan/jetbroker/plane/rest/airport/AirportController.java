package se.plushogskolan.jetbroker.plane.rest.airport;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.plane.domain.Airport;
import se.plushogskolan.jetbroker.plane.rest.airport.model.CreateAirportRequest;
import se.plushogskolan.jetbroker.plane.service.AirportService;
import se.plushogskolan.jetbroker.plane.service.FuelCostService;

@Controller
public class AirportController {

	Logger log = Logger.getLogger(AirportController.class.getName());

	@Inject
	private AirportService airportService;

	@Inject
	private FuelCostService fuelCostService;

	@RequestMapping(value = "/createAirport", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Airport> createAirport(@RequestBody CreateAirportRequest request) throws Exception {
		log.fine("createAirport: " + request);

		Airport airport = null;
		try {
			airport = airportService.createAirport(request.buildAirport());
			return new ResponseEntity<Airport>(airport, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Airport>(airport, HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/getAirport/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Airport> getAirport(@PathVariable long id) {

		Airport airport = null;
		try {
			airport = airportService.getAirport(id);
			return new ResponseEntity<Airport>(airport, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Airport>(airport, HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/deleteAirport/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> deleteAirport(@PathVariable long id) {

		try {
			airportService.deleteAirport(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/updateFuelPrice/{fuelPrice}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateFuelPrice(@PathVariable double fuelPrice) {

		try {
			fuelCostService.updateFuelCost(fuelPrice);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}

}
