package se.plushogskolan.jetbroker.plane.rest.planetype.model;

import se.plushogskolan.jetbroker.plane.domain.PlaneType;

public class CreatePlaneTypeRequest {

	private String code;
	private String name;
	private int maxNoOfPassengers;
	private int maxRangeKm;
	private int maxSpeedKmH;
	private double fuelConsumptionPerKm;

	public PlaneType buildPlaneType() {
		PlaneType pt = new PlaneType();
		pt.setCode(code);
		pt.setName(name);
		pt.setMaxNoOfPassengers(maxNoOfPassengers);
		pt.setMaxRangeKm(maxRangeKm);
		pt.setMaxSpeedKmH(maxSpeedKmH);
		pt.setFuelConsumptionPerKm(fuelConsumptionPerKm);
		return pt;

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNoOfPassengers() {
		return maxNoOfPassengers;
	}

	public void setMaxNoOfPassengers(int maxNoOfPassengers) {
		this.maxNoOfPassengers = maxNoOfPassengers;
	}

	public int getMaxRangeKm() {
		return maxRangeKm;
	}

	public void setMaxRangeKm(int maxRangeKm) {
		this.maxRangeKm = maxRangeKm;
	}

	public int getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public void setMaxSpeedKmH(int maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}

	public double getFuelConsumptionPerKm() {
		return fuelConsumptionPerKm;
	}

	public void setFuelConsumptionPerKm(double fuelConsumptionPerKm) {
		this.fuelConsumptionPerKm = fuelConsumptionPerKm;
	}

	@Override
	public String toString() {
		return String
		        .format("CreatePlaneTypeRequest [code=%s, name=%s, maxNoOfPassengers=%s, maxRangeKm=%s, makSpeedKmH=%s, fuelConsumptionPerKm=%s]",
		                code, name, maxNoOfPassengers, maxRangeKm, maxSpeedKmH, fuelConsumptionPerKm);
	}

}
