package se.plushogskolan.jetbroker.order.service;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.order.TestFixture;
import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.domain.FlightRequestStatus;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

public class FlightRequestServiceImplTest {

	FlightRequestServiceImpl flightRequestService;
	FlightRequest request;

	Airport airport1;
	Airport airport2;

	@Before
	public void setup() {
		flightRequestService = new FlightRequestServiceImpl();
		request = TestFixture.getValidFlightRequest();

		request.setDepartureAirportCode("AAA");
		request.setArrivalAirportCode("BBB");

		airport1 = new Airport("AAA", "Airport 1", 10, 20);
		airport2 = new Airport("BBB", "Airport 2", 20, 30);

	}

	@Test
	public void testHandleIncomingFlightRequest() {

		// Setup mock plane-service
		PlaneService service = createMock(PlaneService.class);
		expect(service.getAirport("AAA")).andReturn(airport1);
		expect(service.getAirport("BBB")).andReturn(airport2);
		replay(service);
		flightRequestService.setPlaneService(service);

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.persist(request)).andReturn(1L);
		replay(repo);
		flightRequestService.setFlightRequestRepository(repo);

		// Setup mock integration facade
		AgentIntegrationFacade facade =
		        createMock(AgentIntegrationFacade.class);
		facade.sendFlightRequestConfirmation(anyObject(FlightRequestConfirmation.class));
		expectLastCall();
		replay(facade);
		flightRequestService.setAgentIntegrationFacade(facade);

		// Perform test
		FlightRequestConfirmation confirmation = flightRequestService.handleIncomingFlightRequest(request);

		assertEquals("Confirmation agent id", 10L, confirmation.getAgentRequestId());
		assertEquals("Confirmation order id", 1L, confirmation.getOrderRequestId());
		assertEquals("Correct id", 1L, request.getId());
		assertEquals("Correct distance", 1545, request.getDistanceKm());
		assertEquals("Correct FlightRequestStatus", FlightRequestStatus.NEW, request.getStatus());

		// Verify
		verify(service);
		verify(repo);
		verify(facade);

	}

	@Test
	public void testRejectFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1L)).andReturn(request);
		repo.update(request);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepository(repo);

		// Setup mock integration facade
		AgentIntegrationFacade facade = createMock(AgentIntegrationFacade.class);
		facade.sendFlightRequestRejectedMessage(request);
		expectLastCall();
		replay(facade);
		flightRequestService.setAgentIntegrationFacade(facade);

		// Perform test
		flightRequestService.rejectFlightRequest(1L);
		assertEquals("Status is REJECTED_BY_US", FlightRequestStatus.REJECTED_BY_US, request.getStatus());
		assertEquals("Offer is null", null, request.getOffer());

		// Verify
		verify(repo);
		verify(facade);
	}

	@Test
	public void testHandleUpdatedOffer() {

		Offer offer = new Offer();

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1L)).andReturn(request);
		repo.update(request);
		expectLastCall();
		replay(repo);
		flightRequestService.setFlightRequestRepository(repo);

		// Setup mock integration facade
		AgentIntegrationFacade facade = createMock(AgentIntegrationFacade.class);
		facade.sendUpdatedOfferMessage(request);
		expectLastCall();
		replay(facade);
		flightRequestService.setAgentIntegrationFacade(facade);

		// Perform test
		flightRequestService.handleUpdatedOffer(1L, offer);
		assertEquals("Status is OFFER_SENT", FlightRequestStatus.OFFER_SENT, request.getStatus());
		assertEquals("Offer is set", offer, request.getOffer());

		// Verify
		verify(repo);
		verify(facade);
	}

}
