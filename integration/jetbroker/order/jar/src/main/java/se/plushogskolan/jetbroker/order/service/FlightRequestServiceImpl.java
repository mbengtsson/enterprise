package se.plushogskolan.jetbroker.order.service;

import se.plushogskolan.jee.utils.map.HaversineDistance;
import se.plushogskolan.jetbroker.order.domain.*;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.repository.FlightRequestRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class FlightRequestServiceImpl implements FlightRequestService {

	private static Logger log = Logger.getLogger(FlightRequestServiceImpl.class.getName());

	@Inject
	private FlightRequestRepository flightRequestRepository;

	@Inject
	private PlaneService planeService;

	@Inject
	private AgentIntegrationFacade agentIntegrationFacade;

	@Override
	public FlightRequestConfirmation handleIncomingFlightRequest(FlightRequest request) {

		log.fine("Incoming flight-request " + request);

		Airport departureAirport = planeService.getAirport(request.getDepartureAirportCode());
		Airport arrivalAirport = planeService.getAirport(request.getArrivalAirportCode());

		request.setStatus(FlightRequestStatus.NEW);
		request.setDistanceKm((int) Math.round(HaversineDistance.getDistance(departureAirport.getLatitude(),
				departureAirport.getLongitude(),
				arrivalAirport.getLatitude(), arrivalAirport.getLongitude())));
		Long id = flightRequestRepository.persist(request);

		FlightRequestConfirmation confirmation = new FlightRequestConfirmation(request.getAgentRequestId(), id);
		agentIntegrationFacade.sendFlightRequestConfirmation(confirmation);

		return confirmation;

	}

	@Override
	public void handleUpdatedOffer(long flightRequestId, Offer offer) {

		FlightRequest request = getFlightRequest(flightRequestId);
		request.setStatus(FlightRequestStatus.OFFER_SENT);
		request.setOffer(offer);

		log.fine("Flight offered " + offer + " for request " + request);

		flightRequestRepository.update(request);

		agentIntegrationFacade.sendUpdatedOfferMessage(request);
	}

	@Override
	public void rejectFlightRequest(long id) {

		FlightRequest request = getFlightRequest(id);
		request.setStatus(FlightRequestStatus.REJECTED_BY_US);
		request.setOffer(null);

		log.fine("Rejected flight-request " + request);

		flightRequestRepository.update(request);

		agentIntegrationFacade.sendFlightRequestRejectedMessage(request);
	}

	@Override
	public FlightRequest getFlightRequest(long id) {
		return getFlightRequestRepository().findById(id);
	}

	@Override
	public List<FlightRequest> getAllFlightRequests() {
		return getFlightRequestRepository().getAllFlightRequests();
	}

	@Override
	public void deleteFlightRequest(long id) {
		FlightRequest flightRequest = getFlightRequest(id);
		getFlightRequestRepository().remove(flightRequest);

	}

	public FlightRequestRepository getFlightRequestRepository() {
		return flightRequestRepository;
	}

	public void setFlightRequestRepository(FlightRequestRepository flightRequestRepository) {
		this.flightRequestRepository = flightRequestRepository;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}

	public AgentIntegrationFacade getAgentIntegrationFacade() {
		return agentIntegrationFacade;
	}

	public void setAgentIntegrationFacade(AgentIntegrationFacade agentIntegrationFacade) {
		this.agentIntegrationFacade = agentIntegrationFacade;
	}

}
