
package se.plushogskolan.jetbroker.order.integration.plane.ws.stubs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for wsPlaneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="wsPlaneType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fuelConsumptionPerKm" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="maxNoOfPassengers" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maxRangeKm" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maxSpeedKmH" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wsPlaneType", propOrder = {
    "code",
    "fuelConsumptionPerKm",
    "maxNoOfPassengers",
    "maxRangeKm",
    "maxSpeedKmH",
    "name"
})
public class WsPlaneType {

    protected String code;
    protected double fuelConsumptionPerKm;
    protected int maxNoOfPassengers;
    protected int maxRangeKm;
    protected int maxSpeedKmH;
    protected String name;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the fuelConsumptionPerKm property.
     * 
     */
    public double getFuelConsumptionPerKm() {
        return fuelConsumptionPerKm;
    }

    /**
     * Sets the value of the fuelConsumptionPerKm property.
     * 
     */
    public void setFuelConsumptionPerKm(double value) {
        this.fuelConsumptionPerKm = value;
    }

    /**
     * Gets the value of the maxNoOfPassengers property.
     * 
     */
    public int getMaxNoOfPassengers() {
        return maxNoOfPassengers;
    }

    /**
     * Sets the value of the maxNoOfPassengers property.
     * 
     */
    public void setMaxNoOfPassengers(int value) {
        this.maxNoOfPassengers = value;
    }

    /**
     * Gets the value of the maxRangeKm property.
     * 
     */
    public int getMaxRangeKm() {
        return maxRangeKm;
    }

    /**
     * Sets the value of the maxRangeKm property.
     * 
     */
    public void setMaxRangeKm(int value) {
        this.maxRangeKm = value;
    }

    /**
     * Gets the value of the maxSpeedKmH property.
     * 
     */
    public int getMaxSpeedKmH() {
        return maxSpeedKmH;
    }

    /**
     * Sets the value of the maxSpeedKmH property.
     * 
     */
    public void setMaxSpeedKmH(int value) {
        this.maxSpeedKmH = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

}
