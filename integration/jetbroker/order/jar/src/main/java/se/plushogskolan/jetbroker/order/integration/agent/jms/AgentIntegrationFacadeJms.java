package se.plushogskolan.jetbroker.order.integration.agent.jms;

import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.order.integration.agent.AgentIntegrationFacade;
import se.plushogskolan.jetbroker.order.xml.ObjectFactory;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequestOffer;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.*;
import java.util.logging.Logger;

public class AgentIntegrationFacadeJms implements AgentIntegrationFacade {

	@Inject
	Logger log;

	ObjectFactory objectFactory = new ObjectFactory();

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;

	@Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE)
	private Queue queue;

	@Override
	public void sendUpdatedOfferMessage(FlightRequest flightRequest) {

		log.fine("Prod: sendUpdateOfferMessage" + flightRequest);

		XmlFlightRequestOffer xmlOffer = objectFactory.createXmlFlightRequestOffer();
		flightRequestToXmlFlightRequestOffer(xmlOffer, flightRequest);

		QueueConnection connection = null;
		QueueSession session = null;
		try {
			connection = connectionFactory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

			TextMessage message = session.createTextMessage();

			message.setText(JaxbUtil.generateXmlString(xmlOffer, XmlFlightRequestOffer.class));
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_OFFER);

			QueueSender sender = session.createSender(queue);
			sender.send(message);

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}

	}

	@Override
	public void sendFlightRequestRejectedMessage(FlightRequest flightRequest) {

		log.fine("Prod: sendFlightRequestRejectedMessage" + flightRequest);

		QueueConnection connection = null;
		QueueSession session = null;
		try {
			connection = connectionFactory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

			MapMessage message = session.createMapMessage();
			message.setLong("agentId", flightRequest.getAgentRequestId());
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_REJECTION);

			QueueSender sender = session.createSender(queue);
			sender.send(message);

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}

	}

	@Override
	public void sendFlightRequestConfirmation(FlightRequestConfirmation response) {

		log.fine("Prod: sendFlightRequestConfirmation: " + response);

		QueueConnection connection = null;
		QueueSession session = null;
		try {
			connection = connectionFactory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

			MapMessage message = session.createMapMessage();
			message.setLong("agentId", response.getAgentRequestId());
			message.setLong("confirmationId", response.getOrderRequestId());
			message.setStringProperty("messageType", JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_CONFIRMATION);

			QueueSender sender = session.createSender(queue);
			sender.send(message);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}
	}

	private void flightRequestToXmlFlightRequestOffer(XmlFlightRequestOffer xmlOffer, FlightRequest request) {
		xmlOffer.setAgentRequestId(request.getAgentRequestId());
		xmlOffer.setPlaneTypeCode(request.getOffer().getPlane().getPlaneTypeCode());
		xmlOffer.setPrice(request.getOffer().getPrice());
	}

}
