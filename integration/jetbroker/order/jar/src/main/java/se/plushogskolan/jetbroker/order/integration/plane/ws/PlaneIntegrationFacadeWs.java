package se.plushogskolan.jetbroker.order.integration.plane.ws;

import se.plushogskolan.jetbroker.order.domain.Airport;
import se.plushogskolan.jetbroker.order.domain.PlaneType;
import se.plushogskolan.jetbroker.order.integration.plane.PlaneIntegrationFacade;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.order.integration.plane.ws.stubs.WsPlaneType;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by marcus on 2014-10-23.
 */
public class PlaneIntegrationFacadeWs implements PlaneIntegrationFacade {

	@Inject
	Logger log;

	@Override
	public List<Airport> getAllAirports() {

		PlaneWebServiceImplService service = new PlaneWebServiceImplService();
		PlaneWebService port = service.getPlaneWebServicePort();

		List<Airport> airports = new ArrayList<Airport>();

		for (WsAirport wsa : port.getAirports()) {
			airports.add(wsAirportToAirport(wsa));
		}

		log.fine("PROD: got airports from webservice");

		return airports;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {

		PlaneWebServiceImplService service = new PlaneWebServiceImplService();
		PlaneWebService port = service.getPlaneWebServicePort();

		List<PlaneType> planeTypes = new ArrayList<PlaneType>();

		for (WsPlaneType wspt : port.getPlaneTypes()) {
			planeTypes.add(wsPlaneTypeToPlaneType(wspt));
		}

		log.fine("PROD: get plane types from webservice");

		return planeTypes;
	}

	private Airport wsAirportToAirport(WsAirport wsa) {

		Airport airport = new Airport();

		airport.setCode(wsa.getCode());
		airport.setName(wsa.getName());
		airport.setLatitude(wsa.getLatitude());
		airport.setLongitude(wsa.getLongitude());

		return airport;
	}

	private PlaneType wsPlaneTypeToPlaneType(WsPlaneType wspt) {

		PlaneType planeType = new PlaneType();

		planeType.setCode(wspt.getCode());
		planeType.setName(wspt.getName());
		planeType.setMaxNoOfPassengers(wspt.getMaxNoOfPassengers());
		planeType.setMaxRangeKm(wspt.getMaxRangeKm());
		planeType.setMaxSpeedKmH(wspt.getMaxSpeedKmH());
		planeType.setFuelConsumptionPerKm(wspt.getFuelConsumptionPerKm());

		return planeType;

	}
}
