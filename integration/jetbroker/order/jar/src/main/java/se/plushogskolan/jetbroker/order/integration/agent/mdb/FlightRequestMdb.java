package se.plushogskolan.jetbroker.order.integration.agent.mdb;

import org.joda.time.DateTime;
import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequest;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Logger;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
				propertyName = "destinationType",
				propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(
				propertyName = "destination",
				propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)
})
public class FlightRequestMdb extends AbstractMDB implements MessageListener {

	@Inject
	Logger log;

	@Inject
	FlightRequestService flightRequestService;

	@Override
	public void onMessage(Message message) {

		log.fine("Flight-request message recieved");

		FlightRequest request = new FlightRequest();

		try {
			TextMessage msg = (TextMessage) message;

			XmlFlightRequest xmlRequest = (XmlFlightRequest) JaxbUtil.generateJaxbObject(msg.getText(),
					XmlFlightRequest.class);

			xmlFlightRequestToFlightRequest(xmlRequest, request);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		flightRequestService.handleIncomingFlightRequest(request);

	}

	public void xmlFlightRequestToFlightRequest(XmlFlightRequest xmlRequest, FlightRequest request) {
		request.setAgentRequestId(xmlRequest.getAgentId());
		request.setDepartureAirportCode(xmlRequest.getDepartureAirportCode());
		request.setArrivalAirportCode(xmlRequest.getArrivalAirportCode());
		request.setNoOfPassengers(xmlRequest.getNoOfPassengers());
		request.setDepartureTime(new DateTime(xmlRequest.getDepartureTime()));
	}

}
