package se.plushogskolan.jetbroker.order.integration.plane.mdb;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.order.service.PlaneService;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
				propertyName = "destinationType",
				propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(
				propertyName = "destination",
				propertyValue = JmsConstants.TOPIC_PLANE_BROADCAST),
		@ActivationConfigProperty(
				propertyName = "messageSelector",
				propertyValue = "messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_FUELPRICECHANGED + "'")

})
public class FuelPriceMdb extends AbstractMDB implements MessageListener {

	@Inject
	Logger log;

	@Inject
	private PlaneService planeService;

	@Override
	public void onMessage(Message message) {

		log.fine("Fuelprice changed incoming message");

		try {
			double fuelPrice = message.getDoubleProperty("fuelPrice");
			planeService.updateFuelCostPerLiter(fuelPrice);

			log.fine("receivedNewFuelPrice. New price is " + fuelPrice);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}

	}

}
