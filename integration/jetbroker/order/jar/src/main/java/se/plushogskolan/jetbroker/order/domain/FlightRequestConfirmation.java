package se.plushogskolan.jetbroker.order.domain;

public class FlightRequestConfirmation {

	long agentRequestId;
	long orderRequestId;

	public FlightRequestConfirmation(long agentRequestId, long orderRequestId) {
		setAgentRequestId(agentRequestId);
		setOrderRequestId(orderRequestId);
	}

	public long getAgentRequestId() {
		return agentRequestId;
	}

	public void setAgentRequestId(long agentRequestId) {
		this.agentRequestId = agentRequestId;
	}

	public long getOrderRequestId() {
		return orderRequestId;
	}

	public void setOrderRequestId(long confirmationId) {
		this.orderRequestId = confirmationId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (agentRequestId ^ (agentRequestId >>> 32));
		result = prime * result + (int) (orderRequestId ^ (orderRequestId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlightRequestConfirmation other = (FlightRequestConfirmation) obj;
		if (agentRequestId != other.agentRequestId)
			return false;
		if (orderRequestId != other.orderRequestId)
			return false;
		return true;
	}

}
