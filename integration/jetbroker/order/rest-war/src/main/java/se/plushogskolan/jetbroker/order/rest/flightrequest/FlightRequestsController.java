package se.plushogskolan.jetbroker.order.rest.flightrequest;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.order.domain.FlightRequest;
import se.plushogskolan.jetbroker.order.domain.Offer;
import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.rest.OkOrErrorResponse;
import se.plushogskolan.jetbroker.order.rest.flightrequest.model.GetFlightRequestResponse;
import se.plushogskolan.jetbroker.order.rest.flightrequest.model.UpdateOfferRequest;
import se.plushogskolan.jetbroker.order.service.FlightRequestService;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
public class FlightRequestsController {

	Logger log = Logger.getLogger(FlightRequestsController.class.getName());

	public static String DATE_FORMAT = "dd-MM-yyyy HH:mm";

	@Inject
	private FlightRequestService flightRequestService;

	@Inject
	private PlaneService planeService;

	@RequestMapping(value = "/getFlightRequest/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetFlightRequestResponse getFlightRequest(@PathVariable long id) throws Exception {
		log.fine("getFlightRequest: " + id);

		FlightRequest flightRequest = getFlightRequestService().getFlightRequest(id);

		return new GetFlightRequestResponse(flightRequest);

	}

	@RequestMapping(value = "/rejectFlightRequest/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse rejectFlightRequest(@PathVariable long id) throws Exception {
		log.fine("rejectFlightRequest: " + id);

		getFlightRequestService().rejectFlightRequest(id);

		return OkOrErrorResponse.getOkResponse();

	}

	@RequestMapping(value = "/updateOffer/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse updateOffer(@PathVariable long id, @RequestBody UpdateOfferRequest offerRequest)
			throws Exception {
		log.fine("updateOffer: Id=" + id + ", offer=" + offerRequest);

		Offer offer = new Offer();
		offer.setPrice(offerRequest.getPrice());
		Plane plane = getPlaneService().getPlane(offerRequest.getPlaneId());
		offer.setPlane(plane);

		getFlightRequestService().handleUpdatedOffer(id, offer);

		return OkOrErrorResponse.getOkResponse();

	}

	@RequestMapping(value = "/deleteFlightRequest/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public OkOrErrorResponse deleteFlightRequest(@PathVariable long id) throws Exception {
		log.fine("deleteFlightRequest: " + id);

		getFlightRequestService().deleteFlightRequest(id);

		return OkOrErrorResponse.getOkResponse();

	}

	public FlightRequestService getFlightRequestService() {
		return flightRequestService;
	}

	public void setFlightRequestService(FlightRequestService service) {
		this.flightRequestService = service;
	}

	public PlaneService getPlaneService() {
		return planeService;
	}

	public void setPlaneService(PlaneService planeService) {
		this.planeService = planeService;
	}
}
