package se.plushogskolan.jetbroker.order.rest.airport.model;

public class GetFuelPriceResponse {

	private double fuelPrice;

	public GetFuelPriceResponse(double price) {
		setFuelPrice(price);
	}

	public double getFuelPrice() {
		return fuelPrice;
	}

	public void setFuelPrice(double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

}
