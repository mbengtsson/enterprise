package se.plushogskolan.jetbroker.order.rest.airport;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import se.plushogskolan.jetbroker.order.rest.airport.model.GetFuelPriceResponse;
import se.plushogskolan.jetbroker.order.service.PlaneService;

import javax.inject.Inject;
import java.util.logging.Logger;

@Controller
public class AirportController {

	Logger log = Logger.getLogger(AirportController.class.getName());

	@Inject
	private PlaneService planeService;

	@RequestMapping(value = "/getFuelPrice", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetFuelPriceResponse getPlaneType() {

		log.severe(">>>>CLASSIC BERTIL CODE CALLED!!!!<<<<");

		double fuelCost = planeService.getFuelCostPerLiter();

		log.fine("getFuelPrice " + fuelCost);

		return new GetFuelPriceResponse(fuelCost);
	}

}
