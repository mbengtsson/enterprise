package se.plushogskolan.jetbroker.order.rest.plane;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import se.plushogskolan.jetbroker.order.domain.Plane;
import se.plushogskolan.jetbroker.order.rest.plane.model.CreatePlaneRequest;
import se.plushogskolan.jetbroker.order.service.PlaneService;

@Controller
public class PlaneController {

	Logger log = Logger.getLogger(PlaneController.class.getName());

	@Inject
	private PlaneService planeService;

	@RequestMapping(value = "/createPlane", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Plane> createPlane(@RequestBody CreatePlaneRequest request) throws Exception {
		log.fine("createPlane: " + request);

		Plane plane = null;
		try {
			plane = planeService.createPlane(request.buildPlane());
			return new ResponseEntity<Plane>(plane, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Plane>(plane, HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/getPlane/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Plane> getPlane(@PathVariable long id) {

		Plane plane = null;
		try {
			plane = planeService.getPlane(id);
			return new ResponseEntity<Plane>(plane, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Plane>(plane, HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/deletePlane/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> deletePlane(@PathVariable long id) {

		try {
			planeService.deletePlane(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}

}
