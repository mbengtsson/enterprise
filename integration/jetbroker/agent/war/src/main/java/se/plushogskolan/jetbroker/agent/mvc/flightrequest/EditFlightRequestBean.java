package se.plushogskolan.jetbroker.agent.mvc.flightrequest;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;
import org.joda.time.MutableDateTime;

import se.plushogskolan.jee.utils.validation.ArrivalAndDepartureAirportHolder;
import se.plushogskolan.jee.utils.validation.DifferentArrivalAndDepartureAirport;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

@DifferentArrivalAndDepartureAirport
public class EditFlightRequestBean implements ArrivalAndDepartureAirportHolder {

	protected static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	private long id;

	@Range(min = 1, max = 500)
	private int noOfPassengers;
	@NotBlank
	private String departureAirportCode;
	@NotBlank
	private String arrivalAirportCode;
	@Min(value = 1, message = "{validation.custumer.missing}")
	private long customerId;
	@Pattern(regexp = "\\d\\d-\\d\\d-\\d\\d\\d\\d", message = "{date.invalid.format}")
	private String date;
	@NotNull
	@Range(min = 0, max = 59)
	private int minute = -1;
	@NotNull
	@Range(min = 0, max = 23)
	private int hour = -1;

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNoOfPassengers() {
		return noOfPassengers;
	}

	public void setNoOfPassengers(int noOfPassengers) {
		this.noOfPassengers = noOfPassengers;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public void copyFlightRequestValuesToBean(FlightRequest flightRequest) {

		setId(flightRequest.getId());
		setArrivalAirportCode(flightRequest.getArrivalAirportCode());
		setDepartureAirportCode(flightRequest.getDepartureAirportCode());
		setNoOfPassengers(flightRequest.getNoOfPassengers());

		if (flightRequest.getCustomer() != null) {
			setCustomerId(flightRequest.getCustomer().getId());
		}

		if (flightRequest.getDepartureTime() != null) {
			setDate(dateFormat.format(flightRequest.getDepartureTimeAsDate()));
			setHour(flightRequest.getDepartureTime().getHourOfDay());
			setMinute(flightRequest.getDepartureTime().getMinuteOfHour());
		} else {
			setDate("");
			setMinute(-1);
			setHour(-1);
		}

	}

	public void copyBeanValuesToFlightRequest(FlightRequest request, Customer customer) throws Exception {
		request.setDepartureAirportCode(getDepartureAirportCode());
		request.setArrivalAirportCode(getArrivalAirportCode());
		request.setNoOfPassengers(getNoOfPassengers());
		request.setCustomer(customer);

		Date date = dateFormat.parse(getDate());
		MutableDateTime dateTime = new MutableDateTime(date.getTime());
		dateTime.setHourOfDay(getHour());
		dateTime.setMinuteOfHour(getMinute());
		request.setDepartureTime(dateTime.toDateTime());

	}

}
