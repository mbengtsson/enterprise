package se.plushogskolan.jetbroker.agent.services;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import se.plushogskolan.jetbroker.agent.TestFixture;
import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

public class FlightRequestServiceImpTest {

	private FlightRequestServiceImp service;
	private FlightRequest request;

	@Before
	public void setup() {
		service = new FlightRequestServiceImp();
		request = TestFixture.getValidFlightRequest();
	}

    @Test
	public void testCreateFlightRequest() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.persist(request)).andReturn(1L);
		replay(repo);
		service.setRepository(repo);

		// Setup integration facade
		OrderFacade facade = createMock(OrderFacade.class);
		facade.sendFlightRequest(request);
		expectLastCall();
		replay(facade);
		service.setOrderFacade(facade);

		// Perform test
		assertEquals("Request status is CREATED", FlightRequestStatus.CREATED, service.createFlightRequest(request)
		        .getRequestStatus());

		// Verify
		verify(repo);
		verify(facade);
	}

	@Test
	public void testHandleFlightRequestConfirmation() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1L)).andReturn(request);
		repo.update(request);
		expectLastCall();
		replay(repo);
		service.setRepository(repo);

		// Perform test
		service.handleFlightRequestConfirmation(new FlightRequestConfirmation(1L, 1L));
		assertEquals("Status is REQUEST_CONFIRMED", FlightRequestStatus.REQUEST_CONFIRMED, request.getRequestStatus());
		assertEquals("ConfirmationID is 1", 1L, request.getConfirmationId());

		// Verify
		verify(repo);
	}

	@Test
	public void testHandleFlightRequestRejection() {

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1L)).andReturn(request);
		repo.update(request);
		expectLastCall();
		replay(repo);
		service.setRepository(repo);

		// Perform test
		service.handleFlightRequestRejection(1L);
		assertEquals("Status is REJECTED", FlightRequestStatus.REJECTED, request.getRequestStatus());
		assertEquals("Offer is null", null, request.getOffer());

		// Verify
		verify(repo);

	}

	@Test
	public void testHandleFlightOffer() {

		FlightOffer offer = new FlightOffer();

		// Setup mock repo
		FlightRequestRepository repo = createMock(FlightRequestRepository.class);
		expect(repo.findById(1L)).andReturn(request);
		repo.update(request);
		expectLastCall();
		replay(repo);
		service.setRepository(repo);

		// Perform test
		service.handleFlightOffer(offer, 1L);
		assertEquals("Status is OFFER_RECEIVED", FlightRequestStatus.OFFER_RECEIVED, request.getRequestStatus());
		assertEquals("Offer is set", offer, request.getOffer());

		// Verify
		verify(repo);
	}
}
