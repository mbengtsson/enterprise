package se.plushogskolan.jetbroker.agent.integration.plane.ws;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.PlaneWebServiceImplService;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsAirport;
import se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs.WsPlaneType;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by marcus on 2014-10-23.
 */
public class PlaneFacadeWs implements PlaneFacade {

	@Inject
	Logger log;

	@Override
	public List<AirPort> getAllAirports() {

		PlaneWebServiceImplService service = new PlaneWebServiceImplService();
		PlaneWebService port = service.getPlaneWebServicePort();

		List<AirPort> airports = new ArrayList<AirPort>();

		for (WsAirport wsa : port.getAirports()) {
			airports.add(wsAirportToAirport(wsa));
		}

		log.fine("PROD: got airports from webservice " + airports);

		return airports;
	}

	@Override
	public List<PlaneType> getAllPlaneTypes() {

		PlaneWebServiceImplService service = new PlaneWebServiceImplService();
		PlaneWebService port = service.getPlaneWebServicePort();

		List<PlaneType> planeTypes = new ArrayList<PlaneType>();

		for (WsPlaneType wspt : port.getPlaneTypes()) {
			planeTypes.add(wsPlaneTypeToPlaneType(wspt));

		}

		log.fine("PROD: get plane types from webservice " + planeTypes);

		return planeTypes;
	}

	private AirPort wsAirportToAirport(WsAirport wsa) {

		AirPort airport = new AirPort();

		airport.setCode(wsa.getCode());
		airport.setName(wsa.getName());

		return airport;
	}

	private PlaneType wsPlaneTypeToPlaneType(WsPlaneType wspt) {

		PlaneType planeType = new PlaneType();

		planeType.setCode(wspt.getCode());
		planeType.setName(wspt.getName());
		planeType.setMaxNoOfPassengers(wspt.getMaxNoOfPassengers());
		planeType.setMaxRangeKm(wspt.getMaxRangeKm());
		planeType.setMaxSpeedKmH(wspt.getMaxSpeedKmH());
		planeType.setFuelConsumptionPerKm(wspt.getFuelConsumptionPerKm());

		return planeType;

	}
}
