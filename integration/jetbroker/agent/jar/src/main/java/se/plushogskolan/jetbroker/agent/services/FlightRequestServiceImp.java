package se.plushogskolan.jetbroker.agent.services;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged;
import se.plushogskolan.jetbroker.agent.annotations.FlightRequestChanged.ChangeType;
import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.events.FlightRequestChangedEvent;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.agent.repository.FlightRequestRepository;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class FlightRequestServiceImp implements FlightRequestService {
	private static Logger log = Logger.getLogger(FlightRequestServiceImp.class.getName());

	@Inject
	private FlightRequestRepository repository;

	@Inject
	private OrderFacade orderFacade;

	@Resource
	private SessionContext context;

	@Inject
	@Any
	@FlightRequestChanged(ChangeType.CONFIRMED)
	@Prod
	Event<FlightRequestChangedEvent> flightRequestConfirmationEvent;

	@Inject
	@Any
	@FlightRequestChanged(ChangeType.OFFER_RECEIVED)
	@Prod
	Event<FlightRequestChangedEvent> flightRequestOfferRecievedEvent;

	@Inject
	@Any
	@FlightRequestChanged(ChangeType.REJECTED)
	@Prod
	Event<FlightRequestChangedEvent> flightRequestRejectedEvent;

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public FlightRequest getFlightRequest(long id) {
		return getRepository().findById(id);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<FlightRequest> getAllFlightRequests() {
		return getRepository().getAllFlightRequests();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public FlightRequest createFlightRequest(FlightRequest request) {

		log.fine("crating flight request " + request);

		request.setRequestStatus(FlightRequestStatus.CREATED);
		repository.persist(request);
		orderFacade.sendFlightRequest(request);

		return request;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void handleFlightRequestConfirmation(FlightRequestConfirmation response) {

		log.fine("handle flight request confirmation" + response);

		FlightRequest request = getFlightRequest(response.getAgentRequestId());
		request.setConfirmationId(response.getOrderRequestId());
		request.setRequestStatus(FlightRequestStatus.REQUEST_CONFIRMED);
		updateFlightRequest(request);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void handleFlightRequestRejection(long flightRequestId) {

		log.fine("handle flight request rejection " + flightRequestId);

		FlightRequest request = getFlightRequest(flightRequestId);
		request.setRequestStatus(FlightRequestStatus.REJECTED);
		request.setOffer(null);
		updateFlightRequest(request);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void handleFlightOffer(FlightOffer offer, long flightRequestId) {

		log.fine("handle flight offer " + offer);

		FlightRequest request = getFlightRequest(flightRequestId);
		request.setRequestStatus(FlightRequestStatus.OFFER_RECEIVED);
		request.setOffer(offer);
		updateFlightRequest(request);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFlightRequest(FlightRequest request) {

		log.fine("updateFlightRequest: " + request);

		getRepository().update(request);

		if (request.getNoOfPassengers() == 13) {
			log.fine("Rollbacking transaction since passengers = 13");
			context.setRollbackOnly();
		}
	}

	@Override
	public List<FlightRequest> getFlightRequestsForCustomer(long id) {
		return getRepository().getFlightRequestsForCustomer(id);
	}

	@Override
	public void deleteFlightRequest(long id) {
		log.fine("deleteFlightRequest, id=" + id);
		FlightRequest flightRequest = getFlightRequest(id);
		getRepository().remove(flightRequest);

	}

	public FlightRequestRepository getRepository() {
		return repository;
	}

	public void setRepository(FlightRequestRepository repository) {
		this.repository = repository;
	}

	public OrderFacade getOrderFacade() {
		return orderFacade;
	}

	public void setOrderFacade(OrderFacade orderFacade) {
		this.orderFacade = orderFacade;
	}

}
