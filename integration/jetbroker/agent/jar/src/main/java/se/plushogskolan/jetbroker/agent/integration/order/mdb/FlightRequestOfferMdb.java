package se.plushogskolan.jetbroker.agent.integration.order.mdb;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightOffer;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequestOffer;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Logger;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
				propertyName = "destinationType",
				propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(
				propertyName = "destination",
				propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE),
		@ActivationConfigProperty(
				propertyName = "messageSelector",
				propertyValue = "messageType = '" + JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_OFFER + "'")
})
public class FlightRequestOfferMdb extends AbstractMDB implements MessageListener {

	@Inject
	Logger log;

	@Inject
	FlightRequestService flightRequestService;

	@Override
	public void onMessage(Message message) {

		log.fine("Flight-request offer received");

		FlightOffer offer = new FlightOffer();
		try {
			TextMessage msg = (TextMessage) message;

			XmlFlightRequestOffer xmlOffer = (XmlFlightRequestOffer) JaxbUtil.generateJaxbObject(msg.getText(),
					XmlFlightRequestOffer.class);

			xmlFlightRequestOfferToFlightOffer(xmlOffer, offer);


			flightRequestService.handleFlightOffer(offer, xmlOffer.getAgentRequestId());

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private void xmlFlightRequestOfferToFlightOffer(XmlFlightRequestOffer xmlOffer, FlightOffer offer) {
		offer.setOfferedPrice(xmlOffer.getPrice());
		offer.setPlaneTypeCode(xmlOffer.getPlaneTypeCode());
	}

}
