package se.plushogskolan.jetbroker.agent.integration.order.mdb;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestConfirmation;
import se.plushogskolan.jetbroker.agent.services.FlightRequestService;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
				propertyName = "destinationType",
				propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(
				propertyName = "destination",
				propertyValue = JmsConstants.QUEUE_FLIGHTREQUEST_RESPONSE),
		@ActivationConfigProperty(
				propertyName = "messageSelector",
				propertyValue = "messageType = '" + JmsConstants.MSGTYPE_FLIGHTREQUEST_RESPONSE_CONFIRMATION + "'")
})
public class FlightRequestConfirmationMdb extends AbstractMDB implements MessageListener {

	@Inject
	Logger log;

	@Inject
	FlightRequestService flightRequestService;

	@Override
	public void onMessage(Message message) {

		log.fine("Flight-request confirmation received");

		FlightRequestConfirmation confirmation = new FlightRequestConfirmation();

		try {
			MapMessage msg = (MapMessage) message;
			confirmation.setAgentRequestId(msg.getLong("agentId"));
			confirmation.setOrderRequestId(msg.getLong("confirmationId"));
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}

		flightRequestService.handleFlightRequestConfirmation(confirmation);

	}

}
