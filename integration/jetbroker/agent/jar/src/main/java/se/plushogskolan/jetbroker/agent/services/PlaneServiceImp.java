package se.plushogskolan.jetbroker.agent.services;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class PlaneServiceImp implements PlaneService {

	private static Logger log = Logger.getLogger(FlightRequestServiceImp.class.getName());

	@Inject
	private PlaneTypeRepository planeTypeRepository;

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<PlaneType> getAllPlaneTypes() {
		return getPlaneTypeRepository().getAllPlaneTypes();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public PlaneType getPlaneType(String code) {
		return getPlaneTypeRepository().getPlaneType(code);
	}

	public PlaneTypeRepository getPlaneTypeRepository() {
		return planeTypeRepository;
	}

	public void setPlaneTypeRepository(PlaneTypeRepository planeTypeRepository) {
		this.planeTypeRepository = planeTypeRepository;
	}

	@Override
	public void handlePlaneTypesChangedEvent() {

		log.fine("handles plane types changed event");

		planeTypeRepository.handlePlaneTypeChangedEvent();

	}

}
