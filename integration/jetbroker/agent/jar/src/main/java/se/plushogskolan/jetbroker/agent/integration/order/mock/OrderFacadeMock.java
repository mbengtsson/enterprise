package se.plushogskolan.jetbroker.agent.integration.order.mock;

import se.plushogskolan.jee.utils.cdi.MockIntegration;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;

import java.util.logging.Logger;

@MockIntegration
public class OrderFacadeMock implements OrderFacade {

	private static Logger log = Logger.getLogger(OrderFacadeMock.class.getName());

	public void sendFlightRequest(FlightRequest request) {

		log.info("sendFlightRequest in OrderFacade MOCK: " + request);
	}

}
