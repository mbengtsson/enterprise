
package se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPlaneTypes_QNAME = new QName("http://ws.jetbroker.integration.plane.jetbroker.plushogskolan.se/", "getPlaneTypes");
    private final static QName _GetPlaneTypesResponse_QNAME = new QName("http://ws.jetbroker.integration.plane.jetbroker.plushogskolan.se/", "getPlaneTypesResponse");
    private final static QName _GetAirports_QNAME = new QName("http://ws.jetbroker.integration.plane.jetbroker.plushogskolan.se/", "getAirports");
    private final static QName _GetAirportsResponse_QNAME = new QName("http://ws.jetbroker.integration.plane.jetbroker.plushogskolan.se/", "getAirportsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: se.plushogskolan.jetbroker.agent.integration.plane.ws.stubs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPlaneTypesResponse }
     * 
     */
    public GetPlaneTypesResponse createGetPlaneTypesResponse() {
        return new GetPlaneTypesResponse();
    }

    /**
     * Create an instance of {@link GetPlaneTypes }
     * 
     */
    public GetPlaneTypes createGetPlaneTypes() {
        return new GetPlaneTypes();
    }

    /**
     * Create an instance of {@link GetAirportsResponse }
     * 
     */
    public GetAirportsResponse createGetAirportsResponse() {
        return new GetAirportsResponse();
    }

    /**
     * Create an instance of {@link GetAirports }
     * 
     */
    public GetAirports createGetAirports() {
        return new GetAirports();
    }

    /**
     * Create an instance of {@link WsPlaneType }
     * 
     */
    public WsPlaneType createWsPlaneType() {
        return new WsPlaneType();
    }

    /**
     * Create an instance of {@link WsAirport }
     * 
     */
    public WsAirport createWsAirport() {
        return new WsAirport();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlaneTypes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.jetbroker.integration.plane.jetbroker.plushogskolan.se/", name = "getPlaneTypes")
    public JAXBElement<GetPlaneTypes> createGetPlaneTypes(GetPlaneTypes value) {
        return new JAXBElement<GetPlaneTypes>(_GetPlaneTypes_QNAME, GetPlaneTypes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlaneTypesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.jetbroker.integration.plane.jetbroker.plushogskolan.se/", name = "getPlaneTypesResponse")
    public JAXBElement<GetPlaneTypesResponse> createGetPlaneTypesResponse(GetPlaneTypesResponse value) {
        return new JAXBElement<GetPlaneTypesResponse>(_GetPlaneTypesResponse_QNAME, GetPlaneTypesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAirports }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.jetbroker.integration.plane.jetbroker.plushogskolan.se/", name = "getAirports")
    public JAXBElement<GetAirports> createGetAirports(GetAirports value) {
        return new JAXBElement<GetAirports>(_GetAirports_QNAME, GetAirports.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAirportsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.jetbroker.integration.plane.jetbroker.plushogskolan.se/", name = "getAirportsResponse")
    public JAXBElement<GetAirportsResponse> createGetAirportsResponse(GetAirportsResponse value) {
        return new JAXBElement<GetAirportsResponse>(_GetAirportsResponse_QNAME, GetAirportsResponse.class, null, value);
    }

}
