package se.plushogskolan.jetbroker.agent.repository.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class CachedAirPortRepository implements AirPortRepository {

	@Inject
	Logger log;

	@Inject
	private PlaneFacade planeFacade;

	@Override
	public List<AirPort> getAllAirPorts() {

		Cache cache = CacheManager.getInstance().getCache("airportCache");

		if (cache.isKeyInCache("all_airports")) {

			log.fine("Returning airport-list from cache");

			Element e = cache.get("all_airports");

			return (List<AirPort>) e.getObjectValue();
		} else {

			List<AirPort> airportList = planeFacade.getAllAirports();

			log.fine("Returning airport-list from webservice");
			log.fine("Caching airport-list");

			cache.put(new Element("all_airports", airportList));

			return airportList;

		}

	}

	@Override
	public AirPort getAirPort(String code) {

		Cache cache = CacheManager.getInstance().getCache("airportCache");

		if (cache.isKeyInCache(code)) {

			log.fine("Returning airport from cache");

			Element e = cache.get(code);

			return (AirPort) e.getObjectValue();
		} else {
			for (AirPort airport : getAllAirPorts()) {
				if (airport.getCode().equals(code)) {

					log.fine("Returning airport from airport list");
					log.fine("Caching airport");

					cache.put(new Element(code, airport));
					return airport;
				}
			}
		}

		log.fine("No airport found for code " + code);
		return null;

	}

	@Override
	public void handleAirportsChangedEvent() {

		Cache cache = CacheManager.getInstance().getCache("airportCache");

		cache.removeAll();

		log.fine("Received airports changed event. ");
	}

}
