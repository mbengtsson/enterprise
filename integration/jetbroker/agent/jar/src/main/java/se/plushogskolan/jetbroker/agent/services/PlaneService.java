package se.plushogskolan.jetbroker.agent.services;

import java.util.List;

import javax.ejb.Local;

import se.plushogskolan.jetbroker.agent.domain.PlaneType;

@Local
public interface PlaneService {

	List<PlaneType> getAllPlaneTypes();

	PlaneType getPlaneType(String code);

	void handlePlaneTypesChangedEvent();

}
