package se.plushogskolan.jetbroker.agent.repository.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import se.plushogskolan.jetbroker.agent.domain.PlaneType;
import se.plushogskolan.jetbroker.agent.integration.plane.PlaneFacade;
import se.plushogskolan.jetbroker.agent.repository.PlaneTypeRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class CachedPlaneTypeRepository implements PlaneTypeRepository {
	@Inject
	Logger log;

	@Inject
	private PlaneFacade planeFacade;

	@Override
	public List<PlaneType> getAllPlaneTypes() {

		Cache cache = CacheManager.getInstance().getCache("planeTypeCache");

		if (cache.isKeyInCache("all_planes")) {

			log.fine("Returning planetype-list from cache");

			Element e = cache.get("all_planes");

			return (List<PlaneType>) e.getObjectValue();
		} else {

			List<PlaneType> planeTypeList = planeFacade.getAllPlaneTypes();

			log.fine("Returning planetype-list from webservice");
			log.fine("Caching planetype-list");

			cache.put(new Element("all_planes", planeTypeList));

			return planeTypeList;

		}
	}

	@Override
	public PlaneType getPlaneType(String code) {

		Cache cache = CacheManager.getInstance().getCache("planeTypeCache");

		if (cache.isKeyInCache(code)) {

			log.fine("Returning planetype from cache");

			Element e = cache.get(code);

			return (PlaneType) e.getObjectValue();
		} else {
			for (PlaneType planeType : getAllPlaneTypes()) {
				if (planeType.getCode().equals(code)) {

					log.fine("Returning planetype from planetype list");
					log.fine("Caching planetype");

					cache.put(new Element(code, planeType));
					return planeType;
				}
			}
		}

		log.info("No plane type found for code " + code);
		return null;
	}

	@Override
	public void handlePlaneTypeChangedEvent() {

		Cache cache = CacheManager.getInstance().getCache("planeTypeCache");

		cache.removeAll();

		log.fine("Handle plane types changed event");

	}

}
