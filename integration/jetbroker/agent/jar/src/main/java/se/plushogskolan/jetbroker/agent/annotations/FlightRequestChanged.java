package se.plushogskolan.jetbroker.agent.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * A Qualifier that represents the type of change that happened to a FlightRequest. Can be used to qualify events etc.
 * 
 */
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE, METHOD, FIELD, PARAMETER })
public @interface FlightRequestChanged {

	public ChangeType value();

	enum ChangeType {

		CONFIRMED, OFFER_RECEIVED, REJECTED;
	}

}
