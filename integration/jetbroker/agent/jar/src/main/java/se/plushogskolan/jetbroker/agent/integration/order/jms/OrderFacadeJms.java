package se.plushogskolan.jetbroker.agent.integration.order.jms;

import se.plushogskolan.jee.utils.cdi.Prod;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jee.utils.jms.JmsHelper;
import se.plushogskolan.jee.utils.xml.JaxbUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.integration.order.OrderFacade;
import se.plushogskolan.jetbroker.order.xml.ObjectFactory;
import se.plushogskolan.jetbroker.order.xml.XmlFlightRequest;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.*;
import java.util.logging.Logger;


public class OrderFacadeJms implements OrderFacade {

	@Inject
	Logger log;

	ObjectFactory objectFactory = new ObjectFactory();

	@Resource(mappedName = JmsConstants.JNDI_CONNECTIONFACTORY)
	private QueueConnectionFactory connectionFactory;

	@Resource(mappedName = JmsConstants.QUEUE_FLIGHTREQUEST_REQUEST)
	private Queue queue;

	@Override
	public void sendFlightRequest(FlightRequest request) {
		log.fine("sendFlightRequest in OrderFacade: " + request);

		XmlFlightRequest xmlRequest = objectFactory.createXmlFlightRequest();
		flightRequestToXmlFlightRequest(xmlRequest, request);

		QueueConnection connection = null;
		QueueSession session = null;

		try {
			connection = connectionFactory.createQueueConnection();
			session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

			TextMessage message = session.createTextMessage();

			message.setText(JaxbUtil.generateXmlString(xmlRequest, XmlFlightRequest.class));

			QueueSender sender = session.createSender(queue);
			sender.send(message);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JmsHelper.closeConnectionAndSession(connection, session);
		}
	}

	private void flightRequestToXmlFlightRequest(XmlFlightRequest xmlRequest, FlightRequest request) {

		xmlRequest.setAgentId(request.getId());
		xmlRequest.setDepartureAirportCode(request.getDepartureAirportCode());
		xmlRequest.setArrivalAirportCode(request.getArrivalAirportCode());
		xmlRequest.setDepartureTime(request.getDepartureTime().getMillis());
		xmlRequest.setNoOfPassengers(request.getNoOfPassengers());
	}

}
