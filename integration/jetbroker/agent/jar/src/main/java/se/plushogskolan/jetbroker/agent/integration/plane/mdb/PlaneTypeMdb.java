package se.plushogskolan.jetbroker.agent.integration.plane.mdb;

import se.plushogskolan.jee.utils.jms.AbstractMDB;
import se.plushogskolan.jee.utils.jms.JmsConstants;
import se.plushogskolan.jetbroker.agent.services.PlaneService;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

/**
 * Created by marcus on 2014-10-23.
 */

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(
				propertyName = "destinationType",
				propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(
				propertyName = "destination",
				propertyValue = JmsConstants.TOPIC_PLANE_BROADCAST),
		@ActivationConfigProperty(
				propertyName = "messageSelector",
				propertyValue = "messageType = '" + JmsConstants.MSGTYPE_PLANEBROADCAST_PLANETYPESCHANGED + "'")

})
public class PlaneTypeMdb extends AbstractMDB implements MessageListener {

	@Inject
	Logger log;

	@Inject
	PlaneService planeService;

	@Override
	public void onMessage(Message message) {

		log.fine("planetypes changed message received");

		planeService.handlePlaneTypesChangedEvent();
	}
}
