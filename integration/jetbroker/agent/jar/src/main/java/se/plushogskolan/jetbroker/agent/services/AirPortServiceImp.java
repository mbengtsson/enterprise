package se.plushogskolan.jetbroker.agent.services;

import se.plushogskolan.jetbroker.agent.domain.AirPort;
import se.plushogskolan.jetbroker.agent.repository.AirPortRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class AirPortServiceImp implements AirPortService {

	@Inject
	Logger log;

	@Inject
	private AirPortRepository airPortRepository;

	@Override
	public List<AirPort> getAllAirPorts() {
		return airPortRepository.getAllAirPorts();
	}

	@Override
	public AirPort getAirPort(String code) {
		return airPortRepository.getAirPort(code);
	}

	@Override
	public void handleAirportsChangedEvent() {

		log.info("Handles incoming airports changed event");

		airPortRepository.handleAirportsChangedEvent();
	}
}
