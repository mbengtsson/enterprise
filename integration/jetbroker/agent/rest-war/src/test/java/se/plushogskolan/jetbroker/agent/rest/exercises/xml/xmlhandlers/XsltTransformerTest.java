package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class XsltTransformerTest {

	@Test
	public void testTransformUsingXslt() throws Exception {

		String html =
		        XsltTransformer.transformUsingXslt("/flightRequest.xml",
		                "/flightRequest.xsl");
		assertTrue("ID", html.contains("<h1>Flight request - 5</h1>"));
		assertTrue("", html.contains("<p>Departure airport code: GBG</p>"));
		assertTrue("", html.contains("<p>arrivalAirportCode: STM</p>"));
		assertTrue("", html.contains("<p>No of passengers: 12</p>"));
		assertTrue("", html.contains("<p>Status: CREATED</p>"));

	}

}
