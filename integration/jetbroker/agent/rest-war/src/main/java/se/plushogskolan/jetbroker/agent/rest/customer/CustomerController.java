package se.plushogskolan.jetbroker.agent.rest.customer;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se.plushogskolan.jetbroker.agent.domain.Customer;
import se.plushogskolan.jetbroker.agent.rest.customer.model.CreateCustomerRequest;
import se.plushogskolan.jetbroker.agent.services.CustomerService;

import javax.inject.Inject;
import java.util.logging.Logger;

@Controller
public class CustomerController {

	Logger log = Logger.getLogger(CustomerController.class.getName());

	@Inject
	private CustomerService customerService;

	@RequestMapping(value = "/createCustomer", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Customer> createCustomer(@RequestBody CreateCustomerRequest request) throws Exception {
		log.fine("createCustomer: " + request);

		Customer customer = null;
		try {
			customer = customerService.createCustomer(request.buildCustomer());
			return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<Customer>(customer, HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/getCustomer/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Customer> getCustomer(@PathVariable long id) {

		log.fine("getCustomer: " + id);
		Customer customer = null;
		try {
			customer = customerService.getCustomer(id);
			return new ResponseEntity<Customer>(customer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Customer>(customer, HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/deleteCustomer/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> deleteCustomer(@PathVariable long id) {
		log.fine("deleteCustomer: " + id);

		try {
			customerService.deleteCustomer(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}

}
