package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import se.plushogskolan.jetbroker.agent.rest.exercises.xml.model.BoardingCard;

public class JaxbTransformer {

	public static String transformToXml(BoardingCard boardingCard) throws Exception {

		JAXBContext context = JAXBContext.newInstance(BoardingCard.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		StringWriter stringWriter = new StringWriter();
		m.marshal(boardingCard, stringWriter);

		return stringWriter.toString();

	}

}
