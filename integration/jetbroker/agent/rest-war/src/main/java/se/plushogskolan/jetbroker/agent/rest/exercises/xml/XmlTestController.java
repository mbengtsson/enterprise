package se.plushogskolan.jetbroker.agent.rest.exercises.xml;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;
import se.plushogskolan.jetbroker.agent.rest.exercises.xml.json.GetFlightRequestAsJsonResponse;
import se.plushogskolan.jetbroker.agent.rest.exercises.xml.model.BoardingCard;
import se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers.DomBuilder;
import se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers.JaxbTransformer;
import se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers.SaxParser;
import se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers.XsltTransformer;

import java.util.logging.Logger;

/**
 * A controller for testing various xml operations.
 */
@Controller
public class XmlTestController {

	Logger log = Logger.getLogger(XmlTestController.class.getName());

	/**
	 * A test for building an xml document using DOM.
	 */
	@RequestMapping(value = "/getXmlUsingDom", method = RequestMethod.GET)
	@ResponseBody
	public String getXmlUsingDom() throws Exception {

		log.fine("getXmlUsingDom");
		String xml = DomBuilder.buildXmlUsingDom(getMockedFlightRequest());
		return xml;
	}

	/**
	 * A test for reading an xml file using SAX, and returning it as JSON.
	 */
	@RequestMapping(value = "/getFlightRequestAsJson", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public GetFlightRequestAsJsonResponse getFlightRequestAsJson(@RequestParam String xmlFile) throws Exception {

		log.fine("getFlightRequestAsJson: file=" + xmlFile);
		FlightRequest flightRequest = SaxParser.readFlightRequestFromXmlUsingSax(xmlFile);
		return new GetFlightRequestAsJsonResponse(flightRequest);
	}

	/**
	 * A test for transforming an xml file using an XSLT stylesheet.
	 */
	@RequestMapping(value = "/getFlightRequest.html", method = RequestMethod.GET)
	@ResponseBody
	public String getFlightRequestAsHtml(@RequestParam String xmlFile, @RequestParam String xlsFile) throws Exception {

		log.fine("getFlightRequestAsHtml: file=" + xmlFile + ", " + xlsFile);

		String html = XsltTransformer.transformUsingXslt(xmlFile, xlsFile);
		return html;
	}

	/**
	 * A test for transforming an object using JAXB
	 */
	@RequestMapping(value = "/getBoardingCard", method = RequestMethod.GET)
	@ResponseBody
	public String getBoardingCard() throws Exception {

		log.fine("getBoardingCard");

		String xml = JaxbTransformer.transformToXml(getMockedBoardingCard());
		return xml;
	}

	/**
	 * General exception handler
	 */
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public String handleException(Exception e) {
		log.fine("handleException");

		e.printStackTrace();
		return "Error: " + e;
	}

	/**
	 * Returns a mocked FlightRequest for testing purposes
	 */
	public static FlightRequest getMockedFlightRequest() {

		FlightRequest fr = new FlightRequest();
		fr.setId(5);
		fr.setArrivalAirportCode("AAA");
		fr.setDepartureAirportCode("BBB");
		fr.setNoOfPassengers(10);
		fr.setRequestStatus(FlightRequestStatus.CREATED);
		return fr;

	}

	/**
	 * Returns a mocked BoardingCard for testing purposes
	 */
	public static BoardingCard getMockedBoardingCard() {
		BoardingCard boardingCard = new BoardingCard();
		boardingCard.setArrivalAirport("Gothenburg");
		boardingCard.setDepartureAirport("Stockholm");
		boardingCard.setDate("2014-04-22 14:45");
		boardingCard.setBoardingCardNo(546);
		return boardingCard;

	}

}
