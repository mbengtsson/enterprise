package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import se.plushogskolan.jee.utils.xml.XmlUtil;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;

public class DomBuilder {

	public static String buildXmlUsingDom(FlightRequest fr) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		Document doc = builder.newDocument();

		Element flightRequest = doc.createElement("flightrequest");
		doc.appendChild(flightRequest);
		flightRequest.setAttribute("id", String.valueOf(fr.getId()));

		Element arrivalAirportCode = doc.createElement("arrivalAirportCode");
		flightRequest.appendChild(arrivalAirportCode);
		arrivalAirportCode.setTextContent(fr.getArrivalAirportCode());

		Element departureAirportCode = doc.createElement("departureAirportCode");
		flightRequest.appendChild(departureAirportCode);
		departureAirportCode.setTextContent(fr.getDepartureAirportCode());

		Element noOfPassengers = doc.createElement("noOfPassengers");
		flightRequest.appendChild(noOfPassengers);
		noOfPassengers.setTextContent(String.valueOf(fr.getNoOfPassengers()));

		Element status = doc.createElement("status");
		flightRequest.appendChild(status);
		status.setTextContent(fr.getRequestStatus().toString());

		return XmlUtil.convertXmlDocumentToString(doc);

	}

}
