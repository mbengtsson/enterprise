package se.plushogskolan.jetbroker.agent.rest.exercises.xml.xmlhandlers;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import se.plushogskolan.jetbroker.agent.domain.request.FlightRequest;
import se.plushogskolan.jetbroker.agent.domain.request.FlightRequestStatus;

public class SaxParser {

	public static FlightRequest readFlightRequestFromXmlUsingSax(String file) throws Exception {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);

		SAXParser parser = factory.newSAXParser();
		InputStream in = SaxParser.class.getResourceAsStream(file);

		MyHandler handler = new MyHandler();
		parser.parse(in, handler);

		return handler.getParsedFlightRequest();

	}

	static class MyHandler extends DefaultHandler {

		private FlightRequest flightRequest;
		private String currentTextValue;

		public FlightRequest getParsedFlightRequest() {
			return flightRequest;
		}

		@Override
		public void startDocument() throws SAXException {

			flightRequest = new FlightRequest();
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equals("flightrequest")) {
				flightRequest.setId(Long.parseLong(attributes.getValue("id")));
			}

		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			currentTextValue = new String(ch, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (qName.equals("departureAirportCode")) {
				flightRequest.setDepartureAirportCode(currentTextValue);
			} else if (qName.equals("arrivalAirportCode")) {
				flightRequest.setArrivalAirportCode(currentTextValue);
			} else if (qName.equals("noOfPassengers")) {
				flightRequest.setNoOfPassengers(Integer.parseInt(currentTextValue));
			} else if (qName.equals("status")) {
				flightRequest.setRequestStatus(FlightRequestStatus.valueOf(currentTextValue));
			}
		}

	}

}
