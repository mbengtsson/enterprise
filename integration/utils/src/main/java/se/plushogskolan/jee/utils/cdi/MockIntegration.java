package se.plushogskolan.jee.utils.cdi;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Stereotype;

/**
 * CDI qualifier to use when using a mock integration.
 * 
 * The @Alternative annotation is an example of a stacked annotation. Normally @Alternative
 * can be put on a class directly to indicate that is is not the default class
 * to be injected. By wrapping @Alternative in this @MockIntegration annotation,
 * we can enable all mocks at the same time by enabling this annotation in
 * beans.xml.
 * 
 * <alternatives>
 * <stereotype>se.plushogskolan.jee.utils.cdi.MockIntegration</stereotype>
 * </alternatives>
 * 
 * I decided to call it MockIntegration and use it only for integration. We
 * could create another annotation called @Mock or @JPAMock, just so we can have
 * more control which mocks we are activating.
 */
@Stereotype
@Alternative
@Retention(RUNTIME)
@Target({ TYPE })
public @interface MockIntegration {

}
